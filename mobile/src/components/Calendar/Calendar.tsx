import * as React from 'react';

import { StyleSheet, View } from 'react-native';

import update from "immutability-helper";

import { CalendarList, DateObject, DotMarkingProps } from 'react-native-calendars'
import { NavigationStackProp } from "react-navigation-stack";

import { IFirebaseContextProps, withFirebase } from "../Firebase";
import Routes from "../../constants/routes";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import { IAsyncStorageContextProps, withAsyncStorage } from "../Session/GlobalContext";
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['pl'] = {
    monthNames: ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
    monthNamesShort: ['Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru'],
    dayNames: ['Niedziela','Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
    dayNamesShort: ['Nd','Pon','Wt','Śr','Czw','Pt','Sob'],
};
LocaleConfig.defaultLocale = 'pl';

interface ICalendarProps extends IAsyncStorageContextProps, IFirebaseContextProps {
    navigation: NavigationStackProp<{}>;
}

interface ICalendarState {
    currentDate: Date;
    marking: DotMarkingProps;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
});

class Calendar extends React.Component<ICalendarProps, ICalendarState> {

    constructor(props: ICalendarProps) {
        super(props);

        this.state = {
            currentDate: new Date(),
            marking: { markedDates: {} }
        }
    }

    componentDidMount() {
        const { currentDate } = this.state;
        this.reloadEvents(currentDate);
    }

    static addMonths(date: Date, n: number) {
        return new Date(date.setMonth(date.getMonth() + n));
    }

    static dateObjectToDateString(dayObject: DateObject) {
        const pad = (s: number) => (s < 10) ? '0' + s : s;
        return dayObject.year + "-" + pad(dayObject.month) + "-" + pad(dayObject.day);
    }

    render() {
        const { currentDate, marking } = this.state;
        return (<View style={styles.container}>
            <CalendarList
                horizontal={true} pagingEnabled={true}
                firstDay={1}
                current={currentDate}
                onDayPress={dateObject => this.handleDayClick(Calendar.dateObjectToDateString(dateObject))}
                onPressArrowLeft={() => this.handleMonthChange(Calendar.addMonths(currentDate, -1))}
                onPressArrowRight={() => this.handleMonthChange(Calendar.addMonths(currentDate, 1))}
                markedDates={marking.markedDates} />
        </View>);
    }

    handleDayClick(date: string) {
        const { navigation } = this.props;
        navigation.navigate(Routes.PLAN, { date });
    }

    handleMonthChange(newDate: Date) {
        this.setState(prevState => update(prevState, {
            currentDate: { $set: newDate }
        }));
        this.reloadEvents(newDate);
    }

    reloadEvents(date: Date) {
        date.setDate(1);
        const { asyncStorageData, firebase } = this.props;

        firebase.getPatientDocRef(asyncStorageData.patientId!).collection("plans").get()
            .then(dayDocs => {
                const marking = dayDocs.docs
                    .filter(doc => doc.exists)
                    .map(doc => doc.id)
                    .reduce((acc: DotMarkingProps, curr: string) => {
                        acc.markedDates[curr] = { marked: true, dotColor: "#283593"};
                        return acc;
                    }, { markedDates: {} });
                this.setState(prevState => update(prevState, {
                    marking: { $set: marking }
                }));
            })
            .catch(error => {

            })
            .finally(() => {

            });
    }

}

export default withAsyncStorage(withFirebase(Calendar));

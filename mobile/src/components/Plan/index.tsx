import * as React from "react";

import { StyleSheet, View, Text } from 'react-native';

import update from "immutability-helper";

import { NavigationStackOptions, NavigationStackProp } from "react-navigation-stack";

import { IFirebaseContextProps, withFirebase } from "../Firebase";

import IPlan, { EMPTY_PLAN } from "@saqq/telerehab-infrastructure/lib/interfaces/plan";
import IPlanItem from "@saqq/telerehab-infrastructure/lib/interfaces/plan-item";
import IExercise, { EMPTY_EXERCISE } from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";

import TextBanner from "../Banner/TextBanner";
import { IAsyncStorageContextProps, withAsyncStorage } from "../Session/GlobalContext";

interface IPlanItemCardProps extends IFirebaseContextProps {
    planItem: IPlanItem;
}

interface IPlanItemCardState {
    busy: boolean;
    exercise: IExercise;
}

class PlanItemCardBase extends React.Component<IPlanItemCardProps, IPlanItemCardState> {

    constructor(props: IPlanItemCardProps) {
        super(props);

        this.state = {
            busy: false,
            exercise: EMPTY_EXERCISE
        }
    }

    componentDidMount() {
        const { planItem } = this.props;

        this.setState(prevState => update(prevState, {
            busy: { $set: true }
        }));

        planItem.exercise.get()
            .then(doc => {
               const exercise = doc.data() as IExercise;
               this.setState(prevState => update(prevState, {
                   exercise: { $set: exercise }
               }));
            })
            .catch(error => {

            })
            .finally(() => {
                this.setState(prevState => update(prevState, {
                    busy: { $set: false }
                }));
            });
    }

    render() {
        const { planItem } = this.props;
        const { busy, exercise } = this.state;

        return <View style={styles.item}>
            <Text style={styles.text}>{busy ? "Loading..." : ("" + exercise.name)}</Text>
        </View>;
    }

}

const PlanItemCard = withFirebase(PlanItemCardBase);

interface IPlanProps extends IAsyncStorageContextProps, IFirebaseContextProps {
    navigation: NavigationStackProp<{ date: string }>;
}

interface IPlanState {
    busy: boolean,
    plan: IPlan
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    item: {
        width: '90%',
        padding: 12,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: '#e0e0e0',
        marginBottom: 4
    },
    text: {

    }
});

class PlanScreen extends React.Component<IPlanProps, IPlanState> {

    constructor(props: IPlanProps) {
        super(props);

        this.state = {
            busy: false,
            plan: EMPTY_PLAN
        }
    }

    componentDidMount() {
        this.loadPlan();
    }

    render() {
        const { navigation } = this.props;
        const { busy, plan } = this.state;
        return (<View style={styles.container}>
            <TextBanner text={navigation.getParam('date', 'unknown') + (busy ? " (loading)": "")}/>
            {
                plan.exercises.map((planItem, idx) =>
                    <PlanItemCard key={idx} planItem={planItem}/>)
            }
        </View>);
    }

    loadPlan() {
        const { navigation, asyncStorageData, firebase } = this.props;
        const date = navigation.getParam('date', 'unknown');

        this.setState(prevState => update(prevState, {
            busy: { $set: true }
        }));

        firebase.getPatientDocRef(asyncStorageData.patientId!).collection('plans').doc(date).get()
            .then(doc => {
                if (doc.exists) {
                    const plan = doc.data() as IPlan;
                    this.setState(prevState => update(prevState, {
                        plan: { $set: plan }
                    }));
                }
            })
            .catch(error => {

            })
            .finally(() => {
                this.setState(prevState => update(prevState, {
                    busy: { $set: false }
                }));
            });
    }

}

export default withAsyncStorage(withFirebase(PlanScreen));

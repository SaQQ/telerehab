import * as React from 'react';


import { IDayPlanContextProps, withDayPlan } from "../DayPlanContext";
import MenuIcon from "./MenuIcon";

import playButtonIcon from "./play-button.png";
import antiClockwiseIcon from "./anti-clockwise.png";
import checkedIcon from "./checked.png";
import endIcon from "./end.png";

interface IDayPlanStartIconProps extends IDayPlanContextProps {
    onStart: () => void;
}

interface IDayPlanStartIconState {

}

/**
 * Menu icon reading day plan context and displaying one of states:
 * no plan for today, start, continue or done
 */
class DayPlanStartIconBase extends React.Component<IDayPlanStartIconProps, IDayPlanStartIconState> {

    constructor(props: IDayPlanStartIconProps) {
        super(props);
    }

    onPress() {
        this.props.onStart();
    }

    render() {
        const { dayPlan } = this.props;

        if (!dayPlan || dayPlan.exercises.length === 0) {
            return (<MenuIcon name={"Brak planu na dziś"} img={antiClockwiseIcon}/>);
        }

        const endTimes = dayPlan.exercises.map(item => item.status.endTime);

        if (endTimes.every(endTime => endTime === null)) {
            return (<MenuIcon name={"Rozpocznij ćwiczenia"} img={playButtonIcon} onPress={() => this.onPress()}/>);
        }

        if (endTimes.some(endTime => endTime === null)) {
            return (<MenuIcon name={"Kontynuuj ćwiczenia"} img={endIcon} onPress={() => this.onPress()}/>);
        }

        return (<MenuIcon name={"Ćwiczenia zakończone"} img={checkedIcon}/>);
    }

}

export default withDayPlan(DayPlanStartIconBase);

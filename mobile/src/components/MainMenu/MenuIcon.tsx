import * as React from 'react';
import { TouchableOpacity, View, StyleSheet, Text, Image } from 'react-native';

interface IMenuIconProps {
    name: string;
    img?: any;
    onPress?: () => void;
}

const MenuIcon = (props: IMenuIconProps) => {
    const { name, img, onPress } = props;

    const Contents = (<View style={styles.background}>
        <Text style={styles.name}>{name}</Text>
        <View style={styles.iconContainer}>
            <Image style={styles.image} source={img}/>
        </View>
    </View>);

    if (onPress) {
        return (<TouchableOpacity style={styles.container} onPress={() => onPress()}>{Contents}</TouchableOpacity>);
    } else {
        return (<View style={styles.container}>{Contents}</View>);
    }
};

export default MenuIcon;


const styles = StyleSheet.create({
    container: {
        width: '90%',
        height: 140,
        margin: 10
    },
    background: {
        flex: 1,
        padding: 12,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#283593',
        borderRadius: 30
    },
    name: {
        fontSize: 20,
        marginBottom: 4,
        color: '#ffffff'
    },
    iconContainer: {
        width: 80,
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: '100%',
        height: '100%'
    }
});

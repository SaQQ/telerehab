import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, BackHandler } from 'react-native';

import { NavigationStackProp } from 'react-navigation-stack';

import CalendarImg from './calendar.png';

import MenuHeader from './MenuHeader';
import MenuIcon from "./MenuIcon";
import DayPlanStartIcon from "./DayPlanStartIcon";
import Routes from "../../constants/routes";
import { IAsyncStorageContextProps, withAsyncStorage } from "../Session/GlobalContext";
import GreetingButton from "../GreetingScreen/GreetingButton";
import logIn from "./log-in.png";

interface IMainMenuProps extends IAsyncStorageContextProps {
    navigation: NavigationStackProp<{}>;
}

class MainMenu extends React.Component<IMainMenuProps> {

    componentDidMount() {
        const { asyncStorageData, navigation } = this.props;
        if (!asyncStorageData.patientId) {
            navigation.replace(Routes.GREETING_SCREEN);
        }
    }

    componentDidUpdate() {
        const { asyncStorageData, navigation } = this.props;
        if (!asyncStorageData.patientId) {
            navigation.replace(Routes.GREETING_SCREEN);
        }
    }

    render() {
        const { asyncStorageData } = this.props;

        return (<View style={styles.container}>
            <MenuHeader/>
            <View style={styles.iconContainer}>
                <DayPlanStartIcon onStart={() => this.onStart()}/>
                <MenuIcon onPress={() => this.onCalendar()} name="Kalendarz" img={CalendarImg}/>
                <GreetingButton text="Wyjdź" img={logIn} onPress={() => this.onExit()}/>
            </View>
            {__DEV__ && <TouchableOpacity onPress={() => this.resetPatientID()}>
                <Text>{`Patient ID: ${asyncStorageData.patientId}`}</Text>
            </TouchableOpacity>}
        </View>);
    }

    private onCalendar() {
        const { push } = this.props.navigation;
        push(Routes.CALENDAR);
    };

    private onStart() {
        const { push } = this.props.navigation;
        push(Routes.EXAMINATION);
    }

    private resetPatientID() {
        const { asyncStorageData } = this.props;
        asyncStorageData.setPatientId(null);
    }

    private onExit() {
        BackHandler.exitApp();
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    iconContainer: {
        flex: 1,
        flexGrow: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default withAsyncStorage(MainMenu);
import * as React from 'react';
import { Text, ActivityIndicator, StyleSheet, View } from 'react-native';
import update from 'immutability-helper';

import IExercise, { EMPTY_EXERCISE } from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";

import { IDayPlanContextProps, withDayPlan } from "../DayPlanContext";
import { IAsyncStorageContextProps, withAsyncStorage } from "../Session/GlobalContext";

import TaskDescription from "./TaskDescription";
import Exercise from "./Excercise";
import IPlanItem from "@saqq/telerehab-infrastructure/lib/interfaces/plan-item";
import IPlan, { IPlanReport } from "@saqq/telerehab-infrastructure/lib/interfaces/plan";
import { NavigationStackProp } from "react-navigation-stack";
import TextBanner from "../Banner/TextBanner";
import Survey from "./Survey";
import Refusal from "./Refusal";
import { IFirebaseContextProps, withFirebase } from "../Firebase";
import Interruption from "./Interruption";
import Routes from "../../constants/routes";

interface IExaminationScreenProps extends IFirebaseContextProps, IAsyncStorageContextProps, IDayPlanContextProps {
    navigation: NavigationStackProp<{ date: string }>;
}

interface IExaminationScreenState {
    stage: "description" | "running" | "survey" | "refusal" | "interruption";
    exercise: IExercise | null;
    photoUrls: string[];
    exerciseLoading: boolean;
}

/**
 * Examination screen handles current exercise determined from dayPlan props.
 * Exercise execution can be in one of temporary stages as  defined in stage property of state.
 * Exercise status in the firestore is updated along with the stage progression.
 * After finishing a single exercise, current plan item in the context changes and the exercise is reloaded.
 */
class ExaminationScreenBase extends React.Component<IExaminationScreenProps, IExaminationScreenState> {

    private endTime: Date;
    private refusalReason: string;

    constructor(props: IExaminationScreenProps) {
        super(props);

        this.state = {
            stage: "description",
            exercise: null,
            photoUrls: [],
            exerciseLoading: false,
        };

        this.endTime = new Date();
        this.refusalReason = "";
    }

    componentDidMount() {
        this.loadExercise().then(() => {
        });
    }

    componentDidUpdate(prevProps: IExaminationScreenProps) {
        const { navigation } = this.props;

        const [prevIndex, prevItem] = this.currentPlanItem(prevProps.dayPlan);
        const [index, item] = this.currentPlanItem(this.props.dayPlan);

        // Somebody did something nasty - deleted plan or something?
        if (item === null) {
            navigation.pop();
        }

        if (item === null && prevItem === null) {
            return;
        }

        if (item !== null && prevItem !== null && item.exercise.isEqual(prevItem.exercise)) {
            return;
        }

        // Reload exercise as it has changed
        this.loadExercise().then(() => {
        });
    }

    render() {
        const { dayPlan } = this.props;
        const { stage, exercise, photoUrls, exerciseLoading } = this.state;

        const [index, item] = this.currentPlanItem(dayPlan);

        if (!item) {
            return <Text>Invalid plan item</Text>;
        }

        if (!exercise || exerciseLoading) {
            return <ActivityIndicator color="#ff9800" size={80}/>;
        }

        let content;

        if (stage === "description") {
            content =
                <TaskDescription exercise={exercise!} photoUrls={photoUrls} onStart={() => this.onStart()}
                                 onSkip={() => this.onSkip()}/>;
        } else if (stage === "running") {
            content = <Exercise planItem={item} onEnd={() => this.onEnd()}/>;
        } else if (stage === "survey") {
            content = <Survey exercise={exercise!} onGrade={grade => this.onGrade(grade)}/>;
        } else if (stage === "refusal") {
            content = <Refusal onChosenOption={reason => this.onRefusalReason(reason)}/>;
        } else if (stage === "interruption") {
            content = <Interruption onLeave={() => this.onLeave()} onContinue={() => this.onContinue()}/>;
        }

        const header = (exercise.code ? exercise.code + " " : "") + exercise.name;

        return <View style={styles.container}>
            <TextBanner text={header}/>
            {content}
        </View>
    }

    currentPlanItem(plan: IPlan | null): [number, IPlanItem | null] {
        if (!plan) {
            return [-1, null];
        }
        const index = plan.exercises.findIndex(exercise => exercise.status.endTime === null);
        if (index >= 0) {
            return [index, plan.exercises[index]];
        } else {
            return [-1, null];
        }
    }

    onStart() {
        const { asyncStorageData, firebase, dayPlan } = this.props;
        const [index, item] = this.currentPlanItem(dayPlan);

        firebase.startExercise(asyncStorageData.patientId!, index, new Date())
            .then(() => {
            })
            .catch((e) => {
                console.warn("Unable to start exercise " + e.toString())
            });

        this.setState(prevState => update(prevState, {
            stage: { $set: "running" },
        }));
    }

    onEnd() {
        this.endTime = new Date();

        this.setState(prevState => update(prevState, {
            stage: { $set: "survey" },
        }));
    }

    onSkip() {
        this.setState(prevState => update(prevState, {
            stage: { $set: "refusal" },
        }));
    }

    onRefusalReason(reason: string) {
        this.refusalReason = reason;
        this.setState(prevState => update(prevState, {
            stage: { $set: "interruption" },
        }));
    }

    onContinue() {
        const { asyncStorageData, firebase, dayPlan } = this.props;
        const [index] = this.currentPlanItem(dayPlan);

        firebase.skipExercise(asyncStorageData.patientId!, index, new Date(), this.refusalReason).then(() => {

        }).catch((e) => {
            console.warn("Unable to skip exercise " + e.toString())
        });

        this.setState(prevState => update(prevState, {
            stage: { $set: "description" },
        }));
    }

    onLeave() {
        const { asyncStorageData, firebase, dayPlan } = this.props;
        const [index] = this.currentPlanItem(dayPlan);

        firebase.skipExercise(asyncStorageData.patientId!, index, new Date(), this.refusalReason).then(() => {

        }).catch((e) => {
            console.warn("Unable to skip exercise " + e.toString())
        });

        const { navigation } = this.props;
        navigation.replace(Routes.DAILY_REPORT);

        //TODO ustawić status planu na pominięty, pozostałe ćwiczenia nie będą wykonywane
    }

    onGrade(grade: number) {
        const { asyncStorageData, firebase, dayPlan, navigation } = this.props;

        const [index, item] = this.currentPlanItem(dayPlan);

        firebase.endExercise(asyncStorageData.patientId!, index, this.endTime)
            .then(() => firebase.gradeExercise(asyncStorageData.patientId!, index, grade))
            .then(() => {
                this.setState(prevState => update(prevState, {
                    stage: { $set: "description" },
                }));
            })
            .catch((e) => {
                console.warn("Unable to end exercise " + e.toString())
            });

        if (index === dayPlan!.exercises.length - 1) {
            const { navigation } = this.props;
            navigation.replace(Routes.DAILY_REPORT);
        }
    }

    async loadExercise() {
        const { dayPlan } = this.props;

        const [index, item] = this.currentPlanItem(dayPlan);
        if (!item) {
            return;
        }

        this.setState(prevState => update(prevState, {
            exerciseLoading: { $set: true }
        }));

        const doc = await item.exercise.get();
        const exercise = { ...EMPTY_EXERCISE, ...doc.data() as IExercise };

        const photoUrls = await Promise.all(exercise.photoPaths.map(path => this.props.firebase.getStorageURL(path)));

        this.setState(prevState => update(prevState, {
            exercise: { $set: exercise },
            photoUrls: { $set: photoUrls },
            exerciseLoading: { $set: false }
        }));
    }

}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        padding: 10,
        flex: 1
    }
});


export default withFirebase(withDayPlan(withAsyncStorage(ExaminationScreenBase)));
import * as React from "react";

import { View, Text, StyleSheet } from "react-native";
import TextBannerButton from "../Banner/TextBannerButton";

export interface IRefusalProps {
    onChosenOption: (option: string) => void;
}

const Refusal: React.FC<IRefusalProps> = (props) => {
    const { onChosenOption } = props;
    return <View style={styles.container}>
        <TextBannerButton text="Źle się czuję" onPress={() => onChosenOption("Źle się czuję")}/>
        <TextBannerButton text="Ćwiczenie jest za trudne" onPress={() => onChosenOption("Ćwiczenie jest za trudne")}/>
        <TextBannerButton text="Inna przyczyna" onPress={() => onChosenOption("Inna przyczyna")}/>
    </View>;
};

export default Refusal;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexGrow: 1,
        flex: 1
    },
    note: {
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
});
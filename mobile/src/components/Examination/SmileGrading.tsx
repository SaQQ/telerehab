import * as React from 'react';
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native';

import smile0 from './smile0.png';
import smile1 from './smile1.png';
import smile2 from './smile2.png';
import smile3 from './smile3.png';
import smile4 from './smile4.png';


interface IGradeButtonProps {
    img: any;
    onClick: () => void;
}

const GradeButton: React.FC<IGradeButtonProps> = (props) => {
    const { img, onClick } = props;
    return <TouchableOpacity style={styles.button} onPress={() => onClick()}>
        <Image style={styles.img} source={img}/>
    </TouchableOpacity>;
};

export interface ISmileGradingProps {
    onClick: (grade: number) => void;
}

const SmileGrading: React.FC<ISmileGradingProps> = (props) => {
    const { onClick } = props;
    return <View style={styles.container}>
        <GradeButton img={smile0} onClick={() => onClick(1)}/>
        <GradeButton img={smile1} onClick={() => onClick(2)}/>
        <GradeButton img={smile2} onClick={() => onClick(3)}/>
        <GradeButton img={smile3} onClick={() => onClick(4)}/>
        <GradeButton img={smile4} onClick={() => onClick(5)}/>
    </View>;
};

export default SmileGrading;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
    },
    img: {
        width: 80,
        height: 80,
        resizeMode: 'contain',
    },
    button: {

    }
});

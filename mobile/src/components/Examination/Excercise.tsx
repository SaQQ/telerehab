import * as React from 'react';
import { View, StyleSheet, ActivityIndicator, Text } from 'react-native';

import update from 'immutability-helper';

import TextBanner from "../Banner/TextBanner";
import TextBannerButton from "../Banner/TextBannerButton";
import IPlanItem from "@saqq/telerehab-infrastructure/lib/interfaces/plan-item";

export interface IExerciseProps {
    planItem: IPlanItem;
    onEnd: () => void;
}

interface IExerciseState {
    elapsedSeconds: number
}

class ExerciseBase extends React.Component<IExerciseProps, IExerciseState> {

    constructor(props: IExerciseProps) {
        super(props);

        this.state = {
            elapsedSeconds: 0
        }
    }

    private intervalHandle: number = 0;

    componentDidMount(): void {
        this.intervalHandle = window.setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount(): void {
        window.clearInterval(this.intervalHandle);
    }

    private tick() {
        this.setState(prevState => update(prevState, {
            elapsedSeconds: { $apply: (x: number) => x + 1 }
        }))
    }

    render() {
        const { planItem, onEnd } = this.props;
        const { elapsedSeconds } = this.state;

        const displayedSeconds = planItem.timeLimit > 0 ? (Math.max(planItem.timeLimit * 60 - elapsedSeconds, 0)) : elapsedSeconds;

        const minutes = Math.floor(displayedSeconds / 60);
        const seconds = displayedSeconds - minutes * 60;
        const timerText = minutes.toString() + ":" + seconds.toString().padStart(2, "0");

        let bottomNote = "Po zakończonym ćwiczeniu naciśnij przycisk Koniec";
        if (planItem.timeLimit > 0 && displayedSeconds === 0) {
            bottomNote = "Czas minął! Przestań ćwiczyć i nacisnij przycisk Koniec";
        }

        let repetitionCounter;
        if (planItem.repetitions >= 11 && planItem.repetitions <=20){
            repetitionCounter = "powtorzeń"
        } else if (planItem.repetitions === 1){
            repetitionCounter = "powtórzenie"
        } else if (planItem.repetitions%10 <= 4 && planItem.repetitions%10 >=2){
            repetitionCounter = "powtórzenia"
        } else {
            repetitionCounter = "powtórzeń"
        }

        return <View style={styles.container}>
            {planItem.repetitions > 0 &&
            <Text style={styles.note}>{`Wykonaj ${planItem.repetitions} ${repetitionCounter}`}</Text>}
            <Text style={styles.timer}>{timerText}</Text>
            <Text style={styles.note}>{bottomNote}</Text>
            <TextBannerButton text="Koniec" onPress={() => onEnd()}/>
        </View>;
    }

}

export default ExerciseBase;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexGrow: 1,
        flex: 1
    },
    timer: {
        flexGrow: 1,
        fontSize: 36,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    note: {
        width: '90%',
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 20
    }
});

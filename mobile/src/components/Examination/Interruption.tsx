import * as React from "react";

import { View, Text, StyleSheet } from "react-native";
import TextBannerButton from "../Banner/TextBannerButton";

export interface IInterruptionProps {
    onLeave: () => void;
    onContinue: () => void;
}

const Interruption: React.FC<IInterruptionProps> = (props) => {
    const { onLeave, onContinue } = props;
    return <View style={styles.container}>
        <Text>Czy chcesz kontynuować trening?</Text>
        <TextBannerButton text="Tak, wyświetl inne ćwiczenie" onPress={() => onContinue()}/>
        <TextBannerButton text="Nie, kończę trening." onPress={() => onLeave()}/>
    </View>;
};

export default Interruption;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexGrow: 1,
        flex: 1
    },
    note: {
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
});
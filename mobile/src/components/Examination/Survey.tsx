import * as React from "react";

import { View, Text, StyleSheet } from "react-native";

import IExercise from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";

import SmileGrading from "./SmileGrading";


export interface ISurveyProps {
    exercise: IExercise;
    onGrade: (grade: number) => void;
}

const Survey: React.FC<ISurveyProps> = (props) => {
    const { exercise, onGrade } = props;
    return <View style={styles.container}>
        <SmileGrading onClick={grade => onGrade(grade)}/>
        <Text style={styles.note}>Oceń ćwiczenie</Text>
    </View>;
};

export default Survey;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexGrow: 1,
        flex: 1
    },
    note: {
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
});

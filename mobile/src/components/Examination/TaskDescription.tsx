import * as React from "react";

import update from "immutability-helper";

import { View, Text, StyleSheet, Image, ScrollView } from "react-native";

import IExercise from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";
import TextBannerButton from "../Banner/TextBannerButton";
import Tts from "react-native-tts";

export interface ITaskDescriptionProps {
    exercise: IExercise;
    photoUrls: string[];
    onStart: () => void;
    onSkip: () => void;
}

export interface ITaskDescriptionState {
    part: number;
    imageUrl: string | null;
}

class TaskDescription extends React.Component<ITaskDescriptionProps, ITaskDescriptionState> {

    private subscription: any = null;

    private photoRegex = /\[PHOTO \d+]/;

    constructor(props: ITaskDescriptionProps) {
        super(props);
        this.state = {
            part: 0, // Means, before first photo - icon is displayed
            imageUrl: null
        }
    }

    componentDidMount() {
        this.subscription = Tts.addEventListener('tts-finish', () => this.nextPart());
        this.play();
    }

    componentWillUnmount() {
        Tts.stop();
        // this.subscription();
        this.subscription = null;
    }

    private play() {
        Tts.stop();
        this.setState(prevState => update(prevState, { part: { $set: 0 } }),
            () => {
                Tts.speak(this.partInstructions(0));
            });
    }

    private partCount() {
        const { exercise } = this.props;
        return exercise.instructions.split(this.photoRegex).length;
    }

    private partInstructions(part: number) {
        const { exercise } = this.props;
        const parts = exercise.instructions.split(this.photoRegex);
        return part < parts.length ? parts[part].trim() : "";
    }

    private partPhotoUrl(part: number) {
        const { exercise, photoUrls } = this.props;
        if (part === 0) {
            return exercise.iconData;
        } else {
            let regex = /\[PHOTO (\d+)]/g;

            let match: RegExpMatchArray | null = null;

            for (let i = 0; i < part; ++i) {
                match = regex.exec(exercise.instructions);
            }

            if (!match) {
                return null;
            }

            const photoIdx = Number(match[1]) - 1;
            if (photoIdx >= 0 && photoIdx < photoUrls.length) {
                return photoUrls[photoIdx];
            } else {
                return null;
            }
        }
    }

    // 0 | F1 | 1 | F2 | 2, partCount = 3

    private nextPart() {
        if (this.state.part + 1 == this.partCount()) {
            return;
        }
        this.setState(prevState => update(prevState, { part: { $set: prevState.part + 1 } }),
            () => {
                Tts.speak(this.partInstructions(this.state.part));
            });
    }

    render() {
        const { onStart, onSkip } = this.props;
        const { part } = this.state;

        const photoUrl = this.partPhotoUrl(part);

        return <View style={styles.container}>
            {photoUrl && <Image style={styles.icon} source={{ uri: photoUrl }}/>}
            {photoUrl == null && <Text>Nofoto</Text>}
            <ScrollView style={styles.instructionsView}>
                <Text style={styles.text}>{this.partInstructions(part)}</Text>
            </ScrollView>
            <Text>{`${part} / ${this.partCount() - 1}`}</Text>
            <TextBannerButton text={"Start"} onPress={() => onStart()}/>
            <TextBannerButton text={"Pomiń"} onPress={() => onSkip()}/>
            <TextBannerButton text={"Przeczytaj ponownie"} onPress={() => this.play()}/>
        </View>;
    }

}

export default TaskDescription;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        flexGrow: 1
    },
    icon: {
        width: '90%',
        flexGrow: 1,
        resizeMode: 'contain',
        margin: 16
    },
    text: {
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18
    },
    instructionsView: {
        width: '100%',
        flexGrow: 1
    }
});

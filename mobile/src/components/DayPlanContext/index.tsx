import * as React from "react";

import update from "immutability-helper";

import IPlan from "@saqq/telerehab-infrastructure/lib/interfaces/plan";

import { IAsyncStorageContextProps, withAsyncStorage } from "../Session/GlobalContext";
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore";
import { IExerciseStatus } from "@saqq/telerehab-infrastructure/lib/interfaces/plan-item";
import { IFirebaseContextProps, withFirebase } from "../Firebase";

export interface IDayPlanContextProps {
    dayPlan: IPlan | null;
}

type Omitted<P> = Pick<P, Exclude<keyof P, keyof IDayPlanContextProps>>;

export const DayPlanContext = React.createContext<IPlan | null>(null);

export function withDayPlan<P extends IDayPlanContextProps>(Component: React.ComponentType<P>): React.ComponentType<Omitted<P>> {
    return (props: Omitted<IDayPlanContextProps>) => {
        return (
            <DayPlanContext.Consumer>
                {dayPlan => <Component {...props as P} dayPlan={dayPlan}/>}
            </DayPlanContext.Consumer>
        );
    }
}

interface IDayPlanProviderProps extends IAsyncStorageContextProps, IFirebaseContextProps {

}

interface IDayPlanProviderState {
    dayPlan: IPlan | null;
    dayPlanRefreshing: boolean;
}

function getDateString(date: Date) {
    return `${date.getFullYear().toString().padStart(4, '0')}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
}

class DayPlanProviderBase extends React.Component<IDayPlanProviderProps, IDayPlanProviderState> {

    constructor(props: IDayPlanProviderProps) {
        super(props);

        this.state = {
            dayPlan: null,
            dayPlanRefreshing: false
        };

        this.unsubscribe = null;
    }

    private unsubscribe: (() => void) | null;

    componentDidUpdate() {
        const { firebase, asyncStorageData } = this.props;

        if (this.unsubscribe) {
            this.unsubscribe();
            this.unsubscribe = null;
        }

        if (asyncStorageData) {
            const ref = firebase.getPatientDocRef(asyncStorageData.patientId!).collection("plans").doc(getDateString(new Date()));
            this.unsubscribe = ref.onSnapshot((doc) => this.onPlanSnapshot(doc));
        }
    }

    static areStatusesEqual(a: IExerciseStatus, b: IExerciseStatus): boolean {
        if (a.grade !== b.grade) {
            return false;
        }

        if (a.endTime === null && b.endTime !== null || a.endTime !== null && b.endTime === null) {
            return false;
        } else if (a.endTime !== null && b.endTime !== null && !a.endTime.isEqual(b.endTime)) {
            return false;
        }

        if (a.startTime === null && b.startTime !== null || a.startTime !== null && b.startTime === null) {
            return false;
        } else if (a.startTime !== null && b.startTime !== null && !a.startTime.isEqual(b.startTime)) {
            return false;
        }

        return true;
    }

    static arePlansEqual(a: IPlan, b: IPlan): boolean {
        if (a.exercises.length !== b.exercises.length) {
            return false;
        }

        for (let i = 0; i < a.exercises.length; ++i) {
            const ea = a.exercises[i];
            const eb = b.exercises[i];
            if (!ea.exercise.isEqual(eb.exercise)) {
                return false;
            }
            if (!DayPlanProviderBase.areStatusesEqual(ea.status, eb.status)) {
                return false;
            }

            // TODO: Detect customization changes
        }

        return true;
    }

    onPlanSnapshot(doc: FirebaseFirestoreTypes.DocumentSnapshot) {
        const { dayPlan } = this.state;
        const newDayPlan = doc.exists ? doc.data() as IPlan : null;

        if (dayPlan === null && newDayPlan === null) {
            return;
        } else if (dayPlan !== null && newDayPlan !== null && DayPlanProviderBase.arePlansEqual(dayPlan, newDayPlan)) {
            return;
        }

        this.setState(prevState => update(prevState, {
            dayPlan: { $set: newDayPlan }
        }));

    }

    componentWillUnmount(): void {
        if (this.unsubscribe) {
            this.unsubscribe();
            this.unsubscribe = null;
        }
    }

    render() {
        const { dayPlan } = this.state;
        return <DayPlanContext.Provider value={dayPlan}>
            {this.props.children}
        </DayPlanContext.Provider>;
    }

}

export const DayPlanProvider = withFirebase(withAsyncStorage(DayPlanProviderBase));

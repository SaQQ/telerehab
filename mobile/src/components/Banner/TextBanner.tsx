import * as React from 'react';
import {Text, StyleSheet} from "react-native";

import Banner from "./Banner";

export interface ITextBannerProps {
    text: string;
}

const TextBanner: React.FC<ITextBannerProps> = (props) => {
    return (<Banner {...props}>
        <Text style={styles.text}>{props.text}</Text>
    </Banner>);
};

export default TextBanner;

const styles = StyleSheet.create({
    text: {
        color: '#ffffff',
        fontSize: 16
    }
});

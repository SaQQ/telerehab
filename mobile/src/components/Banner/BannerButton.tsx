import * as React from 'react';
import { TouchableOpacity, StyleSheet } from "react-native";

import Banner from './Banner';

export interface IBannerButtonProps {
    style?: any;
    onPress: () => void;
}

const BannerButton: React.FC<IBannerButtonProps> = (props) => {
    return (<TouchableOpacity style={styles.container} onPress={() => props.onPress()}>
        <Banner {...props}>
            {props.children}
        </Banner>
    </TouchableOpacity>);
};

export default BannerButton;

const styles = StyleSheet.create({
    container: {
        width: '100%'
    }
});
import * as React from 'react';
import {Text, StyleSheet} from "react-native";

import BannerButton, { IBannerButtonProps } from './BannerButton';

export interface ITextBannerButtonProps extends IBannerButtonProps {
    style?: any;
    text: string;
}

const TextBannerButton: React.FC<ITextBannerButtonProps> = (props: ITextBannerButtonProps) => {
    return (<BannerButton {...props}>
        <Text style={styles.text}>{props.text}</Text>
    </BannerButton>);
};

export default TextBannerButton;

const styles = StyleSheet.create({
    text: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: 'bold'
    }
});

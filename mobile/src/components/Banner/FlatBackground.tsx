import * as React from 'react';
import { View, StyleSheet } from 'react-native';

export interface IFlatBackgroundProps {

}

const FlatBackground: React.FC<IFlatBackgroundProps> = (props) => {
    return <View style={styles.container}>
        {props.children}
    </View>
};

export default FlatBackground;

const styles = StyleSheet.create({
    container: {
        padding: 16,
        marginBottom: 4,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#001064'
    },

});

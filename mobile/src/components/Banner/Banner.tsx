import * as React from 'react';
import { View, StyleSheet } from "react-native";

export interface IBannerProps {
    style?: any;
}

const Banner: React.FC<IBannerProps> = (props) => {
    return (<View style={styles.container}>
        <View style={StyleSheet.flatten([styles.panel, props.style])}>
            {props.children}
        </View>
    </View>);
};

export default Banner;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        marginTop: 4,
        marginBottom: 4
    },
    panel: {
        width: '90%',
        padding: 12,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#283593',
        borderRadius: 10
    }
});
import * as React from "react";

import {View, Text, StyleSheet} from "react-native";
import TextBannerButton from "../Banner/TextBannerButton";
import update from "immutability-helper";
import {IFirebaseContextProps, withFirebase} from "../Firebase";
import {IAsyncStorageContextProps, withAsyncStorage} from "../Session/GlobalContext";
import {NavigationStackProp} from "react-navigation-stack";
import TextBanner from "../Banner/TextBanner";

interface IDailyReportScreenProps extends IFirebaseContextProps, IAsyncStorageContextProps {
    navigation: NavigationStackProp<{}>;
}

interface IDailyReportScreenState {
    stage: "tiredness" | "grade" | "problems";
    tiredness: string;
    grade: string;
    balance: boolean;
    falls: boolean;
    freezing: boolean;
    tremor: boolean;
    other: boolean;
    alert_tiredness: boolean;
    alert_grade: boolean;
}

class DailyReportScreen extends React.Component <IDailyReportScreenProps, IDailyReportScreenState> {

    constructor(props: IDailyReportScreenProps) {
        super(props);

        this.state = {
            stage: "tiredness",
            tiredness: "",
            grade: "",
            balance: false,
            falls: false,
            freezing: false,
            tremor: false,
            other: false,
            alert_tiredness: false,
            alert_grade: false
        };

    }

    render() {
        let content;
        const {stage} = this.state;
        const {
            balance,
            falls,
            freezing,
            tremor,
            other
        } = this.state;

        if (stage === "tiredness") {
            content =
                <View style={styles.container}>
                    <TextBanner text="Jak się czujesz po wykonaniu ćwiczeń?"/>
                    <View style={styles.subcontainer}>
                        <TextBannerButton text="Brak zmęczenia"
                                          onPress={() => this.onSetTiredness("Brak zmęczenia", true)}/>
                        <TextBannerButton text="Trochę zmęczony"
                                          onPress={() => this.onSetTiredness("Trochę zmęczony", false)}/>
                        <TextBannerButton text="Bardzo zmęczony"
                                          onPress={() => this.onSetTiredness("Bardzo zmęczony", true)}/>
                    </View>
                </View>
        } else if (stage === "grade") {
            content =
                <View style={styles.container}>
                    <TextBanner text="Jak oceniasz dzisiejsze ćwiczenia?"/>
                    <View style={styles.subcontainer}>
                        <TextBannerButton text="Było za lekko" onPress={() => this.onSetGrade("Było za lekko", true)}/>
                        <TextBannerButton text="Było za trudno"
                                          onPress={() => this.onSetGrade("Było za trudno", true)}/>
                        <TextBannerButton text="Było w sam raz"
                                          onPress={() => this.onSetGrade("Było w sam raz", false)}/>
                        <TextBannerButton text="Było doskonale"
                                          onPress={() => this.onSetGrade("Było doskonale", false)}/>
                        <TextBannerButton text="Trudno powiedzieć"
                                          onPress={() => this.onSetGrade("Trudno powiedzieć", false)}/>
                    </View>
                </View>
        } else if (stage === "problems") {
            content =
                <View style={styles.container}>
                    <TextBanner text="Zaznacz problemy, które pojawiały się w trakcie ćwiczeń"/>
                    <View style={styles.subcontainer}>
                        <TextBannerButton style={{backgroundColor: balance ? '#283593' : "gray"}}
                                          text="Problemy z utrzymaniem równowagi"
                                          onPress={() => this.onSetProblem("balance")}/>
                        <TextBannerButton style={{backgroundColor: falls ? '#283593' : "gray"}} text="Upadki"
                                          onPress={() => this.onSetProblem("falls")}/>
                        <TextBannerButton style={{backgroundColor: freezing ? '#283593' : "gray"}}
                                          text="Stany zamrożenia" onPress={() => this.onSetProblem("freezing")}/>
                        <TextBannerButton style={{backgroundColor: tremor ? '#283593' : "gray"}} text="Drżenie"
                                          onPress={() => this.onSetProblem("tremor")}/>
                        <TextBannerButton style={{backgroundColor: other ? '#283593' : "gray"}} text="Inne problemy"
                                          onPress={() => this.onSetProblem("other")}/>
                    </View>
                    <TextBannerButton text={balance || falls || freezing || tremor || other ? "Prześlij" : "Brak problemów"} onPress={() => this.onEndTraining()}/>
                </View>
        }

        return content
    }

    onSetTiredness(option: string, alert: boolean) {
        this.setState(prevState => update(prevState, {
            tiredness: {$set: option},
            stage: {$set: "grade"},
            alert_tiredness: {$set: alert}
        }));
    }

    onSetGrade(option: string, alert: boolean) {
        this.setState(prevState => update(prevState, {
            grade: {$set: option},
            stage: {$set: "problems"},
            alert_grade: {$set: alert}
        }));
    }

    onSetProblem(option: string) {

        if (option === "balance") {
            this.setState(prevState => update(prevState, {
                balance: {$apply: b => !b},
            }));
        } else if (option === "falls") {
            this.setState(prevState => update(prevState, {
                falls: {$apply: b => !b},
            }));

        } else if (option === "freezing") {
            this.setState(prevState => update(prevState, {
                freezing: {$apply: b => !b},
            }));

        } else if (option === "tremor") {
            this.setState(prevState => update(prevState, {
                tremor: {$apply: b => !b},
            }));

        } else if (option === "other") {
            this.setState(prevState => update(prevState, {
                other: {$apply: b => !b},
            }));
        }

    }

    onEndTraining() {
        const {tiredness, grade, balance, falls, freezing, tremor, other, alert_grade, alert_tiredness} = this.state;
        const {firebase, asyncStorageData, navigation} = this.props;
        let problems = [];
        if (balance) {
            problems.push("Problemy z utrzymaniem równowagi")
        }
        if (falls) {
            problems.push("Upadki")
        }
        if (freezing) {
            problems.push("Stany zamrożenia")
        }
        if (tremor) {
            problems.push("Drżenie")
        }
        if (other) {
            problems.push("Inne")
        }

        const report = {
            tiredness: tiredness,
            grade: grade,
            problems: problems,
            alert: balance || falls || freezing || tremor || other || alert_grade || alert_tiredness,
            alertConfirmed: false
        };

        firebase.dailyReportSubmit(asyncStorageData.patientId!, report).then(() => {
            navigation.pop();
        }).catch(e => {
            console.warn("Failed to submit daily report: " + e.toString())
        })

    }

}

export default withAsyncStorage(withFirebase(DailyReportScreen));

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginBottom: 10,
        flexGrow: 1,
        flex: 1,
        //alignContent: 'center',
        justifyContent: 'space-between'
    },
    subcontainer: {
        flex: 1,
        justifyContent: 'center'
    }
});


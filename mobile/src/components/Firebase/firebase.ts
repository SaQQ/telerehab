import firebase from "@react-native-firebase/app";
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import '@react-native-firebase/functions';
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import { IPlanReport } from "@saqq/telerehab-infrastructure/lib/interfaces/plan";

class Firebase {

    public readonly auth = auth();
    public readonly firestore = firestore();
    public readonly storage = storage();
    public readonly functions = firebase.app().functions("europe-west3");

    private readonly _updateUserClaims: firebase.functions.HttpsCallable;
    private readonly _setExerciseStatus: firebase.functions.HttpsCallable;
    private readonly _setDailyReport: firebase.functions.HttpsCallable;

    public constructor() {
        this._updateUserClaims = this.functions.httpsCallable("updateUserClaims");
        this._setExerciseStatus = this.functions.httpsCallable("setExerciseStatus");
        this._setDailyReport = this.functions.httpsCallable("setDailyReport");
    }

    public async signInWithEmailAndPassword(email: string, password: string) {

        const userCredential = await this.auth.signInWithEmailAndPassword(email, password);

        if (userCredential.user === null) {
            throw new Error("Failed to sign in");
        }

        const updatedUserClaims = (await this._updateUserClaims()).data as boolean;
        if (!updatedUserClaims) {
            return userCredential;
        }

        const credential = firebase.auth.EmailAuthProvider.credential(email, password);
        return userCredential.user.reauthenticateWithCredential(credential);
    }

    public async signOut() {
        return this.auth.signOut();
    }

    public async createUserWithEmailAndPassword(email: string, password: string) {
        return this.auth.createUserWithEmailAndPassword(email, password);
    }

    public getPatientDocRef(patientId: string) {
        return this.firestore.collection(Collections.PATIENTS).doc(patientId);
    }

    public async startExercise(patientId: string, index: number, timestamp: Date) {
        const firebaseTimestamp = firebase.firestore.Timestamp.fromDate(timestamp);

        const data = {
            patientId,
            index,
            startTime: { seconds: firebaseTimestamp.seconds, nanoseconds: firebaseTimestamp.nanoseconds }
        };

        return this._setExerciseStatus(data);
    }

    public async dailyReportSubmit(patientId: string, report: IPlanReport) {
        const data = {
            patientId: patientId,
            report: report
        };

        return this._setDailyReport(data);
    }


    public async endExercise(patientId: string, index: number, timestamp: Date) {
        const firebaseTimestamp = firebase.firestore.Timestamp.fromDate(timestamp);

        const data = {
            patientId,
            index,
            endTime: { seconds: firebaseTimestamp.seconds, nanoseconds: firebaseTimestamp.nanoseconds }
        };

        return this._setExerciseStatus(data);
    }

    public async skipExercise(patientId: string, index: number, timestamp: Date, reason: string) {
        const firebaseTimestamp = firebase.firestore.Timestamp.fromDate(timestamp);

        const data = {
            patientId,
            index,
            endTime: { seconds: firebaseTimestamp.seconds, nanoseconds: firebaseTimestamp.nanoseconds },
            note: reason
        };

        return this._setExerciseStatus(data);
    }

    public async gradeExercise(patientId: string, index: number, grade: number) {

        const data = {
            patientId,
            index,
            grade
        };

        return this._setExerciseStatus(data);
    }

    public async getStorageURL(path: string) {
        return this.storage.ref(path).getDownloadURL();
    }

}

export default Firebase;

import * as React from 'react';
import {Text, StyleSheet, Image} from 'react-native';

import care from './care.png';

import FlatBackground from "../Banner/FlatBackground";

const styles = StyleSheet.create({
    text: {
        color: '#ffffff',
        fontSize: 24
    },
    image: {

    }
});

const GreetingHeader = () => (
    <FlatBackground>
        <Image style={styles.image} source={care}/>
        <Text style={styles.text}>Asystent pacjenta</Text>
    </FlatBackground>
);

export default GreetingHeader;

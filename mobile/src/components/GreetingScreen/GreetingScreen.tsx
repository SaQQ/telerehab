import * as React from 'react';
import { View, StyleSheet, Text, TextInput, AsyncStorage, ToastAndroid } from 'react-native';

import lock from './lock.png';

import update from 'immutability-helper';

import GreetingHeader from "./GreetingHeader";
import GreetingButton from "./GreetingButton";
import { NavigationStackOptions, NavigationStackProp } from "react-navigation-stack";
import { IAsyncStorageContextProps, withAsyncStorage } from "../Session/GlobalContext";
import Routes from "../../constants/routes";
import Tts from 'react-native-tts';

interface IGreetingsScreenProps extends IAsyncStorageContextProps {
    navigation: NavigationStackProp<{}>;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
    },
    bannerContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        height: 40,
        width: '90%',
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 4,
        marginBottom: 4
    },
});

interface IGreetingsScreenState {
    patientId: string;
}

class GreetingsScreen extends React.Component<IGreetingsScreenProps, IGreetingsScreenState> {

    constructor(props: IGreetingsScreenProps) {
        super(props);
        this.state = {
            patientId: ""
        };
    }

    componentDidMount() {
        const { asyncStorageData, navigation } = this.props;
        if (asyncStorageData.patientId) {
            navigation.replace(Routes.MAIN_MENU);
        }

        Tts.getInitStatus().then(() => {
            Tts.setDefaultLanguage('pl-PL');
            Tts.speak('Witaj!');
        }, (err) => {
            if (err.code === 'no_engine') {
                Tts.requestInstallEngine();
            }
        });

    }

    componentDidUpdate() {
        const { asyncStorageData, navigation } = this.props;
        if (asyncStorageData.patientId) {
            navigation.replace(Routes.MAIN_MENU);
        }
    }

    render() {
        const { patientId } = this.state;

        return (
            <View style={styles.container}>
                <GreetingHeader/>
                <View style={styles.bannerContainer}>
                    <TextInput
                        style={styles.textInput}
                        autoCapitalize="none"
                        placeholder="Identyfikator Pacjenta"
                        onChangeText={id => this.setPatientID(id)}
                        value={patientId}
                    />
                    <GreetingButton text="Zaloguj" img={lock} onPress={() => this.LogIn()}/>
                    <Text>Icons made by Freepik from www.flaticon.com</Text>
                </View>
            </View>)

    }

    setPatientID(id: string) {
        this.setState((prevState) => update(prevState, {
            patientId: { $set: id }
        }));
    }

    LogIn() {
        const { asyncStorageData, navigation } = this.props;
        const { patientId } = this.state;

        asyncStorageData.setPatientId(patientId).then(() => {

        }).catch(e => {
            ToastAndroid.show(e.toString(), ToastAndroid.SHORT);
        });
    }

    LogOut() {

    }

}

export default withAsyncStorage(GreetingsScreen);

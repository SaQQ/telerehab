import * as React from "react";

import update from "immutability-helper";
import AsyncStorage from "@react-native-community/async-storage";
import { IFirebaseContextProps, withFirebase } from "../Firebase";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";

export interface AsyncStorageData {
    patientId: string | null;

    setPatientId: (id: string | null) => Promise<void>;
}

export interface IAsyncStorageContextProps {
    asyncStorageData: AsyncStorageData
}

type Omitted<P> = Pick<P, Exclude<keyof P, keyof IAsyncStorageContextProps>>;

export const AsyncStorageContext = React.createContext<AsyncStorageData | null>(null);

export function withAsyncStorage<P extends IAsyncStorageContextProps>(Component: React.ComponentType<P>): React.ComponentType<Omitted<P>> {
    return (props: Omitted<IAsyncStorageContextProps>) => {
        return (<AsyncStorageContext.Consumer>
            {asyncStorageData => <Component {...props as P} asyncStorageData={asyncStorageData}/>}
        </AsyncStorageContext.Consumer>);
    }
}

interface IAsyncStorageProviderProps extends IFirebaseContextProps {

}

interface IAsyncStorageProviderState {
    patientId: string | null;
}

class AsyncStorageProviderBase extends React.Component<IAsyncStorageProviderProps, IAsyncStorageProviderState> {

    public constructor(props: IAsyncStorageProviderProps) {
        super(props);

        this.state = {
            patientId: null
        };
    }

    async componentDidMount() {

        const patientId = await AsyncStorage.getItem('patientId');

        this.setState(state => update(state, {
            patientId: { $set: patientId }
        }));

    }

    async setPatientId(id: string | null) {
        if (id) {
            const { firebase } = this.props;
            const docRef = firebase.firestore.collection(Collections.PATIENTS).doc(id);
            const doc = await docRef.get();
            if (!doc.exists) {
                throw new Error("Pacjent o podanym ID nie istnieje");
            }
            await AsyncStorage.setItem('patientId', id);
        } else {
            await AsyncStorage.removeItem('patientId');
        }
        this.setState(state => update(state, {
            patientId: { $set: id }
        }));
    }

    render() {
        const { patientId } = this.state;

        return (
            <AsyncStorageContext.Provider value={
                {
                    patientId: patientId,

                    setPatientId: (id) => this.setPatientId(id)
                }
            }>
                {this.props.children}
            </AsyncStorageContext.Provider>
        );
    }

}

export const AsyncStorageProvider = withFirebase(AsyncStorageProviderBase);

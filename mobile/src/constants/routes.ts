class Routes {
    static readonly MAIN_MENU = "MainMenu";
    static readonly CALENDAR = "Calendar";
    static readonly EXAMINATION = "Examination";
    static readonly SURVEY = "SurveyScreen";
    static readonly GREETING_SCREEN = "GreetingScreen";
    static readonly DAILY_REPORT = "DailyReport";
    static readonly PLAN = "Plan";
}

export default Routes;

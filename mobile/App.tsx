/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import * as React from 'react';

import {createAppContainer} from "react-navigation";
import {createStackNavigator} from 'react-navigation-stack';


import MainMenu from './src/components/MainMenu/MainMenu';
import Calendar from './src/components/Calendar/Calendar';
import Examination from './src/components/Examination/Examination';
import GreetingScreen from './src/components/GreetingScreen/GreetingScreen'
import PlanScreen from './src/components/Plan';

import Firebase, {FirebaseContext} from './src/components/Firebase';
import Routes from "./src/constants/routes";
import {AsyncStorageProvider} from "./src/components/Session/GlobalContext";
import {DayPlanProvider} from "./src/components/DayPlanContext";
import DailyReportScreen from "./src/components/DailyReport/DailyReport";

const AppNavigator = createStackNavigator(
    {
        [Routes.GREETING_SCREEN]: {
            screen: GreetingScreen,
            navigationOptions: {header: null}
        },
        [Routes.MAIN_MENU]: {
            screen: MainMenu,
            navigationOptions: {header: null}
        },
        [Routes.CALENDAR]: {
            screen: Calendar,
            navigationOptions: {title: "Kalendarz ćwiczeń"}
        },
        [Routes.EXAMINATION]: {
            screen: Examination,
            navigationOptions: {header: null}
        },
        [Routes.PLAN]: {
            screen: PlanScreen,
            navigationOptions: {title: "Plan ćwiczeń"}
        },
        [Routes.DAILY_REPORT]: {
            screen: DailyReportScreen,
            navigationOptions: {title: "Raport dzienny"}
        }
    },
    {
        initialRouteName: Routes.GREETING_SCREEN
    }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {

    render() {
        return (
            <FirebaseContext.Provider value={new Firebase()}>
                <AsyncStorageProvider>
                    <DayPlanProvider>
                        <AppContainer/>
                    </DayPlanProvider>
                </AsyncStorageProvider>
            </FirebaseContext.Provider>
        );
    }

}

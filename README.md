# Preparation 

* Install `npm` for your distribution (i. e. `sudo apt install npm`)
* Install firebase tools from https://firebase.tools/
* Install JDK 8 for your distribution (i. e. `sudo apt install openjdk-8-jdk-headless`)
* Run `npm install` in web functions
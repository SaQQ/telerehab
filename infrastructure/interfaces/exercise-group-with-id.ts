import IExerciseGroup from "./exercise-group";
import IWithID from "./with-id";

export default interface IExerciseGroupWithID extends IExerciseGroup, IWithID {
}

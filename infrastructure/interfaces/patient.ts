import * as firebase from "firebase";

export default interface IPatient {
    name: string;
    lastName: string;
    therapistUID: string;
}

export const EMPTY_PATIENT: IPatient = {
    name: "",
    lastName: "",
    therapistUID: ""
};
import * as firebase from "firebase";


export default interface IExercise {
    code: string;
    name: string;
    iconData: string | null,
    requirements: string;
    description: string;
    instructions: string;
    youTubeId: string;
    exerciseGroupRefs: firebase.firestore.DocumentReference[];
    photoPaths: string[];
}

export const EMPTY_EXERCISE: IExercise = {
    code: "",
    name: "",
    iconData: null,
    description: "",
    instructions: "",
    requirements: "",
    youTubeId: "",
    exerciseGroupRefs: [],
    photoPaths: []
};

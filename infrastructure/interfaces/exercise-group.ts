export default interface IExerciseGroup {
    name: string;
}

export const EMPTY_EXERCISE_GROUP: IExerciseGroup = {
    name: "",
};

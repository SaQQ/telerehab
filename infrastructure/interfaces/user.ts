export default interface IUser {
    email: string;
    displayName: string;
}

export const EMPTY_USER: IUser = {
    email: "",
    displayName: "",
};
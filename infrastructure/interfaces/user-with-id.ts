import IUser from "./user";
import IWithID from "./with-id";

export default interface IUserWithID extends IUser, IWithID {
}
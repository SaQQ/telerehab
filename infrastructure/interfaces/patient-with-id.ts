import IPatient from "./patient";
import IWithID from "./with-id";

export default interface IPatientWithID extends IPatient, IWithID {
}
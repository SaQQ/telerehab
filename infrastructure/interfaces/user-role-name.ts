export default interface UserRoleName {
    name: string;
}

export const EMPTY_USER_ROLE_NAME: UserRoleName = {
    name: ""
};

import * as firebase from "firebase";

export default interface IUserRole {
    roles: firebase.firestore.DocumentReference[];
}

export const EMPTY_USER_ROLE: IUserRole = {
    roles: []
};

import IExercise from "./exercise";
import IWithID from "./with-id";

export default interface IExerciseWithID extends IExercise, IWithID {
}

import IUserRole from "./user-role";
import IWithID from "./with-id";

export default interface IUserRoleWithID extends IUserRole, IWithID {
} 

import IPlanItem from "./plan-item";
import * as firebase from "firebase";

export interface IPlanReport {
    tiredness: string;
    grade: string;
    problems: string[];
    alert: boolean;
    alertConfirmed: boolean;
}

export interface IPlanStatus {
    startTime: firebase.firestore.Timestamp | null;
    endTime: firebase.firestore.Timestamp | null;
    report: IPlanReport | null;
}

export const EMPTY_PLAN_STATUS: IPlanStatus = {
    startTime: null,
    endTime: null,
    report: null
};

export default interface IPlan {
    exercises: IPlanItem[];
    status: IPlanStatus;
}

export const EMPTY_PLAN: IPlan = {
    exercises: [],
    status: EMPTY_PLAN_STATUS
};

import UserRoleName from "./user-role-name";
import IWithID from "./with-id";

export default interface UserRoleNameWithID extends UserRoleName, IWithID {
}

import  * as firebase from "firebase";

export interface IExerciseStatus {
    startTime: firebase.firestore.Timestamp | null;
    endTime: firebase.firestore.Timestamp | null;
    grade: number;
    note: string;
}

export const EMPTY_EXERCISE_STATUS: IExerciseStatus = {
    startTime: null,
    endTime: null,
    grade: 0,
    note: ""
};

interface IPlanItemWithoutRef {
    repetitions: number;
    timeLimit: number;
    status: IExerciseStatus;
}

export default interface IPlanItem extends IPlanItemWithoutRef {
    exercise: firebase.firestore.DocumentReference;
}

export const EMPTY_PLAN_ITEM: IPlanItemWithoutRef = {
    repetitions: 0,
    timeLimit: 0,
    status: EMPTY_EXERCISE_STATUS
};

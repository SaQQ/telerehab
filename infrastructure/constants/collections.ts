class Collections {
    static readonly USERS = "users";
    static readonly PATIENTS = "patients";
    static readonly EXERCISES = "exercises";
    static readonly EXERCISE = (exerciseID: string) => `/${Collections.EXERCISES}/${exerciseID}`;
    static readonly EXERCISE_GROUPS = "exercise_groups";
    static readonly PATIENT_PLANS = (patientID: string) => `${Collections.PATIENTS}/${patientID}/plans`;
    
    /**
     * List of all the available roles.
     * Document ID is the name of role.
     * Document "name" field is the display name of the role.
     */
    static readonly USER_ROLE_NAMES = "user_role_names";
    
    /**
     * Table mapping user ID to the list of roles.
     */
    static readonly USER_ROLES = "user_roles";
}

export default Collections;

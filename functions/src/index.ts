import firebase from "firebase";
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

import update, { Spec } from "immutability-helper";

import IUserRole, { EMPTY_USER_ROLE } from "@saqq/telerehab-infrastructure/lib/interfaces/user-role";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import IPlan, { EMPTY_PLAN } from "@saqq/telerehab-infrastructure/lib/interfaces/plan";
import { IExerciseStatus } from "@saqq/telerehab-infrastructure/lib/interfaces/plan-item";


const firebaseConfig = {
    apiKey: "AIzaSyAi6cm12ZnAIt0gbHxpO9Zg3Ym_UT08_sY",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: ""
};
firebase.initializeApp(firebaseConfig);
admin.initializeApp();

const regionalFunctions = functions.region('europe-west3');

exports.createProfile = regionalFunctions.auth.user().onCreate(async (user) => {

    const userData = {
        displayName: user.displayName,
        email: user.email,
    };
    await admin.firestore().collection(Collections.USERS).doc(user.uid).set(userData);

    const userRole = EMPTY_USER_ROLE;
    await admin.firestore().collection(Collections.USER_ROLES).doc(user.uid).set(userRole);
});

exports.deleteProfile = regionalFunctions.auth.user().onDelete(async (user) => {
    await admin.firestore().collection(Collections.USERS).doc(user.uid).delete();
    await admin.firestore().collection(Collections.USER_ROLES).doc(user.uid).delete();
});

function hasClaimsChanged(oldClaims: string[], newClaims: string[]) {
    if (oldClaims.length === newClaims.length) {
        if (newClaims.every(newClaim => oldClaims.indexOf(newClaim) >= 0)) {
            return false;
        }
    }

    return true;
}

async function updateUserClaims(userID: string, userRole: IUserRole) {

    // Get old user claims (saved in user token)
    const userRecord = await admin.auth().getUser(userID);
    const oldClaimsObject: any = (userRecord.customClaims === undefined) ? {} : userRecord.customClaims;
    const oldClaims = Object.keys(oldClaimsObject).filter(key => key.startsWith("is_") && oldClaimsObject[key]);

    // Get new user claims
    const newClaims = userRole.roles.map(role => `is_${role.id}`);

    // Check if claims changed
    if (!hasClaimsChanged(oldClaims, newClaims)) {
        return false;
    }

    // Set new user claims
    const newClaimsObject = newClaims.reduce((currClaims, claim) => {
        return { ...currClaims, [claim]: true };
    }, {});

    await admin.auth().setCustomUserClaims(userID, newClaimsObject);

    // Invalidate user token, so they have to reauthenticate and update claims
    await admin.auth().revokeRefreshTokens(userID);

    return true;
}

exports.rolesChanged = regionalFunctions.firestore.document(`${Collections.USER_ROLES}/{userID}`).onUpdate(async (change, context) => {
    const { userID } = context.params;

    // Get new user roles
    const userRole: IUserRole = { ...EMPTY_USER_ROLE, ...change.after.data() as IUserRole };

    await updateUserClaims(userID, userRole);
});

exports.updateUserClaims = regionalFunctions.https.onCall(async (data, context) => {
    if (!context.auth) {
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');
    }

    const userID = context.auth.uid;

    // Get user roles from database
    const doc = await admin.firestore().collection(Collections.USER_ROLES).doc(userID).get();
    const userRole: IUserRole = { ...EMPTY_USER_ROLE, ...doc.data() as IUserRole };

    updateUserClaims(userID, userRole);

    return {};
});

function getDateString(date: Date) {
    return `${date.getFullYear().toString().padStart(4, '0')}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
}

function validateTimestamp(value: any, argName: string) {
    if (typeof value !== 'object') {
        throw new functions.https.HttpsError('invalid-argument', `The function must be called with argument "${argName}" of type { seconds: number, nanoseconds: number }`);
    }
    const seconds = value.seconds;
    const nanoseconds = value.nanoseconds;
    if (!(Number.isInteger(seconds)) || seconds < 0) {
        throw new functions.https.HttpsError('invalid-argument', `The function must be called with integer argument "${argName}.seconds" greater than 0`);
    }
    if (!(Number.isInteger(nanoseconds)) || nanoseconds < 0) {
        throw new functions.https.HttpsError('invalid-argument', `The function must be called with integer argument "${argName}.nanoseconds" greater than 0`);
    }

    return new admin.firestore.Timestamp(seconds, nanoseconds);
}

exports.setExerciseStatus = regionalFunctions.https.onCall(async (data, context) => {

    const patientId = data.patientId;
    const index = data.index;
    const startTime = data.startTime;
    const endTime = data.endTime;
    const grade = data.grade;
    const note = data.note;

    let statusUpdater: Spec<IExerciseStatus> = {};

    if (startTime) {
        statusUpdater.startTime = { $set: validateTimestamp(startTime, "startTime") };
    }

    if (endTime) {
        statusUpdater.endTime = { $set: validateTimestamp(endTime, "endTime") };
    }

    if (grade) {
        if (!(Number.isInteger(grade))) {
            throw new functions.https.HttpsError('invalid-argument', 'The function must be called with integer argument "grade"');
        }
        statusUpdater.grade = { $set: grade };
    }

    if (note) {
        statusUpdater.note = { $set: String(note) };
    }

    if (!(Number.isInteger(index)) || index < 0) {
        throw new functions.https.HttpsError('invalid-argument', 'The function must be called with argument "index" greater than 0.');
    }

    const ref = await admin.firestore().collection(Collections.PATIENT_PLANS(patientId)).doc(getDateString(new Date()));
    const doc = await ref.get();
    const plan: IPlan = { ...EMPTY_PLAN, ...doc.data() as IPlan };

    if (!doc.exists) {
        throw new functions.https.HttpsError('invalid-argument', 'Specified patient does not have plan assigned');
    }

    if (index >= plan.exercises.length) {
        throw new functions.https.HttpsError('invalid-argument', 'The function must be called with integer argument "index" less than in today\'s plan items count.');
    }

    const newPlan = update(plan, {
        exercises: {
            [index]: {
                status: statusUpdater
            }
        }
    });

    await ref.update(newPlan);

    return {};
});

exports.setDailyReport = regionalFunctions.https.onCall(async (data, context) => {

    const patientId = data.patientId;
    const report = data.report;

    if (typeof report !== 'object') {
        throw new functions.https.HttpsError('invalid-argument', `The function must be called with argument report of type IPlanReport`);
    }

    const { tiredness, grade, problems, alert, alertConfirmed } = report;

    if (typeof tiredness !== 'string' || typeof grade !== 'string' || !Array.isArray(problems) || typeof alert !== 'boolean' || typeof alertConfirmed !== 'boolean') {
        throw new functions.https.HttpsError('invalid-argument', `The function must be called with argument report of type IPlanReport`);
    }

    if (!problems.every(s => typeof s === 'string')) {
        throw new functions.https.HttpsError('invalid-argument', `The function must be called with argument report of type IPlanReport`);
    }

    const ref = await admin.firestore().collection(Collections.PATIENT_PLANS(patientId)).doc(getDateString(new Date()));
    const doc = await ref.get();
    const plan: IPlan = { ...EMPTY_PLAN, ...doc.data() as IPlan };

    const newPlan = update(plan, {
        status: {
            report: { $set: report }
        }
    });

    await ref.update(newPlan);

    return {};
});


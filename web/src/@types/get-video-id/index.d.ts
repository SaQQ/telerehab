declare module "get-video-id" {
    export default function getVideoId(url: string): Metadata;
}

interface Metadata {
    id: string;
    service: string;
}

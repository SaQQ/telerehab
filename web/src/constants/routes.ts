class Routes {
    static readonly HOME = "/";
    static readonly SIGN_UP = "/signup";
    static readonly SIGN_IN = "/signin";
    static readonly EXERCISES = "/exercises";
    static readonly EXERCISE_GROUPS = "/exercise_groups";
    static readonly EXERCISE = "/exercise/:id";
    static readonly NEW_EXERCISE = "/new_exercise";
    static readonly USERS = "/users";
    static readonly USER = "/user/:id";
    static readonly PATIENTS = "/patients";
    static readonly PATIENT = "/patient/:id";
    static readonly ACCOUNT = "/account";
    static readonly ADMIN = "/admin";

    static readonly ExerciseRoute = (id: string) => `/exercise/${id}`;
    static readonly UserRoute = (id: string) => `/user/${id}`;
    static readonly PatientRoute = (id: string) => `/patient/${id}`;
}

export default Routes;

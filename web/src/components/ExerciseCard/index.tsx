import React from "react";

import { Card, Intent, Text, Divider, H3, H5, Classes, Tag, ControlGroup } from "@blueprintjs/core";
import Routes from "../../constants/routes";
import { Link } from "react-router-dom";

import update from "immutability-helper";
import { Video } from "../Media";
import IExerciseWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-with-id";
import IExerciseGroupWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group-with-id";
import { IAuthUserContextProps, withAuthUser } from "../Session/context";
import strings from "../../constants/strings";

import "./styles.css"
import { TextWithPhotos } from "../ExerciseDetails";
import { IFirebaseContextProps, withFirebase } from "../Firebase";
import { withNotifications, INotificationProps } from "../Notifications";


interface IExerciseCardProps extends IAuthUserContextProps, IFirebaseContextProps, INotificationProps {
    exercise: IExerciseWithID;
    exerciseGroups: (IExerciseGroupWithID | null)[];
}

interface IExerciseCardState {
    expanded: boolean;
    photoSources: string[] | null;
}

class ExerciseCardBase extends React.Component<IExerciseCardProps, IExerciseCardState> {
    public constructor(props: IExerciseCardProps) {
        super(props);

        this.state = { 
            expanded: false,
            photoSources: null
        };
    }

    onClicked() {
        this.setState(state => update(state, { expanded: { $apply: x => !x } }), async () => {
            if (this.state.expanded && this.state.photoSources === null) {
                // Load photos
                const photoSources = await Promise.all(this.props.exercise.photoPaths.map(async photoPath => {
                    try {
                        const photoSource = await this.props.firebase.storage.ref(photoPath).getDownloadURL() as Promise<string>;
                        return photoSource;
                    } catch(err) {
                        if (err.message !== undefined) {
                            this.props.notifications.showError(`${strings.error_loading_photo_file}: ${err.message}`);
                        }
                        return "";
                     }
                }));

                this.setState(state => update(state, {
                    photoSources: { $set: photoSources }
                }))
            }
        });
    }

    render() {
        const { exercise, exerciseGroups, authUserData } = this.props;
        const { expanded } = this.state;
        

        const hasVideo = (exercise.youTubeId !== "");
        
        return (
            <Card interactive onClick={() => this.onClicked()}>
                <ControlGroup>
                    {exercise.iconData && <img src={exercise.iconData} style={{maxWidth: "2em", maxHeight: "2em", marginRight: "1em"}} alt="icon"/>}
                    <H3 style={{width: "100%"}}>{exercise.code}<span style={{marginLeft: "0.5em"}} />{exercise.name}</H3>
                </ControlGroup>
                {exerciseGroups.map((group, index) => <Tag className="exercise-group" intent={group !== null ? Intent.NONE : Intent.WARNING} key={index}>{group !== null ? group.name : `<${strings.unknown_tag}>`}</Tag>)}
                {expanded && <Divider/>}
                {expanded && exercise.requirements && (
                    <div>
                        <H5>
                            {`${strings.requirements}:`}
                        </H5>
                        <p>
                            {exercise.requirements}
                        </p>
                        <H5>
                            {`${strings.instructions}:`}
                        </H5>
                        <TextWithPhotos 
                            text={exercise.instructions}
                            photoSources={this.state.photoSources !== null ? this.state.photoSources : exercise.photoPaths.map(_ => "")}
                        />
                    </div>
                )}
                <Text ellipsize={!expanded}>{exercise.description}</Text>
                {expanded && <Divider/>}
                {expanded && hasVideo && <Video youTubeId={exercise.youTubeId} />}
                {expanded && authUserData && <Link className={Classes.BUTTON} to={Routes.ExerciseRoute(exercise.id)}>{strings.details}</Link>}
            </Card>
        );
    }
}

const ExerciseCard = withNotifications(withFirebase(withAuthUser(ExerciseCardBase)));

export default ExerciseCard;

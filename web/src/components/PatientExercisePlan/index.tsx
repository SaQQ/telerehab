import React from "react";

import update from "immutability-helper";

import { Col, Container, Row, Visible } from "react-grid-system";

import { RouteComponentProps, withRouter } from "react-router";

import { ProgressBar } from "@blueprintjs/core";
import { DatePicker } from "@blueprintjs/datetime";

import { withFirebase, IFirebaseContextProps } from "../Firebase";
import { INotificationProps, withNotifications } from "../Notifications";

import IExercise, { EMPTY_EXERCISE } from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";
import IExerciseWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-with-id";
import IPatient, { EMPTY_PATIENT } from "@saqq/telerehab-infrastructure/lib/interfaces/patient";
import IPatientWithID from "@saqq/telerehab-infrastructure/lib/interfaces/patient-with_id";

import PlanDayCard from './PlanDayCard';

import Routes from "../../constants/routes";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import strings from "../../constants/strings";
import DateUtils from "../../utils/date-utils";

import "./index.css";
import IPlan from "@saqq/telerehab-infrastructure/lib/interfaces/plan";
import { CancellablePromiseComponent, PromiseCancelled } from "../../utils/cancellable-promise";

import MomentLocaleUtils from "react-day-picker/moment";

import "moment/locale/pl";
import "moment/locale/en-gb";


interface IPatientExercisePlanPageProps extends IFirebaseContextProps, RouteComponentProps<any>, INotificationProps {

}

interface IPatientExercisePlanPageState {
    loading: boolean;
    patient: IPatientWithID;
    selectedDate: Date;
    exercises: IExerciseWithID[];
    notEmptyDatesDict: { [key: string]: boolean };
}

class PatientExercisePlanPageBase extends CancellablePromiseComponent<IPatientExercisePlanPageProps, IPatientExercisePlanPageState> {
    constructor(props: IPatientExercisePlanPageProps) {
        super(props);

        const today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

        this.state = {
            loading: false,
            patient: {
                ...EMPTY_PATIENT,
                id: ""
            },
            selectedDate: today,
            exercises: [],
            notEmptyDatesDict: {}
        };
    }

    async componentDidMount() {
        const { firebase, notifications, history, match } = this.props;
        const { notEmptyDatesDict } = this.state;
        const patientID = match.params.id;

        this.setState(state => update(state, { loading: { $set: true } }));

        try {
            const patientDoc = await this.addCancelablePromise(firebase.firestore.collection(Collections.PATIENTS).doc(patientID).get());
            if (!patientDoc.exists) {
                throw new Error(strings.formatString(strings.user_with_id_X_not_found, patientID) as string);
            }

            const patient: IPatientWithID = {
                ...EMPTY_PATIENT,
                ...patientDoc.data() as IPatient,
                id: patientDoc.id,
            };

            this.setState(state => update(state, { patient: { $set: patient } }));

            const exercisesSnapshot = await this.addCancelablePromise(firebase.firestore.collection(Collections.EXERCISES).orderBy("name").get());
            const exercises: IExerciseWithID[] = exercisesSnapshot.docs.map((doc) => ({
                ...EMPTY_EXERCISE,
                ...doc.data() as IExercise,
                id: doc.id,
            }));

            this.setState(state => update(state, { exercises: { $set: exercises } }));
        } catch (exc) {
            if (exc instanceof PromiseCancelled) {
                return;
            } else if (exc instanceof Error) {
                notifications.showError(`${strings.could_not_download_data}: ${exc.message}`);
                history.push(Routes.PATIENTS);
            }
        }

        this.setState(state => update(state, { loading: { $set: false } }));


        // Set non-empty dates
        const patientPlans = await firebase.firestore.collection(Collections.PATIENT_PLANS(patientID)).get();
        patientPlans.docs.forEach(doc => {
            notEmptyDatesDict[doc.id] = true;
        });

        this.setState(state => update(state, {
            notEmptyDatesDict: { $merge: notEmptyDatesDict }
        }))
    }

    private onDateChanged(date: Date) {
        this.setState(state => update(state, { selectedDate: { $set: date } }));
    }

    static addDays(date: Date, days: number) {
        const result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    isNotEmpty(date: Date) {
        const { notEmptyDatesDict } = this.state;
        const dayStr = DateUtils.getDateString(date);

        return (dayStr in notEmptyDatesDict) && notEmptyDatesDict[dayStr];
    }

    onPlanChanged(date: Date, plan: IPlan) {
        const dayStr = DateUtils.getDateString(date);

        this.setState(state => update(state, {
            notEmptyDatesDict: { $merge: { [dayStr]: plan.exercises.length > 0 } }
        }));
    }

    render() {
        const { match } = this.props;
        const { loading, selectedDate, exercises } = this.state;

        const d1 = PatientExercisePlanPageBase.addDays(selectedDate, -1);
        const d2 = PatientExercisePlanPageBase.addDays(selectedDate, +0);
        const d3 = PatientExercisePlanPageBase.addDays(selectedDate, +1);

        const modifiers = { isNotEmpty: this.isNotEmpty.bind(this) };

        return (
            <Container>
                <Row>
                    <Col sm={12}>
                        {loading && <ProgressBar/>}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <DatePicker
                            modifiers={modifiers}
                            canClearSelection={false}
                            value={selectedDate}
                            onChange={newDate => this.onDateChanged(newDate)}
                            locale={strings.localization_string}
                            localeUtils={MomentLocaleUtils}
                        />
                    </Col>
                    <Visible lg xl>
                        <Col>
                            <PlanDayCard 
                                key={d2.toString()} 
                                patientID={match.params.id} 
                                day={d1} 
                                exercises={exercises} 
                                onPlanChanged={(plan) => this.onPlanChanged(d1, plan)}
                                notEmptyDatesDict={this.state.notEmptyDatesDict}
                            />
                        </Col>
                    </Visible>
                    <Col>
                        <PlanDayCard 
                            emphasized 
                            key={d1.toString()} 
                            patientID={match.params.id} 
                            day={d2} 
                            exercises={exercises} 
                            onPlanChanged={(plan) => this.onPlanChanged(d2, plan)} 
                            notEmptyDatesDict={this.state.notEmptyDatesDict}
                        />
                    </Col>
                    <Visible lg xl>
                        <Col>
                            <PlanDayCard 
                                key={d3.toString()} 
                                patientID={match.params.id} 
                                day={d3} exercises={exercises} 
                                onPlanChanged={(plan) => this.onPlanChanged(d3, plan)} 
                                notEmptyDatesDict={this.state.notEmptyDatesDict}
                            />
                        </Col>
                    </Visible>
                </Row>
            </Container>
        );
    }
}

const PatientExercisePlanPage = withNotifications(withRouter(withFirebase(PatientExercisePlanPageBase)));

export default PatientExercisePlanPage;

import React from "react";

import update from "immutability-helper";

import {
    Button,
    ButtonGroup,
    Card,
    FormGroup,
    ControlGroup,
    Text,
    Icon,
    NumericInput,
    Intent
} from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import IExercise from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";
import IExerciseWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-with-id";
import IPlanItem, {
    IExerciseStatus
} from "@saqq/telerehab-infrastructure/lib/interfaces/plan-item";

import strings from "../../constants/strings";

import './index.css';

type ExerciseStatus = "not_started" | "started" | "done" | "skipped";

function getExerciseStatus(status: IExerciseStatus): ExerciseStatus {
    if (status.startTime) {
        if (status.endTime) {
            return "done";
        } else {
            return "started";
        }
    } else {
        if (status.endTime) {
            return "skipped";
        } else {
            return "not_started";
        }
    }
}

interface IStarGradingProps {
    max: Number;
    value: Number;
}

const StarGrading = (props: IStarGradingProps) => {
    const { max, value } = props;
    return <ControlGroup>
        {
            [<Text>{`${strings.exercise_grade}: `}</Text>].concat(
                Array.from(Array(max).keys()).map(i => <Icon icon={i < value ? IconNames.STAR : IconNames.STAR_EMPTY}
                                                             iconSize={16} style={{ marginLeft: 4 }}/>))
        }
    </ControlGroup>;
};

interface IPlanCardProps {
    planItem: IPlanItem;
    onDuplicate?: () => void;
    onDelete?: () => void;
    onMoveUp?: () => void;
    onMoveDown?: () => void;
    onSave?: (repetitions: number, timeLimit: number) => void;
}

interface IPlanCardState {
    loading: boolean;
    exercise: IExerciseWithID | null;
    repetitions: number;
    timeLimit: number;
}

class PlanCard extends React.Component<IPlanCardProps, IPlanCardState> {
    constructor(props: IPlanCardProps) {
        super(props);
        this.state = {
            loading: false,
            exercise: null,
            repetitions: 0,
            timeLimit: 0
        };
    }

    componentDidMount() {
        const { planItem } = this.props;
        planItem.exercise
            .get()
            .then((doc) => {
                if (doc.exists) {
                    this.setState(state => update(state, {
                        exercise: {
                            $set: {
                                id: doc.id,
                                ...doc.data() as IExercise
                            }
                        },
                        repetitions: { $set: planItem.repetitions },
                        timeLimit: { $set: planItem.timeLimit }
                    }));
                }
            })
            .finally(() => {
                this.setState(state => update(state, { loading: { $set: false } }));
            })
            .catch();
    }

    render() {
        const { onDelete, onDuplicate, onMoveUp, onMoveDown, onSave, planItem } = this.props;
        const { exercise, repetitions, timeLimit } = this.state;
        const { status } = planItem;

        if (!exercise) {
            return (<Card className="exercise-plan-card">
                {!exercise && strings.unknown_exercise}
            </Card>);
        }

        const header = (exercise.code ? exercise.code + " " : "") + exercise.name;

        const exerciseStatus = getExerciseStatus(status);

        const savePossible = planItem.repetitions !== repetitions || planItem.timeLimit !== timeLimit;

        return (
            <Card className={"exercise-plan-card exercise-plan-card--" + exerciseStatus}>
                <FormGroup label={header} style={{ margin: 0 }}>
                    <Text>{strings.status + ": " + strings[exerciseStatus]}</Text>
                    {status.startTime &&
                    <Text>{`${strings.start_time}: ${status.startTime.toDate().toLocaleTimeString()}`}</Text>
                    }
                    {status.endTime &&
                    <Text>{`${strings.end_time}: ${status.endTime.toDate().toLocaleTimeString()}`}</Text>
                    }
                    {status.startTime && status.endTime &&
                    <StarGrading max={5} value={status.grade}/>
                    }
                    {status.note &&
                    <Text>{`${strings.plan_item_note}: ${status.note}`}</Text>
                    }
                    {(exerciseStatus === "not_started") &&
                    [
                        <ControlGroup fill vertical>
                            <NumericInput fill placeholder={strings.customization_repetitions} min={0} max={100}
                                          value={repetitions > 0 ? repetitions : ""}
                                          intent={repetitions > 0 ? Intent.PRIMARY : Intent.NONE}
                                          onValueChange={repetitions => this.onRepetitionsChanged(repetitions)}/>
                            <NumericInput fill placeholder={`${strings.customization_duration} [min]`} min={0} max={3600}
                                          value={timeLimit > 0 ? timeLimit : ""}
                                          intent={timeLimit > 0 ? Intent.PRIMARY : Intent.NONE}
                                          onValueChange={timeLimit => this.onTimeLimitChanged(timeLimit)}/>
                            {(onDelete || onDuplicate || onMoveUp || onMoveDown || onSave) &&
                            <ButtonGroup>
                                {onDelete && <Button minimal icon={IconNames.TRASH} onClick={() => onDelete()}/>}
                                {onDuplicate &&
                                <Button minimal icon={IconNames.DUPLICATE} onClick={() => onDuplicate()}/>}
                                {onMoveUp &&
                                <Button minimal icon={IconNames.CHEVRON_UP} onClick={() => onMoveUp()}/>}
                                {onMoveDown &&
                                <Button minimal icon={IconNames.CHEVRON_DOWN} onClick={() => onMoveDown()}/>}
                                {onSave && <Button minimal icon={IconNames.FLOPPY_DISK} disabled={!savePossible}
                                                   onClick={() => onSave(repetitions, timeLimit)}/>}
                            </ButtonGroup>
                            }
                        </ControlGroup>
                    ]}
                </FormGroup>
            </Card>

        );

    }

    private onRepetitionsChanged(repetitions: number) {
        this.setState(state => update(state, {
            repetitions: { $set: repetitions }
        }));
    }

    private onTimeLimitChanged(timeLimit: number) {
        this.setState(state => update(state, {
            timeLimit: { $set: timeLimit }
        }));
    }
}

export default PlanCard;

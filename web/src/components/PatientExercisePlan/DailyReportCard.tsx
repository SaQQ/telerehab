import React from "react";

import {
    Card,
    FormGroup,
    Text
} from "@blueprintjs/core";

import strings from "../../constants/strings";

import './index.css';
import { IPlanReport } from "@saqq/telerehab-infrastructure/lib/interfaces/plan";
import { Elevation } from "@blueprintjs/core/lib/esm/common/elevation";

interface IDailyReportCardProps {
    report: IPlanReport;
    onConfirmAlert: () => void;
}

const DailyReportCard: React.FC<IDailyReportCardProps> = (props) => {
    const { report, onConfirmAlert } = props;

    const confirmable = report.alert && !report.alertConfirmed;

    return (
        <Card interactive={confirmable}
              className={"exercise-plan-card" + (confirmable ? " exercise-plan-card--skipped" : "")}
              elevation={Elevation.TWO} onClick={() => onConfirmAlert()}>
            <FormGroup label={strings.daily_report} style={{ margin: 0 }}>
                <Text>{`Zmęczenie: ${report.tiredness}`}</Text>
                <Text>{`Ocena: ${report.grade}`}</Text>
                <Text>{`Problemy: ${report.problems.join(", ")}`}</Text>
            </FormGroup>
        </Card>

    );

};

export default DailyReportCard;

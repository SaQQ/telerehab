import React from "react";

import update from "immutability-helper";

import {MenuItem, NonIdealState, Text, Popover, Button, Classes} from "@blueprintjs/core";
import {IItemRendererProps, Suggest} from "@blueprintjs/select";

import Firebase from "firebase/app";

import uuid from "uuid";

import {IFirebaseContextProps, withFirebase} from "../Firebase";
import {INotificationProps, withNotifications} from "../Notifications";

import IExerciseWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-with-id";

import IPlan, {EMPTY_PLAN, EMPTY_PLAN_STATUS, IPlanStatus} from "@saqq/telerehab-infrastructure/lib/interfaces/plan";
import IPlanItem, {
    EMPTY_EXERCISE_STATUS,
    EMPTY_PLAN_ITEM
} from "@saqq/telerehab-infrastructure/lib/interfaces/plan-item";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";

import PlanCard from './PlanCard';
import strings from "../../constants/strings";
import DateUtils from "../../utils/date-utils";
import {CancellablePromiseComponent, PromiseCancelled} from "../../utils/cancellable-promise";
import {IconNames} from "@blueprintjs/icons";
import Loading from "../Loading";
import DailyReportCard from "./DailyReportCard";
import {DatePicker} from "@blueprintjs/datetime";
import MomentLocaleUtils from "react-day-picker/moment";

import "moment/locale/pl";
import "moment/locale/en-gb";

interface IPlanItemWithUUID extends IPlanItem {
    uuidKey: string;
}

interface IPlanDayCardProps extends IFirebaseContextProps, INotificationProps {
    emphasized?: boolean;
    patientID: string;
    day: Date;
    exercises: IExerciseWithID[];
    onPlanChanged: (plan: IPlan) => void;

    notEmptyDatesDict: { [key: string]: boolean };
}

interface IPlanDayCardState {
    loading: boolean;
    items: IPlanItemWithUUID[];
    status: IPlanStatus;
    exercises: IExerciseWithID[];

    cloneDate: Date | null;
}

const ExerciseSuggest = Suggest.ofType<IExerciseWithID>();

class PlanDayCardBase extends CancellablePromiseComponent<IPlanDayCardProps, IPlanDayCardState> {

    private readonly planRef: Firebase.firestore.DocumentReference;

    constructor(props: IPlanDayCardProps) {
        super(props);

        const {firebase, patientID, day} = this.props;
        const dayStr = DateUtils.getDateString(day);

        this.planRef = firebase.firestore.collection(Collections.PATIENT_PLANS(patientID)).doc(dayStr);

        this.state = {
            loading: false,
            items: [],
            status: EMPTY_PLAN_STATUS,
            exercises: [],

            cloneDate: null
        };
    }

    private canEdit() {
        const {day} = this.props;
        const now = new Date();
        const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

        if (today > day) {
            // Plan day already passed
            return false;
        }

        // All exercises are not yet started
        return this.state.items.every(item => item.status.startTime === null && item.status.endTime === null);
    }

    async componentDidMount() {
        const {notifications} = this.props;

        this.setState(state => update(state, {
            loading: {$set: true}
        }));

        try {
            const doc = await this.addCancelablePromise(this.planRef.get());
            if (doc.exists) {
                this.setState(state => {
                    const plan = {...EMPTY_PLAN, ...doc.data()} as IPlan;
                    return update(state, {
                        items: {
                            $set: plan.exercises.map(item => ({
                                ...EMPTY_PLAN_ITEM, ...item,
                                uuidKey: uuid.v4()
                            }))
                        },
                        status: {$set: plan.status}
                    });
                });
            }
        } catch (exc) {
            if (exc instanceof PromiseCancelled) {
                return;
            } else if (exc instanceof Error) {
                notifications.showError(`${strings.failed_to_download_plan}: ${exc.message}`);
            }
        }

        this.setState(state => update(state, {
            loading: {$set: false}
        }));
    }

    async onNewExercise(exercise: IExerciseWithID) {
        const {firebase} = this.props;
        const {items} = this.state;

        const exerciseRef = firebase.firestore.doc(Collections.EXERCISE(exercise.id));
        const newItems = update(items, {
            $push: [{
                exercise: exerciseRef,
                uuidKey: uuid.v4(),
                status: EMPTY_EXERCISE_STATUS,
                repetitions: 0,
                timeLimit: 0
            }]
        });

        await this.savePlan(newItems);
    }

    async onSave(idx: number, repetitions: number, timeLimit: number) {
        const {items} = this.state;

        const newItems = update(items, {
            [idx]: {
                repetitions: {$set: repetitions},
                timeLimit: {$set: timeLimit}
            }
        });

        await this.savePlan(newItems);
    }

    async onDuplicate(idx: number) {
        const {items} = this.state;

        const newItems = update(items, {
            $splice: [[idx, 0, {
                exercise: items[idx].exercise,
                uuidKey: uuid.v4(),
                status: EMPTY_EXERCISE_STATUS,
                repetitions: items[idx].repetitions,
                timeLimit: items[idx].timeLimit
            }]]
        });

        await this.savePlan(newItems);
    }

    async onDelete(idx: number) {
        const {items} = this.state;

        const newItems = update(items, {$splice: [[idx, 1]]});

        await this.savePlan(newItems);
    }

    async onMoveUp(idx: number) {
        const {items} = this.state;

        let newItems = update(items, {$splice: [[idx - 1, 1]]});
        newItems = update(newItems, {$splice: [[idx, 0, items[idx - 1]]]});

        await this.savePlan(newItems);
    }

    async onMoveDown(idx: number) {
        const {items} = this.state;

        let newItems = update(items, {$splice: [[idx, 1]]});
        newItems = update(newItems, {$splice: [[idx + 1, 0, items[idx]]]});

        await this.savePlan(newItems);
    }

    async savePlan(newItems: IPlanItemWithUUID[]) {
        const {notifications, onPlanChanged} = this.props;

        this.setState(state => update(state, {
            loading: {$set: true}
        }));

        // TODO: Make sure data from user did not change, so we will not override them

        // Get current statuses from database


        // Remove status information
        // const newPlan: IPlanWithoutStatus = {
        //     exercises: newItems.map(item => {
        //         const result: IPlanItemWithoutStatus = {
        //             customization: item.customization,
        //             exercise: item.exercise
        //         };

        //         return result;
        // })};

        const newPlan: IPlan = {
            exercises: newItems.map(item => update(item, {$unset: ['uuidKey']})),
            status: EMPTY_PLAN_STATUS
        };

        try {
            if (newPlan.exercises.length > 0) {
                await this.addCancelablePromise(this.planRef.set(newPlan));
            } else {
                await this.addCancelablePromise(this.planRef.delete());
            }

            this.setState(state => update(state, {
                items: {$set: newItems}
            }));
        } catch (exc) {
            if (exc instanceof PromiseCancelled) {
                return;
            } else if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state,
            {
                loading: {$set: false}
            }), () => onPlanChanged(newPlan));
    }

    private static readonly exerciseRenderer = (exercise: IExerciseWithID, props: IItemRendererProps) => {
        if (!props.modifiers.matchesPredicate) {
            return null;
        }
        return (<MenuItem
            active={props.modifiers.active}
            key={exercise.id}
            onClick={props.handleClick}
            text={(
                <div>
                    {exercise.code}
                    <span style={{marginLeft: "0.5em"}}/>
                    {exercise.name}
                </div>
            )}/>);
    };

    private confirmAlert() {
        // TODO
    }

    private readonly onCloneDateChanged = (date: Date | null) => {
        if (date !== null && !this.isDateNotEmpty(date))
            return;

        this.setState(state => update(state, {
            cloneDate: {$set: date}
        }));
    };

    private readonly isDateNotEmpty = (date: Date) => {
        const {notEmptyDatesDict} = this.props;
        const dayStr = DateUtils.getDateString(date);

        return (dayStr in notEmptyDatesDict) && notEmptyDatesDict[dayStr];
    };

    private readonly onClonePlan = async () => {
        const date = this.state.cloneDate;
        if (date === null)
            return;

        this.setState(state => update(state, {
            loading: {$set: true}
        }));

        try {
            const dayStr = DateUtils.getDateString(date);
            const planRef = this.props.firebase.firestore.collection(Collections.PATIENT_PLANS(this.props.patientID)).doc(dayStr);

            const doc = await this.addCancelablePromise(planRef.get());
            if (doc.exists) {
                const plan = {...EMPTY_PLAN, ...doc.data()} as IPlan;
                const items = plan.exercises.map(item => ({
                    ...EMPTY_PLAN_ITEM, ...item,
                    status: EMPTY_EXERCISE_STATUS,
                    uuidKey: uuid.v4()
                }));

                await this.savePlan(items);
            } else {
                this.props.notifications.showError(strings.failed_to_download_plan);
            }
        } catch (exc) {
            if (exc instanceof PromiseCancelled) {
                return;
            } else if (exc instanceof Error) {
                this.props.notifications.showError(`${strings.failed_to_download_plan}: ${exc.message}`);
            }
        }

        this.setState(state => update(state, {
            loading: {$set: false}
        }));
    };

    public render() {
        const {day, exercises} = this.props;
        const {loading, items, status} = this.state;

        // const planItemNotStarted = (item: IPlanItem) => !item.status.startTime && !item.status.endTime;

        const canEdit = this.canEdit();
        const planEmpty = items.length === 0;


        //    const localeUtils = new MomentLocaleUtils(strings.localization_string);


        const cloneDayExercises = (
            <Popover
                fill
                popoverClassName={Classes.POPOVER_CONTENT_SIZING}
                content={
                    <div>
                        <DatePicker
                            modifiers={{isNotEmpty: this.isDateNotEmpty}}
                            canClearSelection={true}
                            value={this.state.cloneDate!}
                            onChange={this.onCloneDateChanged}
                            highlightCurrentDay={true}
                            locale={strings.localization_string}
                            localeUtils={MomentLocaleUtils}
                        />

                        <Button
                            fill
                            disabled={this.state.cloneDate === null}
                            onClick={this.onClonePlan}
                        >
                            {strings.copy}
                        </Button>
                    </div>
                }
            >
                <Button fill>{strings.copy_plan_from_an_other_day}</Button>
            </Popover>
        );

        const exerciseSuggest = (
            <ExerciseSuggest
                fill
                inputProps={{placeholder: strings.search}}
                items={exercises}
                itemRenderer={PlanDayCardBase.exerciseRenderer}
                itemPredicate={(query, exercise) => exercise.name.match(new RegExp(query, 'i')) !== null || exercise.code.match(new RegExp(query, 'i')) !== null}
                inputValueRenderer={exercise => exercise.name}
                noResults={<MenuItem disabled={true} text={strings.no_results}/>}
                onItemSelect={exercise => this.onNewExercise(exercise)}
            />
        );

        return (
            <Loading loading={loading}>
                <Text>{day.toLocaleDateString()}</Text>
                {
                    planEmpty && !canEdit &&
                    <NonIdealState
                        title={strings.plan_is_empty}
                        description={strings.you_cannot_modify_past_plans}
                        icon={IconNames.LOCK}
                    />
                }
                {
                    planEmpty && canEdit &&
                    <NonIdealState
                        title={strings.plan_is_empty}
                        description={strings.add_some_items}
                        icon={IconNames.INSERT}
                        action={
                            <div>
                                {cloneDayExercises}
                                {exerciseSuggest}
                            </div>
                        }
                    />
                }
                {status.report && <DailyReportCard report={status.report} onConfirmAlert={() => this.confirmAlert()}/>}
                {
                    items.map((item, idx) =>
                        <PlanCard
                            key={item.uuidKey}
                            planItem={item}
                            onSave={canEdit ? (repetitions, timeLimit) => this.onSave(idx, repetitions, timeLimit) : undefined}
                            onDuplicate={canEdit ? () => this.onDuplicate(idx) : undefined}
                            onDelete={canEdit ? () => this.onDelete(idx) : undefined}
                            onMoveUp={(canEdit && idx !== 0) ? () => this.onMoveUp(idx) : undefined}
                            onMoveDown={(canEdit && idx !== items.length - 1) ? () => this.onMoveDown(idx) : undefined}
                        />
                    )
                }
                {
                    !planEmpty && canEdit &&
                    exerciseSuggest
                }
            </Loading>
        );
    }
}


const PlanDayCard = withNotifications(withFirebase(PlanDayCardBase));

export default PlanDayCard;


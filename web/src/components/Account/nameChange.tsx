import * as React from "react";

import { ControlGroup, InputGroup, ProgressBar, Button, Intent } from "@blueprintjs/core";

import { IconNames } from "@blueprintjs/icons";

import { withAuthUser, IAuthUserContextProps } from "../Session/context";
import { withFirebase, IFirebaseContextProps } from "../Firebase";
import { INotificationProps, withNotifications } from "../Notifications";

import update from "immutability-helper";
import strings from "../../constants/strings";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";


interface IAccountNameChangeProps extends IAuthUserContextProps, IFirebaseContextProps, INotificationProps {

}

interface IAccountNameChangeState {
    userName: string;
    busy: boolean;
    changed: boolean;
}


const INITIAL_STATE: IAccountNameChangeState = {
    userName: "",
    busy: false,
    changed: false
}

export class AccountNameChangeBase extends React.Component<IAccountNameChangeProps, IAccountNameChangeState> {
    public constructor(props: IAccountNameChangeProps) {
        super(props);

        this.state = {
            ...INITIAL_STATE,
        };
    }

    public componentDidMount() {
        const { authUserData: authUser } = this.props;

        this.setState(state => update(state, { userName: { $set: authUser && authUser.user.displayName ? authUser.user.displayName : "" } }));
    }

    private onUserNameChanged(userName: string) {
        this.setState(state => update(state, { userName: { $set: userName }, changed: { $set: true } }));
    }

    private onSubmit() {
        const { authUserData: authUser, firebase, notifications } = this.props;
        const { userName } = this.state;

        this.setState(state => update(state, { busy: { $set: true } }));

        // TODO: User reauthenticaiton

        authUser!.user.updateProfile({ displayName: userName })
            .then(() => firebase.firestore.collection(Collections.USERS).doc(authUser!.user.uid)
                .set({
                    displayName: userName,
                }, { merge: true }))
            .then(() => {
                notifications.showSuccess(strings.successfully_changed_user_name);
            })
            .catch((error: Error) => {
                return notifications.showError(error.message);
            })
            .finally(() => {
                this.setState(state => update(state, { busy: { $set: false }, changed: { $set: false } }));
            })
            .catch();
    }

    render() {
        const {
            userName,
            busy,
            changed
        } = this.state;

        return (
            <ControlGroup vertical>
                <InputGroup
                    large
                    placeholder={strings.user_name}
                    leftIcon={IconNames.USER}
                    value={userName}
                    disabled={busy}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onUserNameChanged(event.target.value)}/>
                <Button
                    large
                    disabled={busy || !changed}
                    onClick={() => this.onSubmit()}>
                    {strings.change_name}
                </Button>
                {busy && <ProgressBar intent={Intent.PRIMARY}/>}
            </ControlGroup>
        );
    }
}

const AccountNameChange = withNotifications(withFirebase(withAuthUser(AccountNameChangeBase)));

export default AccountNameChange;

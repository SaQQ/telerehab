import * as React from "react";

import { Container, Row, Col } from "react-grid-system";
import { H2 } from "@blueprintjs/core";

import AccountPasswordChange from "./passwordChange";
import AccountNameChange from "./nameChange";
import {IAuthUserContextProps, withAuthUser} from "../Session/context";

import strings from "../../constants/strings"


interface IAccountPageProps extends IAuthUserContextProps {

}

const AccountPage = (props: IAccountPageProps) => (
    <Container>
        <Row>
            <Col sm={12} lg={6}>
                <H2>{strings.change_name}</H2>
                {/*This causes re-creation of component when authUser changes so that it will be remounted*/}
                <AccountNameChange key={props.authUserData ? props.authUserData.user.uid : ""} />
            </Col>
            <Col sm={12} lg={6}>
                <H2>{strings.change_password}</H2>
                <AccountPasswordChange />
            </Col>
        </Row>
    </Container>
);

export default withAuthUser(AccountPage);

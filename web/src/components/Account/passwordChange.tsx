import React, { ChangeEvent } from "react";

import * as firebase from "firebase/app";
import "firebase/auth";

import { IconNames } from "@blueprintjs/icons";
import { ControlGroup, InputGroup, ProgressBar, Button, Intent } from "@blueprintjs/core";

import { IAuthUserContextProps, withAuthUser } from "../Session/context";
import { IFirebaseContextProps, withFirebase } from "../Firebase";
import { INotificationProps, withNotifications } from "../Notifications";

import update from "immutability-helper";
import strings from "../../constants/strings";


interface IAccountPasswordChangeProps extends IAuthUserContextProps, IFirebaseContextProps, INotificationProps {

}

interface IAccountPasswordChangeState {
    currentPassword: string;
    newPassword: string;
    newPasswordConfirm: string;
    busy: boolean;
}

const INITIAL_STATE: IAccountPasswordChangeState = {
    currentPassword: "",
    newPassword: "",
    newPasswordConfirm: "",
    busy: false
};

export class AccountPasswordChangeBase extends React.Component<IAccountPasswordChangeProps, IAccountPasswordChangeState> {
    public constructor(props: IAccountPasswordChangeProps) {
        super(props);

        this.state = {
            ...INITIAL_STATE,
        };
    }

    private onCurrentPasswordChange(currentPassword: string) {
        this.setState(state => update(state, { currentPassword: { $set: currentPassword } }));
    }

    private onNewPasswordChange(newPassword: string) {
        this.setState(state => update(state, { newPassword: { $set: newPassword } }));
    }

    private onNewPasswordConfirmChange(newPasswordConfirm: string) {
        this.setState(state => update(state, { newPasswordConfirm: { $set: newPasswordConfirm } }));
    }

    onSubmit() {
        const { authUserData, notifications } = this.props;
        const { currentPassword, newPassword } = this.state;

        const user = authUserData!.user;

        this.setState(state => update(state, { busy: { $set: true } }));

        const credential = firebase.auth.EmailAuthProvider.credential(
            user.email || "",
            currentPassword
        );

        user.reauthenticateWithCredential(credential)
            .then(() => user.updatePassword(newPassword))
            .then(() => {
                this.setState(() => ({ ...INITIAL_STATE }));
                notifications.showSuccess(strings.successfully_changed_password);
            })
            .catch((error) => {
                notifications.showError(error.message);
            })
            .finally(() => {
                this.setState(state => update(state, { busy: { $set: false } }));
            }).catch();
    }

    render() {
        const {
            currentPassword,
            newPassword,
            newPasswordConfirm,
            busy
        } = this.state;

        const isInvalid = (newPassword === "" || !(newPassword === newPasswordConfirm));

        return (
            <ControlGroup vertical>
                <InputGroup
                    large
                    placeholder={strings.enter_current_password}
                    leftIcon={IconNames.LOCK}
                    type="password"
                    value={currentPassword}
                    disabled={busy}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => this.onCurrentPasswordChange(event.target.value)}/>
                <InputGroup
                    large
                    placeholder={strings.enter_new_password}
                    leftIcon={IconNames.LOCK}
                    type="password"
                    value={newPassword}
                    disabled={busy}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => this.onNewPasswordChange(event.target.value)}/>
                <InputGroup
                    large
                    placeholder={strings.repeat_new_password}
                    leftIcon={IconNames.LOCK}
                    type="password"
                    value={newPasswordConfirm}
                    disabled={busy}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => this.onNewPasswordConfirmChange(event.target.value)}/>
                <Button
                    large
                    disabled={busy || isInvalid}
                    onClick={() => this.onSubmit()}>
                    {strings.change_password}
                </Button>
                {busy && <ProgressBar intent={Intent.PRIMARY} />}
            </ControlGroup>
        );
    }
}

const AccountPasswordChange = withNotifications(withFirebase(withAuthUser(AccountPasswordChangeBase)));

export default AccountPasswordChange;

import React from "react";
import firebase from "firebase/app"

import { AuthUserContext, AuthUserData } from "./context";
import { withFirebase, IFirebaseContextProps } from "../Firebase";

import update from "immutability-helper";

export interface IAuthenticationContextProps extends IFirebaseContextProps {

};

export function withAuthentication<P extends IAuthenticationContextProps>(Component: React.ComponentType<P>) {

    interface IWithAuthenticationState {
        authUserData: AuthUserData | null;
    }

    class WithAuthentication extends React.Component<IAuthenticationContextProps, IWithAuthenticationState> {
        public constructor(props: IAuthenticationContextProps) {
            super(props);

            this.state = {
                authUserData: null,
            };
        }

        private listener: firebase.Unsubscribe | null = null; 

        componentDidMount() {
            const { firebase: firebase_ } = this.props;
            this.listener = firebase_.auth.onAuthStateChanged(
                async (user) => {
                    if (user) {
                        const roles: string[] = [];

                        try {
                            const token = await user.getIdTokenResult();
                            const claims = token.claims;

                            Object.keys(claims).forEach((key) => {
                                // Roles has format "is_ROLENAME"
                                if (key.startsWith("is_")) {
                                    // Value must be set to true
                                    if ((claims as any)[key]) {
                                        roles.push(key.substring(3));
                                    }
                                }
                            });
                        }
                        catch(exc) {
                        }

                        this.setState(state => update(state, { 
                            authUserData: { $set: { user, roles } } 
                        }));
                    } else {
                        this.setState(state => update(state, { 
                            authUserData: { $set: null } }));
                    }
                },
            );
        }

        componentWillUnmount() {
            this.listener!();
        }

        render() {
            const { authUserData } = this.state;
            
            return (
                <AuthUserContext.Provider value={authUserData}>
                    <Component {...this.props as P} />
                </AuthUserContext.Provider>
            );
        }
    }

    return withFirebase(WithAuthentication);
};

import React from "react";
import firebase from "firebase/app";

export interface AuthUserData {
    user: firebase.User;
    roles: string[];
}

export interface IAuthUserContextProps {
    authUserData: AuthUserData | null;
}

type Omitted<P> = Pick<P, Exclude<keyof P, keyof IAuthUserContextProps>>;

export const AuthUserContext = React.createContext<AuthUserData | null>(null);

export function withAuthUser<P extends IAuthUserContextProps>(Component: React.ComponentType<P>): React.ComponentType<Omitted<P>> {
    return function BoundComponent(props: Omitted<IAuthUserContextProps>) {
        return (
            <AuthUserContext.Consumer>
                {authUserData => <Component {...props as P} authUserData={authUserData} />}
            </AuthUserContext.Consumer>
        );
    }
}


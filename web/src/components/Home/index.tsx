import React from "react";

import { H4, H2, UL, Callout, Intent } from "@blueprintjs/core";
import strings from "../../constants/strings";


const HomePage = () => (
    <div>
        <H2>{strings.home_page_title}</H2>
        <p>{strings.home_page_main_text}</p>
        <br />
        <Callout intent={Intent.PRIMARY}>
            <H4>{strings.home_page_before_training_header}</H4>
            <div>
                <p>{strings.home_page_before_training_main_text1}</p>
                <p>{strings.home_page_before_training_main_text2}</p>
                <UL>
                    <li>{strings.home_page_before_training_list_item1}</li>
                    <li>{strings.home_page_before_training_list_item2}</li>
                    <li>{strings.home_page_before_training_list_item3}</li>
                    <li>{strings.home_page_before_training_list_item4}</li>
                    <li>{strings.home_page_before_training_list_item5}</li>
                </UL>
            </div>
        </Callout>
        <br />
        <Callout intent={Intent.WARNING}>
            <H4>{strings.home_page_exercise_safety_header}</H4>
            <div>
                <p>{strings.home_page_exercise_safety_main_text}</p>
                <UL>
                    <li>{strings.home_page_exercise_safety_list_item1}</li>
                    <li>{strings.home_page_exercise_safety_list_item2}</li>
                    <li>{strings.home_page_exercise_safety_list_item3}</li>
                    <li>{strings.home_page_exercise_safety_list_item4}</li>
                    <li>{strings.home_page_exercise_safety_list_item5}</li>
                    <li>{strings.home_page_exercise_safety_list_item6}</li>
                </UL>
            </div>
        </Callout>
    </div>
);

export default HomePage;

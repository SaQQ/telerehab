import * as React from "react";

import Firebase from "./firebase";

export interface IFirebaseContextProps {
    firebase: Firebase
}

type Omitted<P> = Pick<P, Exclude<keyof P, keyof IFirebaseContextProps>>;

export const FirebaseContext = React.createContext<Firebase | null>(null);

export function withFirebase<P extends IFirebaseContextProps>(Component: React.ComponentType<P>): React.ComponentType<Omitted<P>> {
    return function BoundComponent(props: Omitted<IFirebaseContextProps>) {
        return (
            <FirebaseContext.Consumer>
                {firebase => <Component {...props as P} firebase={firebase}/>}
            </FirebaseContext.Consumer>
        );
    };

}

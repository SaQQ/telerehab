import firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage";
import "firebase/firestore";
import "firebase/functions";
import strings from "../../constants/strings";


const config = {
    apiKey: "AIzaSyAi6cm12ZnAIt0gbHxpO9Zg3Ym_UT08_sY",
    authDomain: "telerehab-30983.firebaseapp.com",
    databaseURL: "https://telerehab-30983.firebaseio.com",
    projectId: "telerehab-30983",
    storageBucket: "telerehab-30983.appspot.com",
    messagingSenderId: "56612698564",
    appId: "1:56612698564:web:f59d551bc486f12e",
};

class Firebase {

    public readonly auth: firebase.auth.Auth;
    public readonly firestore: firebase.firestore.Firestore;
    public readonly storage: firebase.storage.Storage;
    private readonly functions: firebase.functions.Functions;

    private updateUserClaims: firebase.functions.HttpsCallable;

    public constructor() {
        firebase.initializeApp(config);

        this.auth = firebase.auth();
        this.firestore = firebase.firestore();
        this.storage = firebase.storage();
        this.functions = firebase.app().functions("europe-west3");

        this.updateUserClaims = this.functions.httpsCallable("updateUserClaims");
    }

    public async createUserWithEmailAndPassword(email: string, password: string) {
        return this.auth.createUserWithEmailAndPassword(email, password);
    }

    public async signInWithEmailAndPassword(email: string, password: string) {
        
        const userCredential = await this.auth.signInWithEmailAndPassword(email, password);

        if (userCredential.user === null) {
            throw new Error(strings.failed_to_sign_in);
        }

        const updatedUserClaims = (await this.updateUserClaims()).data as boolean;
        if (!updatedUserClaims) {
            return userCredential;
        }

        const credential = firebase.auth.EmailAuthProvider.credential(email, password);
        return userCredential.user.reauthenticateWithCredential(credential);
    }

    public async signOut() {
        return this.auth.signOut();
    }
}

export default Firebase;

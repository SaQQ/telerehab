import Firebase from "./firebase";

export * from "./context";

export default Firebase;

import React from "react"
import YouTube from "react-youtube";

import "./Media.css"

interface IVideoProps {
    youTubeId: string;
}

export const Video = (props: IVideoProps) => (
    <div className="you-tube-container">
        {<YouTube videoId={props.youTubeId} />}
    </div>
)

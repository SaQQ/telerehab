import React from "react";

import { Button } from "@blueprintjs/core";

import { withFirebase, IFirebaseContextProps } from "../Firebase";
import { withRouter, RouteComponentProps } from "react-router-dom";

import Routes from "../../constants/routes";
import strings from "../../constants/strings";


interface ISignOutButtonProps extends RouteComponentProps<any>, IFirebaseContextProps {

}

const SignOutButton = (props: ISignOutButtonProps) => (
    <Button
        minimal
        onClick={() => {
            props.firebase
                .signOut()
                .then(() => {
                    props.history.push(Routes.SIGN_IN);
                })
                .catch();
        }}>
        {strings.sign_out}
    </Button>
);

export default withRouter(withFirebase(SignOutButton));

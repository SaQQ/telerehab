import * as React from "react";

import update from "immutability-helper";

import { IconNames } from "@blueprintjs/icons";
import { Intent, Alert } from "@blueprintjs/core";

import DefaultToaster from "../../utils/toaster";
import strings from "../../constants/strings";


interface INotificationService {
    showError: (message: JSX.Element | string) => void;
    showSuccess: (message: JSX.Element | string) => void;
}

export interface INotificationProps {
    notifications: INotificationService;
}

type Omitted<P extends INotificationProps> = Pick<P, Exclude<keyof P, keyof INotificationProps>>;

interface IWithNotificationsState {
    errorOpen: boolean;
    errorElement: JSX.Element | string;
}

export function withNotifications<P extends INotificationProps>(Component: React.ComponentType<P>): React.ComponentType<Omitted<P>> {

    return class WithNotifications extends React.Component<Omitted<P>, IWithNotificationsState> {

        constructor(props: Omitted<P>) {
            super(props);

            this.state = {
                errorOpen: false,
                errorElement: ""
            };
        }

        private showError(message: JSX.Element | string) {
            DefaultToaster.show(
                {
                    icon: IconNames.ERROR,
                    intent: Intent.DANGER,
                    message: message
                }
            );
        }

        public showSuccess(message: JSX.Element | string) {
            DefaultToaster.show(
                {
                    icon: IconNames.TICK,
                    intent: Intent.SUCCESS,
                    message: message
                }
            );
        }

        private closeErrorAlert() {
            this.setState(state => update(state, {
                errorOpen: { $set: false }
            }));
        }

        render() {
            const { ...props } = this.props;
            const { errorOpen, errorElement } = this.state;

            return (
                <div>
                    <Alert
                        confirmButtonText={strings.close}
                        icon={IconNames.ERROR}
                        intent={Intent.WARNING}
                        isOpen={errorOpen}
                        onClose={() => this.closeErrorAlert()}
                    >
                        <div>
                            <p>{`${strings.errorOccurred}:`}</p>
                            <p>
                                {errorElement}
                            </p>
                        </div>
                    </Alert>

                    <Component {...props as P} notifications={{
                        showError: message => this.showError(message),
                        showSuccess: message => this.showSuccess(message)
                    }}/>
                </div>
            );
        }
    }
}

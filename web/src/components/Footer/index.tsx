import React from "react";
import { Text } from "@blueprintjs/core";

import strings from "../../constants/strings";

import "./Footer.css";

interface IFooterProps {

}

const Footer = (props: IFooterProps) => (
    <div className="footer">
        <Text>{strings.footer_text}</Text>
    </div>
);

export default Footer;

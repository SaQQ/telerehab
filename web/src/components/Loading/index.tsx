import React from "react"
import { NonIdealState, ProgressBar } from "@blueprintjs/core";
import strings from "../../constants/strings";

interface ILoadingProps {
    loading: boolean;
}

const Loading = (props: React.PropsWithChildren<ILoadingProps>) => (
    <div>
        { props.loading && <NonIdealState title={strings.loading} action={<ProgressBar />} /> }
        { !props.loading && props.children }
    </div>
);

export default Loading;

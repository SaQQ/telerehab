import React from "react";
import { Link } from "react-router-dom";
import { Hidden } from "react-grid-system";
import { Navbar, Alignment, Classes } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import "./Navigation.css";

import { IAuthUserContextProps, withAuthUser, AuthUserData } from "../Session/context";

import strings from "../../constants/strings";
import Routes from "../../constants/routes";
import SignOutButton from "../SignOut";
import ForRoles from "../ForRoles";


const NavigationBase = (props: IAuthUserContextProps) => (
    props.authUserData ? <NavigationAuth userData={props.authUserData}/> : <NavigationNonAuth/>
);

interface INavigationAuthProps {
    userData: AuthUserData;
}

const LINK_STYLE = `${Classes.BUTTON} ${Classes.MINIMAL}`;

const NavigationAuth = (props: INavigationAuthProps) => (
    <Navbar className="navigation">
        <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>{strings.application_name}</Navbar.Heading>
            <Navbar.Divider/>
            <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.HOME)}`} to={Routes.HOME}>{strings.navigation_home}</Link>
            <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.LIST)}`} to={Routes.EXERCISES}>{strings.navigation_exercise_library}</Link>
            <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.GRID_VIEW)}`} to={Routes.EXERCISE_GROUPS}>{strings.navigation_exercise_tags}</Link>
            <ForRoles roles={["admin", "therapist"]}>
                <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.PERSON)}`} to={Routes.PATIENTS}>{strings.navigation_patients}</Link>
            </ForRoles>
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT}>
            <Hidden xs sm md>{props.userData.user.email}</Hidden>
            <Navbar.Divider/>
            <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.USER)}`} to={Routes.ACCOUNT}>{strings.navigation_account}</Link>
            <ForRoles roles={["admin"]}>
                <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.COG)}`} to={Routes.ADMIN}>{strings.navigation_admin}</Link>
            </ForRoles>
            <SignOutButton/>
        </Navbar.Group>
    </Navbar>
);

interface INavigationNonAuthProps {

}

const NavigationNonAuth = (props: INavigationNonAuthProps) => (
    <Navbar className="navigation">
        <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>{strings.application_name}</Navbar.Heading>
            <Navbar.Divider/>
            <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.HOME)}`} to={Routes.HOME}>{strings.navigation_home}</Link>
            <Link className={`${LINK_STYLE} ${Classes.iconClass(IconNames.LIST)}`} to={Routes.EXERCISES}>{strings.navigation_exercise_library}</Link>
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT}>
            <Link className={`${LINK_STYLE}`} to={Routes.SIGN_IN}>{strings.sign_in}</Link>
            <Link className={`${LINK_STYLE}`} to={Routes.SIGN_UP}>{strings.sign_up}</Link>
        </Navbar.Group>
    </Navbar>
);

export default withAuthUser(NavigationBase);

import React from "react";

import {
    Button,
    ButtonGroup,
    Callout,
    Classes,
    ControlGroup,
    EditableText,
    FormGroup,
    H1,
    Intent,
    ProgressBar
} from "@blueprintjs/core";

import {IconNames} from "@blueprintjs/icons";

import {Col, Container, Row} from "react-grid-system";
import {IFirebaseContextProps, withFirebase} from "../Firebase";

import Routes from "../../constants/routes";

import update from "immutability-helper";
import {RouteComponentProps, withRouter} from "react-router";
import {INotificationProps, withNotifications} from "../Notifications";
import {Video} from "../Media";
import IExerciseWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-with-id";
import IExercise, {EMPTY_EXERCISE} from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";
import IExerciseGroupWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group-with-id";
import IExerciseGroup, {EMPTY_EXERCISE_GROUP} from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group";
import strings from "../../constants/strings";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import getVideoId from "get-video-id";

import "./styles.css"
import ExerciseGroupMultiSelect from "../../utils/exercise-group-multiselect";
import {IConfirmationProps, withConfirmations} from "../Confirmations";
import StyledDropzone from "../../utils/styled-dropzone"
import uuid from "uuid";


interface PhotoSource {
    storagePath: string;
    source: string;
    isNew: boolean;
    isRemoved: boolean;
    newFile: File | null;
}

const PHOTO_MARKER_REGEX = /\[PHOTO ([+-]?\d+)]/;

interface TextWithPhotosProps {
    text: string;
    photoSources: string[];
}

interface TextWithPhotosElementProps {
    text?: string;
    photoIndex?: number;
    photoSources: string[];
}

function TextWithPhotosElement(props: TextWithPhotosElementProps) {

    if (props.text) {
        return <p>{props.text}</p>;
    } else if (props.photoIndex) {
        if (props.photoIndex < 1 || props.photoIndex > props.photoSources.length) {
            return (
                <Callout intent={Intent.DANGER}>
                    {`${strings.invalid_photo_index}: ${props.photoIndex}`}
                </Callout>
            )
        } else {
            const photoSource = props.photoSources[props.photoIndex - 1];
            return (
                <img src={photoSource} alt={`IMG ${props.photoIndex}`} style={{width: "100%"}}/>
            );
        }
    } else {
        return (<p/>);
    }

}

export function TextWithPhotos(props: TextWithPhotosProps) {

    const elements: JSX.Element[] = [];

    let currText = props.text;
    let match = currText.match(PHOTO_MARKER_REGEX);

    let elementIndex = 0;

    while (match !== null) {

        const markerStart = match.index!;
        const markerLength = match[0].length;
        const photoIndex = Number.parseInt(match[1]);

        const prevText = currText.substr(0, markerStart);

        elements.push(<TextWithPhotosElement key={'element' + elementIndex} photoSources={props.photoSources}
                                             text={prevText}/>);
        elementIndex++;
        elements.push(<TextWithPhotosElement key={'element' + elementIndex} photoSources={props.photoSources}
                                             photoIndex={photoIndex}/>);
        elementIndex++;

        currText = currText.substr(markerStart + markerLength);
        match = currText.match(PHOTO_MARKER_REGEX);
    }

    elements.push(<TextWithPhotosElement key={'element' + elementIndex} photoSources={props.photoSources}
                                         text={currText}/>);

    return (
        <div>
            {elements}
        </div>
    );
}

interface IExerciseDetailsPageProps {

}

interface IExerciseDetailsPageBaseProps extends IFirebaseContextProps, RouteComponentProps<any>, INotificationProps, IConfirmationProps {
    create: boolean
}

interface IExerciseDetailsPageState {
    create: boolean;

    loading: boolean;
    changed: boolean;

    videoLink: string;
    videoLinkHint: string;

    exercise: IExerciseWithID;
    exerciseGroups: IExerciseGroupWithID[];
    allExerciseGroups: IExerciseGroupWithID[];

    photoSources: PhotoSource[];
}

class ExerciseDetailsPageBase extends React.Component<IExerciseDetailsPageBaseProps, IExerciseDetailsPageState> {
    public constructor(props: IExerciseDetailsPageBaseProps) {
        super(props);

        this.state = {
            create: props.create,

            loading: false,
            changed: false,

            videoLink: "",
            videoLinkHint: "",

            exercise: {
                ...EMPTY_EXERCISE,
                name: props.create ? strings.new_exercise : "",
                id: ""
            },

            exerciseGroups: [],
            allExerciseGroups: [],

            photoSources: []
        };
    }

    private async loadPhotoSources(photoPaths: string[]) {
        // Load photos from storage
        return await Promise.all(photoPaths.map(async (photoPath) => {
            try {
                const photoRef = this.props.firebase.storage.ref(photoPath);
                const source = (await photoRef.getDownloadURL()) as string;

                return {
                    storagePath: photoPath,
                    source: source,
                    isNew: false,
                    isRemoved: false,
                    newFile: null
                };
            } catch (exc) {
                if (exc.message !== undefined) {
                    this.props.notifications.showError(exc.message);
                }

                return {
                    storagePath: photoPath,
                    source: "",
                    isNew: false,
                    isRemoved: false,
                    newFile: null
                };
            }
        }));
    }

    public async componentDidMount() {
        const {match, firebase, notifications} = this.props;
        const {create} = this.state;

        this.setState(state => update(state, {loading: {$set: true}}));

        // Load all exercise groups
        try {
            const exerciseGroupsSnapshot = await firebase.firestore.collection(Collections.EXERCISE_GROUPS).orderBy("name").get();
            const exerciseGroups: IExerciseGroupWithID[] = exerciseGroupsSnapshot.docs.map(doc => ({
                ...EMPTY_EXERCISE_GROUP,
                ...doc.data() as IExerciseGroup,
                id: doc.id,
            }));

            this.setState(state => update(state, {
                allExerciseGroups: {$set: exerciseGroups}
            }));
        } catch (exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        // Load existing exercise
        if (!create) {
            try {
                const exerciseDoc = await firebase.firestore.collection(Collections.EXERCISES).doc(match.params.id).get();
                if (!exerciseDoc.exists) {
                    throw new Error(strings.document_does_not_exist);
                }

                const exercise: IExerciseWithID = {
                    ...EMPTY_EXERCISE,
                    ...exerciseDoc.data() as IExercise,
                    id: match.params.id
                };

                const videoLink = exercise.youTubeId !== "" ? `https://www.youtube.com/watch?v=${exercise.youTubeId}` : "";

                this.setState(state => update(state, {
                    videoLink: {$set: videoLink},
                    exercise: {$set: exercise}
                }));


                const groups = await Promise.all(exercise.exerciseGroupRefs.map(ref => ref.get()));
                const validDocs = groups.filter(doc => doc.exists);

                if (validDocs.length < groups.length) {
                    notifications.showError((groups.length - validDocs.length) + strings.x_of_x + (groups.length) + strings.of_exercise_tags_not_exist);
                }

                const exerciseGroups: IExerciseGroupWithID[] = validDocs.map(group => ({
                    ...EMPTY_EXERCISE_GROUP,
                    ...group.data() as IExerciseGroup,
                    id: group.id
                }));

                this.setState(state => update(state, {
                    exerciseGroups: {$set: exerciseGroups}
                }));

                const photoSources = await this.loadPhotoSources(exercise.photoPaths);

                this.setState(state => update(state, {
                    photoSources: {$set: photoSources}
                }));
            } catch (exc) {
                if (exc.message !== undefined) {
                    notifications.showError(exc.message);
                }
            }
        }

        this.setState(state => update(state, {loading: {$set: false}}));
    }

    private static isNumber(charCode: number) {
        return charCode >= 48 && charCode <= 57;
    }

    private static isLetter(charCode: number) {
        return (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122);
    }

    private static validateUID(uid: string): string | null {
        if (uid.length > 8)
            return strings.exercise_code_validation_too_long;
        if (uid.length < 1)
            return strings.exercise_code_validation_too_short;

        if (!this.isLetter(uid.charCodeAt(0)))
            return strings.exercise_code_validation_first_character_should_be_a_letter;

        if (uid.endsWith("-"))
            return strings.exercise_code_validation_cannot_end_with_a_dash;

        if (uid.includes("--"))
            return strings.exercise_code_validation_cannot_contain_two_consecutive_dashes;

        for (let i = 1; i < uid.length; ++i) {
            const charCode = uid.charCodeAt(i);

            if (!this.isLetter(charCode) && !this.isNumber(charCode) && charCode !== 45)
                return strings.exercise_code_validation_can_only_contain_letters_numbers_and_dashes;
        }

        return null;
    }

    private onNameChanged(name: string) {
        this.setState(state => update(state, {
            exercise: {name: {$set: name}},
            changed: {$set: true}
        }));
    }

    private onCodeChanged(code: string) {
        this.setState(state => update(state, {
            exercise: {code: {$set: code.toUpperCase()}},
            changed: {$set: true}
        }));
    }

    private onRequirementsChanged(requirements: string) {
        this.setState(state => update(state, {
            exercise: {requirements: {$set: requirements}},
            changed: {$set: true}
        }));
    }

    private onDescriptionChanged(description: string) {
        this.setState(state => update(state, {
            exercise: {description: {$set: description}},
            changed: {$set: true}
        }));
    }

    private onInstructionsChanged(instructions: string) {
        this.setState(state => update(state, {
            exercise: {instructions: {$set: instructions}},
            changed: {$set: true}
        }));
    }

    private onVideoLinkChanged(videoLink: string) {
        let id = "";
        let hint = "";

        if (videoLink !== "") {
            const metadata = getVideoId(videoLink);
            const isEmpty = !Object.keys(metadata).length;
            if (isEmpty) {
                hint = strings.invalid_video_link;
            } else if (metadata.service !== "youtube") {
                hint = strings.formatString(strings.provided_x_link_only_youtube_videos_are_supported, metadata.service) as string;
            } else {
                id = metadata.id;
            }
        }

        this.setState(state => update(state, {
            videoLink: {$set: videoLink},
            videoLinkHint: {$set: hint},
            exercise: {youTubeId: {$set: id}},
            changed: {$set: true}
        }));
    }

    private async onSave() {
        const {firebase, notifications} = this.props;
        const {create, exercise, exerciseGroups} = this.state;

        const exerciseGroupsRef = firebase.firestore.collection(Collections.EXERCISE_GROUPS);
        const groupsRef = exerciseGroups.map(group => exerciseGroupsRef.doc(group.id));

        const exerciseWithoutId: IExercise = {
            code: exercise.code,
            name: exercise.name,
            iconData: exercise.iconData,
            description: exercise.description,
            instructions: exercise.instructions,
            requirements: exercise.requirements,
            youTubeId: exercise.youTubeId,
            exerciseGroupRefs: groupsRef,
            photoPaths: this.state.photoSources.filter(source => !source.isRemoved).map(source => source.storagePath)
        };

        // console.log("New photo paths: " +exerciseWithoutId.photoPaths)

        this.setState(state => update(state, {loading: {$set: true}}));

        try {
            // Check if exercise with given code exists
            const sameCodeExercises = await firebase.firestore.collection(Collections.EXERCISES).where("code", "==", exerciseWithoutId.code).get();
            sameCodeExercises.docs.forEach(doc => {
                if (doc.id === exercise.id)
                    return;

                throw new Error(strings.exercise_with_given_code_already_exists);
            });

            const removedPhotoSources = this.state.photoSources.filter(source => source.isRemoved);
            const newPhotoSources = this.state.photoSources.filter(source => source.isNew);
            removedPhotoSources.forEach(source => console.log("Removed photo: " + source.storagePath));
            newPhotoSources.forEach(source => console.log("New photo: " + source.storagePath));

            // Remove deleted photos from storage
            try {
                await Promise.all(removedPhotoSources.map(source => this.props.firebase.storage.ref(source.storagePath).delete()));
            } catch (exc) {
                if (exc.message !== undefined) {
                    notifications.showError(`${strings.error_removing_photo}: ${exc.message}`);
                }
            }

            await Promise.all(newPhotoSources.map(source => this.props.firebase.storage.ref(source.storagePath).put(source.newFile!, {contentType: source.newFile!.type})));

            if (create) {
                const exerciseDoc = await firebase.firestore.collection(Collections.EXERCISES).add(exerciseWithoutId);

                this.setState(state => update(state, {
                    create: {$set: false},
                    exercise: {id: {$set: exerciseDoc.id}}
                }));

                notifications.showSuccess(strings.successfully_created_new_exercise);
            } else {
                await firebase.firestore.collection(Collections.EXERCISES).doc(exercise.id).update(exerciseWithoutId);

                notifications.showSuccess(strings.successfullt_saved_changes);
            }

            const photoSources = await this.loadPhotoSources(exerciseWithoutId.photoPaths);

            this.setState(state => update(state, {
                changed: {$set: false}, photoSources: { $set: photoSources }
            }));
        } catch (exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, {loading: {$set: false}}));
    }

    private async onDelete() {
        const {firebase, history, notifications, confirmation} = this.props;
        const {exercise: {id}, photoSources} = this.state;

        const dialogResult = await confirmation.showConfirmation({
            icon: IconNames.TRASH,
            intent: Intent.DANGER,
            cancelButtonText: strings.cancel,
            confirmButtonText: strings.delete,
            message: strings.delete_exercise_confirmation_question
        });

        if (!dialogResult) {
            return;
        }

        this.setState(state => update(state, {loading: {$set: true}}));

        try {
            await firebase.firestore.collection(Collections.EXERCISES).doc(id).delete();
        } catch (exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        try {
            await Promise.all(photoSources.filter(source => !source.isNew).map(source => this.props.firebase.storage.ref(source.storagePath).delete()));
        } catch (exc) {
            if (exc.message !== undefined) {
                notifications.showError(`${strings.error_removing_photo}: ${exc.message}`);
            }
        }

        this.setState(state => update(state, {loading: {$set: false}}));

        history.push(Routes.EXERCISES);
    }

    private onAddNewExercise() {
        const {history} = this.props;
        history.push(Routes.NEW_EXERCISE);
    }

    private readonly onSelectGroups = (exerciseGroups: IExerciseGroupWithID[]) => {
        this.setState(state => update(state, {
            exerciseGroups: {$set: exerciseGroups},
            changed: {$set: true}
        }));
    }

    private readonly photosAccepted = async (photos: File[]) => {
        const {notifications} = this.props;

        if (photos.length === 0)
            return;

        const newPhotoSources: PhotoSource[] = [];

        for (const photo of photos) {
            const source = await ExerciseDetailsPageBase.loadImage(photo);
            if (source === null || typeof source !== "string") {
                notifications.showError(`${strings.error_loading_photo_file}: "${photo.name}"`);
                continue;
            }

            const extension = photo.name.substr(photo.name.lastIndexOf(".") + 1);
            newPhotoSources.push({
                storagePath: "ExercisePhotos/" + uuid() + "." + extension,
                source: source,
                isNew: true,
                isRemoved: false,
                newFile: photo
            });
        }

        this.setState(state => update(state, {
            photoSources: {$push: newPhotoSources},
            changed: {$set: newPhotoSources.length > 0}
        }));
    };

    private readonly photosRejected = async (photos: File[]) => {
        const {notifications} = this.props;

        if (photos.length === 0)
            return;

        const photoNames = photos.map(photo => photo.name).join(", ");
        notifications.showError(`${strings.invalid_photo_files}: "${photoNames}"`);
    };

    private readonly iconAccepted = async (files: File[]) => {
        const {notifications} = this.props;

        if (files.length === 0)
            return;

        const file = files[0];
        const iconData = await ExerciseDetailsPageBase.loadImage(file);
        if (!iconData || typeof iconData !== "string") {
            notifications.showError(`${strings.error_loading_icon_file}: "${file.name}"`);
            return;
        }

        this.setState(state => update(state, {
            exercise: {iconData: {$set: iconData}},
            changed: {$set: true}
        }));
    }

    private readonly iconRejected = async (files: File[]) => {
        const {notifications} = this.props;

        if (files.length === 0)
            return;

        const file = files[0];
        notifications.showError(`${strings.invalid_icon_file}: "${file.name}"`);
    };

    private removeIcon() {
        this.setState(state => update(state, {
            exercise: {iconData: {$set: null}},
            changed: {$set: true}
        }));
    }

    private readonly onRemovePhoto = (photoIdx: number, photoSource: PhotoSource) => {
        if (photoSource.isNew) {
            this.setState(state => update(state, {
                photoSources: {$splice: [[photoIdx, 1]]},
                changed: {$set: true}
            }));
        } else {
            this.setState(state => update(state, {
                photoSources: {[photoIdx]: {isRemoved: {$set: true}}},
                changed: {$set: true}
            }));
        }
    };

    private static loadImage(file: File): Promise<string | ArrayBuffer | null> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.onerror = () => reject(null);
            reader.readAsDataURL(file);
        });
    }

    public render() {
        const {
            create,
            exercise,
            changed,
            loading,
            videoLink,
            videoLinkHint,
            exerciseGroups,
            allExerciseGroups,
        } = this.state;

        const hasVideo = (exercise.youTubeId !== "");
        const exerciseCodeValidation = ExerciseDetailsPageBase.validateUID(exercise.code);
        const canSave = changed && exercise.description !== "" && exercise.instructions !== "" && exercise.name !== "" && exerciseCodeValidation === null;
        const loadingClass = loading ? Classes.SKELETON : "";

        return (
            <Container>
                <Row>
                    <Col sm={7}>
                        <FormGroup
                            label={strings.exercise_code_and_name}
                            labelInfo={exerciseCodeValidation ? `(${exerciseCodeValidation})` : undefined}
                        >
                            <H1>
                                <ControlGroup>
                                    <EditableText
                                        className={loadingClass}
                                        disabled={loading}
                                        placeholder={`${strings.exercise_code}...`}
                                        value={exercise.code}
                                        onChange={value => this.onCodeChanged(value)}
                                        maxLength={8}
                                    />
                                    <span style={{marginLeft: "0.5em"}}/>
                                    <EditableText
                                        className={loadingClass}
                                        disabled={loading}
                                        placeholder={`${strings.exercise_name}...`}
                                        value={exercise.name}
                                        onChange={value => this.onNameChanged(value)}
                                    />
                                </ControlGroup>
                            </H1>
                        </FormGroup>
                    </Col>
                    <Col sm={5}>
                        <ButtonGroup className="align-right">
                            <Button icon={IconNames.FLOPPY_DISK} onClick={() => this.onSave()}
                                    disabled={!canSave}>{strings.save}</Button>
                            {create ||
                            <Button icon={IconNames.DELETE} onClick={() => this.onDelete()}>{strings.delete}</Button>}
                            {create || <Button icon={IconNames.ADD}
                                               onClick={() => this.onAddNewExercise()}>{strings.new_exercise}</Button>}
                        </ButtonGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup
                            label={strings.exercise_tags}
                            labelInfo={`(${strings.optional})`}>
                            <ExerciseGroupMultiSelect
                                fill={true}
                                allGroups={allExerciseGroups}
                                selectedGroups={exerciseGroups}
                                onSelectGroups={this.onSelectGroups}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12} lg={6}>
                        <FormGroup
                            label={strings.exercise_icon}
                        >
                            <StyledDropzone
                                onDropAccepted={this.iconAccepted}
                                onDropRejected={this.iconRejected}
                                multiple={false}
                                accept={"image/*"}
                                maxSize={1024 * 1024 * 1024}
                            >
                                {
                                    exercise.iconData !== null &&
                                    <div style={{width: 100, position: "relative", margin: "0 auto"}}>
                                        <Button minimal small style={{
                                            position: "absolute",
                                            right: -15,
                                            top: -15,
                                            borderRadius: "50%"
                                        }} icon={IconNames.REMOVE} intent={Intent.DANGER} onClick={(e: any) => {
                                            e.stopPropagation();
                                            this.removeIcon()
                                        }}/>
                                        <img src={exercise.iconData} alt="Icon" style={{width: "100%"}}/>
                                    </div>
                                }
                                {
                                    exercise.iconData === null &&
                                    strings.drop_icon_file_here_or_clic_here_to_select_icon_file
                                }
                            </StyledDropzone>
                        </FormGroup>

                        <FormGroup
                            label={strings.description}>
                            <EditableText
                                className={loadingClass}
                                disabled={loading}
                                multiline
                                minLines={3}
                                maxLines={10}
                                placeholder={`${strings.description}...`}
                                value={exercise.description}
                                onChange={value => this.onDescriptionChanged(value)}/>
                        </FormGroup>

                        <FormGroup
                            label={strings.requirements}
                            labelInfo={`(${strings.optional})`}>
                            <EditableText
                                className={loadingClass}
                                disabled={loading}
                                multiline
                                minLines={3}
                                maxLines={10}
                                placeholder={`${strings.requirements}... (${strings.optional})`}
                                value={exercise.requirements}
                                onChange={value => this.onRequirementsChanged(value)}/>
                        </FormGroup>
                    </Col>
                    <Col sm={12} lg={6}>
                        <FormGroup
                            label={strings.instructions}>
                            <EditableText
                                className={loadingClass}
                                disabled={loading}
                                multiline
                                minLines={10}
                                maxLines={20}
                                placeholder={`${strings.instructions}...`}
                                value={exercise.instructions}
                                onChange={value => this.onInstructionsChanged(value)}/>
                        </FormGroup>

                        <FormGroup
                            label={strings.photos}
                        >
                            <StyledDropzone
                                onDropAccepted={this.photosAccepted}
                                onDropRejected={this.photosRejected}
                                multiple={true}
                                accept={"image/*"}
                            >
                                {
                                    this.state.photoSources.length === 0 &&
                                    strings.drop_photos_here_or_clic_here_to_select_a_photo
                                }
                                {
                                    this.state.photoSources.length > 0 &&
                                    <div style={{position: "relative"}}>
                                        {
                                            this.state.photoSources.map((photoSource, photoIdx) => (
                                                !photoSource.isRemoved &&
                                                <div key={`exercise-photo-${photoSource.storagePath}`}
                                                     style={{height: 100, position: "relative", marginTop: 10}}>
                                                    <Button minimal small style={{
                                                        position: "absolute",
                                                        right: -15,
                                                        top: -15,
                                                        borderRadius: "50%"
                                                    }} icon={IconNames.REMOVE} intent={Intent.DANGER}
                                                            onClick={(e: any) => {
                                                                e.stopPropagation();
                                                                this.onRemovePhoto(photoIdx, photoSource);
                                                            }}/>
                                                    <img src={photoSource.source} alt={`IMG ${photoIdx + 1}`}
                                                         style={{height: "100%"}}/>
                                                </div>
                                            ))
                                        }
                                    </div>
                                }
                            </StyledDropzone>
                        </FormGroup>

                        <FormGroup
                            label={strings.instructions_preview}
                        >
                            <TextWithPhotos
                                text={this.state.exercise.instructions}
                                photoSources={this.state.photoSources.map(photo => photo.source)}
                            />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12} lg={6}>
                        <FormGroup
                            inline
                            label={strings.youtube_video_link}
                            labelInfo={`(${strings.optional})`}
                            helperText={videoLinkHint === "" ? undefined : videoLinkHint}
                            intent={Intent.DANGER}>
                            <EditableText
                                className={loadingClass}
                                disabled={loading}
                                placeholder={strings.youtube_video_link}
                                value={videoLink}
                                onChange={value => this.onVideoLinkChanged(value)}/>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {hasVideo && <Video youTubeId={exercise.youTubeId}/>}
                    </Col>
                </Row>
                {loading && <ProgressBar intent={Intent.PRIMARY}/>}
            </Container>
        );
    }
}

const ExerciseDetailsPage = withConfirmations(withNotifications(withFirebase(withRouter(ExerciseDetailsPageBase))));

export const NewExerciseDetailsPage = (props: IExerciseDetailsPageProps) => (
    <ExerciseDetailsPage create={true} {...props}/>
);

export const ExistingExerciseDetailsPage = (props: IExerciseDetailsPageProps) => (
    <ExerciseDetailsPage create={false} {...props}/>
);

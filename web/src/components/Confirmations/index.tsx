import * as React from "react";

import update from "immutability-helper";

import { IconName } from "@blueprintjs/icons";
import { Intent, Alert } from "@blueprintjs/core";
import strings from "../../constants/strings";


type EmptyFunction = () => void;

export interface IConfirmationData {
    confirmButtonText?: string;
    cancelButtonText?: string;
    message: JSX.Element | string;

    intent?: Intent;
    icon?: IconName;   
}

export interface IConfirmationDataCallbacks extends IConfirmationData {
    onConfirm: EmptyFunction;
    onCancel: EmptyFunction;
}

type ConfirmationFunction = (confirmationData: IConfirmationDataCallbacks) => void;

class ConfirmationService {
    public constructor(private showConfirmationFunction: ConfirmationFunction) {

    }

    public readonly showConfirmation = (confirmationData: IConfirmationData) =>
        new Promise((resolve: (result: boolean) => void) => {
            this.showConfirmationFunction({ 
                ...confirmationData, 
                onCancel: () => resolve(false),
                onConfirm: () => resolve(true)
            })
        });
}



export interface IConfirmationProps {
    confirmation: ConfirmationService;
}

type Omitted<P extends IConfirmationProps> = Pick<P, Exclude<keyof P, keyof IConfirmationProps>>;

export function withConfirmations<P extends IConfirmationProps>(Component: React.ComponentType<P>): React.ComponentType<Omitted<P>> {

    interface IWithConfirmationsState {
        confirmationOpen: boolean;
        confirmationData: IConfirmationDataCallbacks;
    }
    
    return class WithConfirmations extends React.Component<Omitted<P>, IWithConfirmationsState> {
        
        constructor(props: Omitted<P>) {
            super(props);

            this.state = {
                confirmationOpen: false,
                confirmationData: {
                    cancelButtonText: "",
                    confirmButtonText: "",
                    message: "",
                    onCancel: () => {},
                    onConfirm: () => {}
                }
            };
        }

        private readonly showConfirmation: ConfirmationFunction = (confirmationData) => {
            this.setState(state => update(state, { 
                confirmationOpen: { $set: true }, 
                confirmationData: { $set: { ...confirmationData } }
            } ));
        }
    
        private readonly onConfirm = () => {
            this.setState(state => update(state, {
                confirmationOpen: { $set: false }
            }), this.state.confirmationData.onConfirm);
        }

        private readonly onCancel = () => {
            this.setState(state => update(state, { 
                confirmationOpen: { $set: false } 
            }), this.state.confirmationData.onCancel);
        }

        render() {
            const { ...props } = this.props;
            const { confirmationOpen, confirmationData } = this.state;

            const confirmationService = new ConfirmationService(this.showConfirmation);

            const confirmText = confirmationData.confirmButtonText ? confirmationData.confirmButtonText : strings.ok;
            const cancelText = confirmationData.cancelButtonText ? confirmationData.cancelButtonText : strings.cancel;

            return (
                <div>
                    <Alert
                        isOpen={confirmationOpen}
                        confirmButtonText={confirmText}
                        cancelButtonText={cancelText}
                        icon={confirmationData.icon}
                        intent={confirmationData.intent}
                        onConfirm={this.onConfirm}
                        onCancel={this.onCancel}
                    >
                        <p>{confirmationData.message}</p>
                    </Alert>

                    <Component {...props as P} confirmation={confirmationService} />
                </div>
            );
        }
    }
}

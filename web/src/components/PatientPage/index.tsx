import React from "react"
import { RouteComponentProps, withRouter } from "react-router-dom";

import update from "immutability-helper"
import { INotificationProps, withNotifications } from "../Notifications";
import { IFirebaseContextProps, withFirebase } from "../Firebase";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import IPatient, { EMPTY_PATIENT } from "@saqq/telerehab-infrastructure/lib/interfaces/patient";
import strings from "../../constants/strings";
import { Tab, Tabs, H2, Divider } from "@blueprintjs/core";
import PatientExercisePlanPage from "../PatientExercisePlan";
import Loading from "../Loading";
import { CancellablePromiseComponent, PromiseCancelled } from "../../utils/cancellable-promise";


interface IPatientPageProps extends RouteComponentProps<any>, INotificationProps, IFirebaseContextProps {

}

interface IPatientPageState {
    loading: boolean;
    patient: IPatient;
}

class PatientPageBase extends CancellablePromiseComponent<IPatientPageProps, IPatientPageState> {

    public constructor(props: IPatientPageProps) {
        super(props);

        this.state = {
            loading: false,
            patient: EMPTY_PATIENT
        };
    }

    async componentDidMount() {
        const { firebase, notifications, match } = this.props;

        this.setState(state => update(state, { loading: { $set: true }}));

        try {
            const patientDoc = await this.addCancelablePromise(firebase.firestore.collection(Collections.PATIENTS).doc(match.params.id).get());
            if (!patientDoc.exists) {
                notifications.showError(strings.patient_does_not_exist);
            } else {
                const patient: IPatient = {
                    ...EMPTY_PATIENT,
                    ...patientDoc.data() as IPatient,
                };
                this.setState(state => update(state, {patient: {$set: patient}}));
            }
        } catch(exc) {
            if (exc instanceof PromiseCancelled) {
                return;
            } else if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, { loading: { $set: false }}));
    }

    render() {
        const { loading, patient } = this.state;

        return (
            <div>
                <H2>{`${patient.name} ${patient.lastName}`}</H2>
                <Divider />

                <Loading loading={loading}>
                    <Tabs large>
                        <Tab id="plan" title={strings.exercise_plan} panel={<PatientExercisePlanPage />} />
                    </Tabs>
                </Loading>
            </div>
        );
    }
}


const PatientPage = withFirebase(withNotifications(withRouter(PatientPageBase)));

export default PatientPage;

import React from "react";

import { Container, Row, Col } from "react-grid-system";
import { H2, HTMLTable } from "@blueprintjs/core";
import { withFirebase, IFirebaseContextProps } from "../Firebase";
import { withRouter, RouteComponentProps } from "react-router-dom";
import Routes from "../../constants/routes";

import IUser, { EMPTY_USER } from "@saqq/telerehab-infrastructure/lib/interfaces/user";
import IUserWithID from "@saqq/telerehab-infrastructure/lib/interfaces/user-with-id";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";

import update from "immutability-helper";
import strings from "../../constants/strings";
import Loading from "../Loading";


interface IUserPageProps extends IFirebaseContextProps, RouteComponentProps<any> {

}

interface IUserPageState {
    loading: boolean;
    users: IUserWithID[];
}


class UsersPageBase extends React.Component<IUserPageProps, IUserPageState> {
    constructor(props: IUserPageProps) {
        super(props);

        this.state = {
            loading: false,
            users: [],
        };
    }

    componentDidMount() {
        const { firebase } = this.props;
        this.setState(state => update(state, { loading: { $set: true } }));

        firebase.firestore.collection(Collections.USERS).orderBy("email")
            .get()
            .then((querySnapshot) => {
                this.setState(state => update(state, {
                    users: {
                        $set: querySnapshot.docs.map(doc => ({
                            ...EMPTY_USER,
                            ...doc.data() as IUser,
                            id: doc.id,
                        })),
                    }
                }));
            })
            .finally(() => {
                this.setState(state => update(state, { loading: { $set: false } }));
            }).catch();
    }

    onUserClicked(id: string) {
        const { history } = this.props;

        history.push(Routes.UserRoute(id));
    }

    render() {
        const { loading, users } = this.state;

        return (
            <Container>
                <Row>
                    <Col sm={12}>
                        <H2>{strings.navigation_users}</H2>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <Loading loading={loading}>
                            <HTMLTable interactive>
                                <tbody>
                                {users.map(user => (
                                    <tr key={user.id} onClick={() => this.onUserClicked(user.id)}>
                                        <td>{user.email}</td>
                                        <td>{user.displayName}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </HTMLTable>
                        </Loading>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const UsersPage = withRouter(withFirebase(UsersPageBase));

export default UsersPage;

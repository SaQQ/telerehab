import React from "react"
import { IAuthUserContextProps, withAuthUser } from "../Session/context";

interface IForRolesProps extends IAuthUserContextProps {
    roles: string[];
}

const ForRolesBase: React.FunctionComponent<IForRolesProps> = (props) => {
    if (props.authUserData === null) {
        return null;
    }

    const userRoles = props.authUserData.roles;
    if (!props.roles.some(role => userRoles.includes(role))) {
        return null;
    }

    return (
        <div>
            { props.children }
        </div>
    );
}

const ForRoles = withAuthUser(ForRolesBase);

export default ForRoles;

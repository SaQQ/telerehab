import * as React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";

import { Card, Elevation, InputGroup, ProgressBar, Intent, H2, FormGroup, Callout } from "@blueprintjs/core";

import { Container, Row, Col } from "react-grid-system";

import update from "immutability-helper";

import { IconNames } from "@blueprintjs/icons";

import Routes from "../../constants/routes";

import { SignUpLink } from "../SignUp";
import { withFirebase, IFirebaseContextProps } from "../Firebase"; 
import strings from "../../constants/strings";


const SignInPage = () => (
    <Container>
        <Row justify="center">
            <Col sm={12} lg={4}>
                <Card elevation={Elevation.TWO}>
                    <H2>{strings.sign_in}</H2>
                    <SignInForm/>
                    <SignUpLink/>
                </Card>
            </Col>
        </Row>
    </Container>
);

interface ISignInFormProps extends RouteComponentProps<any>, IFirebaseContextProps {

}

interface ISignInFormState {
    email: string;
    password: string;
    busy: boolean;
    error: string | null;
}

const INITIAL_STATE: ISignInFormState = {
    email: "",
    password: "",
    busy: false,
    error: null,
};

class SignInFormBase extends React.Component<ISignInFormProps, ISignInFormState> {
    public constructor(props: ISignInFormProps) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    private onSubmit(): void {
        const { email, password } = this.state;
        const { firebase, history } = this.props;

        this.setState(state => update(state, { 
            busy: { $set: true }, 
            error: { $set: null } 
        }));
        
        firebase.signInWithEmailAndPassword(email, password)
            .then(() => {
                history.push(Routes.HOME);
            })
            .catch((error) => {
                this.setState(state => update(state, {
                    busy: { $set: false },
                    error: { $set: error.message }
                }));
            });
    }

    private onEmailChange(value: string): void {
        this.setState(state => update(state, { email: { $set: value } }));
    }

    private onPasswordChange(value: string): void {
        this.setState(state => update(state, { password: { $set: value } }));
    }

    public render(): React.ReactNode {
        const {
            email, password, busy, error,
        } = this.state;

        const isInvalid = password === "" || email === "";

        return (
            <form
                onSubmit={e => {
                  e.preventDefault();
                  this.onSubmit();
                }}
            >
                <FormGroup>
                    <InputGroup
                        large
                        placeholder={strings.enter_email}
                        leftIcon={IconNames.ENVELOPE}
                        type="email"
                        value={email}
                        disabled={busy}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onEmailChange(event.target.value)}
                    />
                    <InputGroup
                        large
                        placeholder={strings.enter_password}
                        leftIcon={IconNames.LOCK}
                        type="password"
                        value={password}
                        disabled={busy}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onPasswordChange(event.target.value)}
                    />
                    <InputGroup
                        large
                        type="submit"
                        value={strings.sign_in}
                        disabled={busy || isInvalid}
                    />
                    {busy && <ProgressBar intent={Intent.PRIMARY}/>}
                    {error && <Callout intent={Intent.DANGER}>{error}</Callout>}
                </FormGroup>
            </form>
        );
    }
}

const SignInForm = withRouter(withFirebase(SignInFormBase));

export default SignInPage;

export { SignInForm };

import * as React from "react";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";

import { FormGroup, InputGroup, ProgressBar, Intent, H2, Card, Elevation, Callout } from "@blueprintjs/core";

import { IconNames } from "@blueprintjs/icons";

import Routes from "../../constants/routes";

import { withFirebase, IFirebaseContextProps } from "../Firebase";
import { Container, Row, Col } from "react-grid-system";

import update from "immutability-helper";
import strings from "../../constants/strings";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections" ;


const SignUpPage = () => (
    <Container>
        <Row justify="center">
            <Col sm={12} lg={4}>
                <Card elevation={Elevation.TWO}>
                    <H2>{strings.sign_up}</H2>
                    <SignUpForm/>
                </Card>
            </Col>
        </Row>
    </Container>
);

interface ISignUpFormProps extends IFirebaseContextProps, RouteComponentProps<any> {

}

interface ISignUpFormState {
    username: string;
    email: string;
    passwordOne: string;
    passwordTwo: string;
    busy: boolean;
    errorMessage: string | null;
}

const INITIAL_STATE: ISignUpFormState = {
    username: "",
    email: "",
    passwordOne: "",
    passwordTwo: "",
    busy: false,
    errorMessage: null,
};

class SignUpFormBase extends React.Component<ISignUpFormProps, ISignUpFormState> {
    constructor(props: ISignUpFormProps) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    async onSubmit() {
        const { history, firebase } = this.props;
        const { username, email, passwordOne } = this.state;

        this.setState(state => update(state, { busy: { $set: true }, errorMessage: { $set: null } }));

        try {
            const userCredentials = await firebase.createUserWithEmailAndPassword(email, passwordOne);
            if (userCredentials === null || userCredentials.user === null) {
                throw new Error(strings.failed_to_create_user);
            }

            const user = userCredentials.user;
            await user.updateProfile({ displayName: username });

            await firebase.firestore.collection(Collections.USERS).doc(user.uid).update({ displayName: username });            
        
            this.setState(state => update(state, { busy: { $set: false }, errorMessage: { $set: null } }));

            history.push(Routes.HOME);
        }
        catch(exc) {
            if (exc instanceof Error) {
                const error: Error = exc;
                this.setState(state => update(state, { busy: { $set: false }, errorMessage: { $set: error.message } }));
            }
        }
    }

    onNameChange(value: string) {
        this.setState(state => update(state, { username: { $set: value } }));
    }

    onEmailChange(value: string) {
        this.setState(state => update(state, { email: { $set: value } }));
    }

    onPasswordOneChange(value: string) {
        this.setState(state => update(state, { passwordOne: { $set: value } }));
    }

    onPasswordTwoChange(value: string) {
        this.setState(state => update(state, { passwordTwo: { $set: value } }));
    }

    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            busy,
            errorMessage,
        } = this.state;

        const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === "" ||
            email === "" ||
            username === "";

        return (
            <form
                onSubmit={(event) => {
                  event.preventDefault();
                  return this.onSubmit();
                }}
            >
                <FormGroup>
                    <InputGroup
                        large
                        placeholder={strings.enter_name}
                        leftIcon={IconNames.USER}
                        type="text"
                        value={username}
                        disabled={busy}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onNameChange(event.target.value)}
                    />
                    <InputGroup
                        large
                        placeholder={strings.enter_email}
                        leftIcon={IconNames.ENVELOPE}
                        type="email"
                        value={email}
                        disabled={busy}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onEmailChange(event.target.value)}
                    />
                    <InputGroup
                        large
                        placeholder={strings.enter_password}
                        leftIcon={IconNames.LOCK}
                        type="password"
                        value={passwordOne}
                        disabled={busy}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onPasswordOneChange(event.target.value)}
                    />
                    <InputGroup
                        large
                        placeholder={strings.repeat_password}
                        leftIcon={IconNames.LOCK}
                        type="password"
                        value={passwordTwo}
                        disabled={busy}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onPasswordTwoChange(event.target.value)}
                    />
                    <InputGroup
                        large
                        type="submit"
                        value={strings.sign_up}
                        disabled={busy || isInvalid}
                    />
                    {busy && <ProgressBar intent={Intent.PRIMARY}/>}
                    {errorMessage && <Callout intent={Intent.DANGER}>{errorMessage}</Callout>}
                </FormGroup>
            </form>
        );
    }
}

const SignUpLink = () => (
    <p>
        {strings.dont_have_an_account_yet_question}{" "}
        <strong><Link to={Routes.SIGN_UP}>{strings.sign_up}</Link></strong>
    </p>
);

const SignUpForm = withRouter(withFirebase(SignUpFormBase));

export default SignUpPage;

export { SignUpForm, SignUpLink };

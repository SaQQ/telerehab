import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Container, Row, Col } from "react-grid-system";

import Routes from "../../constants/routes";
import "./App.css";

import { withAuthentication, IAuthenticationContextProps } from "../Session/withAuthentication";
import { IAuthUserContextProps, withAuthUser } from "../Session/context";

import Navigation from "../Navigation";
import SignUpPage from "../SignUp";
import SignInPage from "../SignIn";
import HomePage from "../Home";
import ExerciseLibraryPage from "../ExerciseLibrary";
import AccountPage from "../Account";
import UsersPage from "../Users";
import {ExistingExerciseDetailsPage, NewExerciseDetailsPage} from "../ExerciseDetails";
import AdminPage from "../Admin";
import ExerciseGroupsPage from "../ExerciseGroups";
import PatientsPage from "../Patients";
import PatientPage from "../PatientPage";
import NotFoundPage from "../NotFoundPage";
import Footer from "../Footer";
import { Divider } from "@blueprintjs/core";

interface IAppProps extends IAuthenticationContextProps, IAuthUserContextProps {

}

const App = (props: IAppProps) => {

    const HasRole = (roles: string[], route: JSX.Element) => {
        if (props.authUserData === null) {
            return undefined;
        }
    
        const userRoles = props.authUserData.roles;
        if (!roles.some(role => userRoles.includes(role))) {
            return undefined;
        }
    
        return route;
    };

    return (
        <Router>
            <Container>
                <Row>
                    <Col sm={12}>
                        <Navigation />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <Switch>
                            <Route exact path={Routes.HOME} component={HomePage} />
                            <Route exact path={Routes.SIGN_UP} component={SignUpPage} />
                            <Route exact path={Routes.SIGN_IN} component={SignInPage} />
                            <Route exact path={Routes.ACCOUNT} component={AccountPage} />
                            <Route exact path={Routes.EXERCISES} component={ExerciseLibraryPage} />
                            <Route exact path={Routes.EXERCISE_GROUPS} component={ExerciseGroupsPage} />
                            <Route exact path={Routes.EXERCISE} component={ExistingExerciseDetailsPage} />
                            <Route exact path={Routes.NEW_EXERCISE} component={NewExerciseDetailsPage} />
                            { HasRole(["admin", "therapist"], <Route exact path={Routes.PATIENTS} component={PatientsPage} />) }
                            <Route exact path={Routes.PATIENT} component={PatientPage} />
                            <Route exact path={Routes.USERS} component={UsersPage} />, 
                            { HasRole(["admin"], <Route exact path={Routes.ADMIN} component={AdminPage} />) }

                            <Route component={NotFoundPage} />
                        </Switch>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <Divider/>
                        <Footer />
                    </Col>
                </Row>
            </Container>
        </Router>
    );
};

export default withAuthentication(withAuthUser(App));

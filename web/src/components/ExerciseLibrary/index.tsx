import React, { ChangeEvent } from "react";

import { Container, Row, Col } from "react-grid-system";

import Pagination from "react-js-pagination";

import { H2, InputGroup, NonIdealState, Button, ControlGroup } from "@blueprintjs/core";

import ExerciseCard from "../ExerciseCard";
import { withFirebase, IFirebaseContextProps } from "../Firebase";

import update from "immutability-helper";

import { IconNames } from "@blueprintjs/icons";

import IExercise, { EMPTY_EXERCISE } from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";
import IExerciseWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-with-id";
import IExerciseGroup, { EMPTY_EXERCISE_GROUP } from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group";
import IExerciseGroupWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group-with-id";

import { RouteComponentProps } from "react-router";
import Routes from "../../constants/routes"
import { INotificationProps, withNotifications } from "../Notifications";
import { IAuthUserContextProps, withAuthUser } from "../Session/context";

import "./exerciseLibrary.css";
import strings from "../../constants/strings";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import ExerciseGroupMultiSelect from "../../utils/exercise-group-multiselect";
import Loading from "../Loading";


interface IExerciseLibraryPageProps extends IFirebaseContextProps, RouteComponentProps<any>, INotificationProps, IAuthUserContextProps {

}

interface IExerciseLibraryPageState {
    loading: boolean;
    allExercises: IExerciseWithID[];
    pageExercises: IExerciseWithID[];
    allExerciseGroups: IExerciseGroupWithID[];
    selectedExerciseGroups: IExerciseGroupWithID[];
    searchText: string;

    pageNumber: number;
    exercisesCount: number;
    exercisesPerPage: number;
}

class ExerciseLibraryPageBase extends React.Component<IExerciseLibraryPageProps, IExerciseLibraryPageState> {
    constructor(props: IExerciseLibraryPageProps) {
        super(props);

        this.state = {
            loading: false,
            allExercises: [],
            pageExercises: [],
            allExerciseGroups: [],
            selectedExerciseGroups: [],
            searchText: "",

            pageNumber: 1,
            exercisesCount: 0,
            exercisesPerPage: 6
        };
    }

    getExercises(filter: string, exerciseGroupIds: string[]) {
        const { firebase } = this.props;
        const lowerFilter = filter.toLowerCase();

        return firebase.firestore.collection(Collections.EXERCISES).orderBy("name")
            .get()
            .then((snapshot) => {
                const exercises: IExerciseWithID[] = snapshot.docs.map(doc => ({
                    ...EMPTY_EXERCISE,
                    ...doc.data() as IExercise,
                    id: doc.id
                }));

                let filteredExercises = exercises;
                if (filter !== "") {
                    filteredExercises = filteredExercises.filter(exercise => exercise.name.toLowerCase().includes(lowerFilter) || exercise.code.toLowerCase().includes(lowerFilter));
                }
                if (exerciseGroupIds.length > 0) {
                    filteredExercises = filteredExercises.filter(exercise => {
                        return exerciseGroupIds.every(id => {
                            return exercise.exerciseGroupRefs.some(ref => ref.id === id);
                        })
                    });
                }

                return filteredExercises;
            });
    }

    updateExercises() {
        const { notifications } = this.props;
        const { searchText, selectedExerciseGroups } = this.state;

        this.setState(state => update(state, {
            loading: { $set: true },
            allExercises: { $set: [] },
            pageExercises: { $set: [] }
        }));

        this.getExercises(searchText, selectedExerciseGroups.map(group => group.id))
            .then((exercises) => {
                this.setState(state => update(state, {
                    exercisesCount: { $set: exercises.length },
                    allExercises: { $set: exercises }
                }), () => {
                    this.updatePageExercises();
                });
            })
            .catch((error) => {
                notifications.showError(error.message);
            })
            .finally(() => {
                this.setState(state => update(state, { loading: { $set: false } }));
            })
            .catch();
    }

    updatePageExercises() {
        const { exercisesPerPage, pageNumber, allExercises } = this.state;
        const startIndex = (pageNumber - 1) * exercisesPerPage;

        let slicedExercises = allExercises;
        if (allExercises.length !== 0) {
            slicedExercises = allExercises.slice(startIndex, Math.min(startIndex + exercisesPerPage, allExercises.length));
        }

        this.setState(state => update(state, { pageExercises: { $set: slicedExercises } }));
    }

    loadExerciseGroups() {
        const { firebase, notifications } = this.props;

        this.setState(state => update(state, {
            loading: { $set: true }
        }));

        firebase.firestore.collection(Collections.EXERCISE_GROUPS).orderBy("name")
            .get()
            .then((snapshot) => {
                const exerciseGroups: IExerciseGroupWithID[] = snapshot.docs.map((doc) => ({
                    ...EMPTY_EXERCISE_GROUP,
                    ...doc.data() as IExerciseGroup,
                    id: doc.id
                }));

                this.setState(state => update(state, {
                    allExerciseGroups: { $set: exerciseGroups }
                }));
            })
            .catch((error: Error) => {
                notifications.showError(error.message);
            })
            .finally(() => {
                this.setState(state => update(state, {
                    loading: { $set: false }
                }));
            })
            .catch();
    }

    componentDidMount() {
        this.loadExerciseGroups();
        this.updateExercises();
    }

    onPageNumberChanged(pageNumber: number) {
        this.setState(state => update(state, { pageNumber: { $set: pageNumber } }), () => {
            this.updatePageExercises();
        });
    }

    onSearchTextChanged(searchText: string) {
        this.setState(state => update(state, {
            searchText: { $set: searchText },
            pageNumber: { $set: 1 }
        }), () => {
            this.updateExercises();
        });
    }

    onAddExercise() {
        const { history } = this.props;
        history.push(Routes.NEW_EXERCISE);
    }

    private readonly onSelectedGroups = (selectedGroups: IExerciseGroupWithID[]) => {
        this.setState(state => update(state, {
            selectedExerciseGroups: { $set: selectedGroups },
            pageNumber: { $set: 1 }
        }), () => {
            this.updateExercises();
        });
    };

    render() {
        const { authUserData } = this.props;
        const { loading, pageExercises, searchText, pageNumber, exercisesCount, exercisesPerPage, allExerciseGroups, selectedExerciseGroups } = this.state;

        return (
            <Container>
                <Row>
                    <Col sm={12}>
                        <H2>{strings.navigation_exercise_library}</H2>


                        <ControlGroup>
                            <ExerciseGroupMultiSelect fill
                                                      allGroups={allExerciseGroups}
                                                      selectedGroups={selectedExerciseGroups}
                                                      onSelectGroups={this.onSelectedGroups}
                            />
                            <InputGroup fill
                                        leftIcon={IconNames.SEARCH}
                                        placeholder={`${strings.exercise_name}...`}
                                        value={searchText}
                                        onChange={(event: ChangeEvent<HTMLInputElement>) => this.onSearchTextChanged(event.target.value)}
                            />
                            {authUserData && <Button icon={IconNames.ADD}
                                                     onClick={() => this.onAddExercise()}>{strings.add_exercise}</Button>}
                        </ControlGroup>
                        <Pagination
                            activePage={pageNumber}
                            itemsCountPerPage={exercisesPerPage}
                            totalItemsCount={exercisesCount}
                            pageRangeDisplayed={7}
                            onChange={newPageNumber => this.onPageNumberChanged(newPageNumber)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <div className="card-list">
                            <Loading loading={loading}>
                                {pageExercises.length === 0 && searchText === "" && (
                                    <NonIdealState
                                        title={strings.library_is_empty}
                                        description={strings.add_some_exercises_to_start}/>
                                )}
                                {pageExercises.length === 0 && searchText !== "" && (
                                    <NonIdealState
                                        title={strings.search_result_is_empty}
                                        description={strings.formatString(strings.no_exercises_with_name_containing, searchText) as string}/>
                                )}
                                {pageExercises.map(ex => {

                                    let groups = ex.exerciseGroupRefs.map(ref => {
                                        const idx = allExerciseGroups.findIndex(group => group.id === ref.id);
                                        return idx < 0 ? null : allExerciseGroups[idx];
                                    });

                                    groups = groups.sort((group1, group2) => {
                                        const name1 = group1 === null ? "" : group1.name;
                                        const name2 = group2 === null ? "" : group2.name;

                                        return name1.localeCompare(name2);
                                    });

                                    return <ExerciseCard key={ex.id} exercise={ex} exerciseGroups={groups}/>
                                })}
                            </Loading>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const ExerciseLibraryPage = withAuthUser(withNotifications(withFirebase(ExerciseLibraryPageBase)));

export default ExerciseLibraryPage;

import React from "react"
import { NonIdealState } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import strings from "../../constants/strings";


export default function NotFoundPage() {
    return (
        <NonIdealState 
            icon={IconNames.GEOSEARCH} 
            title={strings.page_not_found}
        />
    );
}

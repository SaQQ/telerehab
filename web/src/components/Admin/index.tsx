import React from "react"

import "./index.css"

import { H2 } from "@blueprintjs/core";
import update from "immutability-helper"
import { IFirebaseContextProps, withFirebase } from "../Firebase";
import { INotificationProps, withNotifications } from "../Notifications";

import IUserRoleNameWithID from "@saqq/telerehab-infrastructure/lib/interfaces/user-role-name-with-id";
import IUserRoleName, { EMPTY_USER_ROLE_NAME } from "@saqq/telerehab-infrastructure/lib/interfaces/user-role-name";
import IUserRole, { EMPTY_USER_ROLE } from "@saqq/telerehab-infrastructure/lib/interfaces/user-role";
import IUserRoleWithID from "@saqq/telerehab-infrastructure/lib/interfaces/user-role-with-id";
import IUser, { EMPTY_USER } from "@saqq/telerehab-infrastructure/lib/interfaces/user";
import IUserWithID from "@saqq/telerehab-infrastructure/lib/interfaces/user-with-id";

import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import strings from "../../constants/strings";

import Loading from "../Loading";
import UserCard from "./UserCard";



interface IAdminPageProps extends IFirebaseContextProps, INotificationProps {

}

interface IAdminPageState {
    loading: boolean;
    users: IUserWithID[];
    userRoleNames: IUserRoleNameWithID[];
    userRoles: IUserRoleWithID[];
}

interface IWithID {
    id: string;
}

function GetItemWithID<WithID extends IWithID>(array: WithID[], id: string, defaultValue: WithID) {
    const result = array.find(withId => withId.id === id);
    if (result === undefined) {
        return defaultValue;
    }

    return result;
}


class AdminPageBase extends React.Component<IAdminPageProps, IAdminPageState> {
    constructor(props: IAdminPageProps) {
        super(props);

        this.state = {
            loading: false,
            users: [],
            userRoleNames: [],
            userRoles: [],
        };
    }    

    async componentDidMount() {
        const { firebase, notifications } = this.props;

        this.setState(state => update(state, {
            loading: { $set: true }
        }));

        try {
            const usersSnapshot = await firebase.firestore.collection(Collections.USERS).orderBy("email").get();
            const users = usersSnapshot.docs.map(doc => ({
                ...EMPTY_USER,
                ...doc.data() as IUser,
                id: doc.id
            } as IUserWithID));

            this.setState(state => update(state, {
                users: { $set: users }
            }));


            const roleNamesSnapshot = await firebase.firestore.collection(Collections.USER_ROLE_NAMES).get();    
            const userRoleNames = roleNamesSnapshot.docs.map(doc => ({ 
                ...EMPTY_USER_ROLE_NAME, 
                ...doc.data() as IUserRoleName, 
                id: doc.id 
            } as IUserRoleNameWithID));
            
            this.setState(state => update(state, {
                userRoleNames: { $set: userRoleNames }
            }));


            const userRolesSnapshot = await firebase.firestore.collection(Collections.USER_ROLES).get();
            const userRoles = userRolesSnapshot.docs.map(doc => ({
                ...EMPTY_USER_ROLE,
                ...doc.data() as IUserRole,
                id: doc.id
            } as IUserRoleWithID));

            this.setState(state => update(state, {
                userRoles: { $set: userRoles }
            }));
        } 
        catch(exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, {
            loading: { $set: false }
        }));
    }

    render() {
        const { loading, users, userRoleNames, userRoles } = this.state;

        return (
            <div>
                <H2>{strings.admin_panel}</H2>

                <Loading loading={loading}>
                    <div className="card-list">
                        {users.map(user => (
                            <UserCard
                                key={user.id}
                                user={user}
                                userRole={GetItemWithID(userRoles, user.id, { ...EMPTY_USER_ROLE, id: "" })}
                                userRoleNames={userRoleNames}
                            />
                        ))}
                    </div>   
                </Loading>
            </div>
        );
    }
}

const AdminPage = withNotifications(withFirebase(AdminPageBase));

export default AdminPage;

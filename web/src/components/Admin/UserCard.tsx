import React from "react"
import { IFirebaseContextProps, withFirebase } from "../Firebase";
import { Card, Elevation, FormGroup, Checkbox, Button, Collapse, Icon } from "@blueprintjs/core";
import IUserRole from "@saqq/telerehab-infrastructure/lib/interfaces/user-role";
import IUserRoleNameWithID from "@saqq/telerehab-infrastructure/lib/interfaces/user-role-name-with-id"

import update from "immutability-helper"
import IUserWithID from "@saqq/telerehab-infrastructure/lib/interfaces/user-with-id";
import { IconNames } from "@blueprintjs/icons";
import { INotificationProps, withNotifications } from "../Notifications";
import { IAuthUserContextProps, withAuthUser } from "../Session/context";
import { Container, Col, Row } from "react-grid-system";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import strings from "../../constants/strings";

import "./UserCard.css"


interface IUserCardProps extends IFirebaseContextProps, INotificationProps, IAuthUserContextProps {
    user: IUserWithID;
    userRole: IUserRole;
    userRoleNames: IUserRoleNameWithID[];
}

interface IUserCardState {
    expanded: boolean;
    loading: boolean;
    changed: boolean;
    user: IUserWithID;
    userRole: IUserRole;
}

class UserCardBase extends React.Component<IUserCardProps, IUserCardState> {
    constructor(props: IUserCardProps) {
        super(props);

        this.state = {
            expanded: false,
            loading: false,
            changed: false,
            user: { ...this.props.user },
            userRole: { roles: this.props.userRole.roles.slice() }
        };
    }

    componentWillReceiveProps(nextProps: IUserCardProps) {
        this.setState(state => update(state, {
            user: { $set: { ...nextProps.user } },
            userRole: { roles: { $set: nextProps.userRole.roles.slice() } }
        }));
    }

    private readonly onRoleCheckboxChange = (roleId: string, value: boolean) => {
        const { firebase } = this.props;

        const roleRef = firebase.firestore.collection(Collections.USER_ROLE_NAMES).doc(roleId);
        
        this.setState(state => {
            const index = state.userRole.roles.findIndex(ref => ref.id === roleId);            
            if (value) {
                // Add new role
                
                if (index >= 0) {
                    // Nothing to do, already exists
                    return state;
                }
                
                // Do add new role
                return update(state, {
                    userRole: { roles: { $push: [roleRef] } },
                    changed: { $set: true }
                });
            } else {
                // Remove role
                
                if (index < 0) {
                    // Nothing to do, already removed
                    return state;
                }

                // Do remove role
                return update(state, {
                    userRole: { roles: { $splice: [[index, 1]]} },
                    changed: { $set: true }
                });
            }
        });
    };

    private readonly onSaveRoles = async () => {
        const { firebase, notifications } = this.props; 
        const { user, userRole } = this.state;
        
        this.setState(state => update(state, {
            loading: { $set: true }
        }));

        const newUserRole: IUserRole = {
            roles: userRole.roles
        };

        try {
            await firebase.firestore.collection(Collections.USER_ROLES).doc(user.id).update(newUserRole);
        
            this.setState(state => update(state, {
                changed: { $set: false }
            }))
        } catch(exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, {
            loading: { $set: false }
        }));
    };

    private readonly toggleExpand = () => {
        this.setState(state => update(state, {
            expanded: { $apply: (v) => !v }
        }));
    };

    render() {
        const { userRoleNames, authUserData } = this.props;
        const { expanded, changed, loading, user, userRole } = this.state;

        return (
            <div>
                <Card interactive onClick={this.toggleExpand} elevation={expanded ? Elevation.FOUR : Elevation.TWO}>
                    <Container>
                        <Row>
                            <Col>
                                <FormGroup label={`${strings.login}${authUserData && user.id === authUserData.user.uid ? ` (${strings.current_user})` : ""}`}>
                                    {user.email}
                                </FormGroup>
                            </Col>

                            <Col>
                                <FormGroup label={strings.user_name}>
                                    {user.displayName}
                                </FormGroup>
                            </Col>
                        </Row>
                    </Container>
                    
                    <Icon className="align-right" icon={expanded ? IconNames.CHEVRON_UP : IconNames.CHEVRON_DOWN} />
                </Card>

                <Collapse isOpen={expanded}>
                    <Container>
                        <Row>
                            <Col>
                                <Card interactive elevation={Elevation.TWO}>
                                    <FormGroup label={strings.roles}>
                                        {userRoleNames.map(role => (
                                            <Checkbox 
                                                key={role.id}
                                                checked={userRole.roles.find(ref => ref.id === role.id) !== undefined}
                                                onClick={(event) => { event.stopPropagation(); event.nativeEvent.stopImmediatePropagation(); }}
                                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onRoleCheckboxChange(role.id, event.target.checked)}
                                            >
                                                {role.name}
                                            </Checkbox>
                                        ))}

                                        <Collapse isOpen={changed || loading}>
                                            <Button 
                                                disabled={!changed}
                                                icon={IconNames.FLOPPY_DISK}
                                                loading={loading}
                                                onClick={this.onSaveRoles}
                                                >
                                                {strings.save}
                                            </Button>
                                        </Collapse>

                                    </FormGroup>
                                </Card>
                            </Col>
                        </Row>                        
                    </Container>
                </Collapse>
            </div>
        );
    }
}

const UserCard = withAuthUser(withNotifications(withFirebase(UserCardBase)));

export default UserCard;

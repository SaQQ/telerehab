import React from "react";

import { Container, Row, Col } from "react-grid-system";
import { H2, HTMLTable, Button, Dialog, Classes, Intent, FormGroup, InputGroup } from "@blueprintjs/core";
import { withFirebase, IFirebaseContextProps } from "../Firebase";
import { withRouter, RouteComponentProps } from "react-router-dom";
import Routes from "../../constants/routes";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";

import update from "immutability-helper";
import strings from "../../constants/strings";
import IPatientWithID from "@saqq/telerehab-infrastructure/lib/interfaces/patient-with_id";
import IPatient, { EMPTY_PATIENT } from "@saqq/telerehab-infrastructure/lib/interfaces/patient";
import { INotificationProps, withNotifications } from "../Notifications";
import { IAuthUserContextProps, withAuthUser } from "../Session/context";
import { IconNames } from "@blueprintjs/icons";
import Loading from "../Loading";


interface IPatientsPageProps extends IFirebaseContextProps, RouteComponentProps<any>, INotificationProps, IAuthUserContextProps {

}

interface IPatientsPageState {
    loading: boolean;
    patients: IPatientWithID[];

    addPatientOpen: boolean;
    addPatientLoading: boolean;
    addPatientID: string;
    addPatientName: string;
    addPatientLastName: string;
}

class PatientsPageBase extends React.Component<IPatientsPageProps, IPatientsPageState> {
    constructor(props: IPatientsPageProps) {
        super(props);

        this.state = {
            loading: false,
            patients: [],

            addPatientOpen: false,
            addPatientLoading: false,
            addPatientID: "",
            addPatientName: "",
            addPatientLastName: "",
        };
    }

    async componentDidMount() {
        return this.reloadData(true);
    }

    private async reloadData(reloadPatients: boolean) {
        const { firebase, notifications, authUserData } = this.props;

        const therapistUID = authUserData!.user.uid;

        this.setState(state => update(state, { loading: { $set: true } }));

        try {
            if (reloadPatients) {
                const query = firebase.firestore.collection(Collections.PATIENTS).where("therapistUID", "==", therapistUID);

                const patientsSnapshot = await query.get();
                const patients: IPatientWithID[] = patientsSnapshot.docs.map(doc => ({
                    ...EMPTY_PATIENT,
                    ...doc.data() as IPatient,
                    id: doc.id
                }));

                this.setState(state => update(state, {
                    patients: { $set: patients },
                }));
            }
        } catch (exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, { loading: { $set: false } }));
    }

    private onPatientClicked(id: string) {
        const { history } = this.props;

        history.push(Routes.PatientRoute(id));
    }

    private readonly onAddPatient = () => {
        this.setState(state => update(state, {
            addPatientOpen: { $set: true },
            addPatientID: { $set: "" },
            addPatientName: { $set: "" },
            addPatientLastName: { $set: "" },
        }));
    };

    private readonly onAddPatientConfirm = async () => {
        const { firebase, notifications, authUserData } = this.props;
        const { addPatientID, addPatientName, addPatientLastName } = this.state;

        this.setState(state => update(state, {
            addPatientLoading: { $set: true }
        }));

        try {
            if (addPatientID === "") {
                throw new Error(strings.no_user_selected);
            }
            if (addPatientName === "" || addPatientLastName === "") {
                throw new Error(strings.missing_patient_data);
            }

            const newPatient: IPatient = {
                name: addPatientName,
                lastName: addPatientLastName,
                therapistUID: authUserData!.user.uid ? authUserData!.user.uid : ""
            };

            const docRef = firebase.firestore.collection(Collections.PATIENTS).doc(addPatientID);

            const doc = await docRef.get();
            if (doc.exists) {
                throw new Error(strings.patient_with_given_id_already_exists);
            } else {
                await docRef.set(newPatient);
                this.onAddPatientClose();
                notifications.showSuccess(strings.successfully_added_new_patient);
                await this.reloadData(true);
            }
        } catch (exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, {
            addPatientLoading: { $set: false }
        }));
    };

    private readonly onAddPatientClose = () => {
        this.setState(state => update(state, {
            addPatientOpen: { $set: false }
        }));
    };

    private onAddPatientIDChange(id: string) {
        this.setState(state => update(state, {
            addPatientID: { $set: id }
        }));
    }

    private onAddPatientNameChange(name: string) {
        this.setState(state => update(state, {
            addPatientName: { $set: name }
        }));
    }

    private onAddPatientLastNameChange(lastName: string) {
        this.setState(state => update(state, {
            addPatientLastName: { $set: lastName }
        }));
    }

    public render() {
        const { authUserData } = this.props;
        const { loading, patients, addPatientOpen, addPatientLoading, addPatientID, addPatientName, addPatientLastName } = this.state;

        return (
            <Container>
                <Dialog
                    icon={IconNames.ADD}
                    onClose={this.onAddPatientClose}
                    isOpen={addPatientOpen}
                    title={strings.add_patient}
                >
                    <div className={Classes.DIALOG_BODY}>

                        <FormGroup label={strings.patient_id}>
                            <InputGroup
                                type={"text"}
                                value={addPatientID}
                                placeholder={`${strings.patient_id}...`}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onAddPatientIDChange(event.target.value)}
                            />
                        </FormGroup>

                        <FormGroup label={strings.name}>
                            <InputGroup
                                type={"text"}
                                value={addPatientName}
                                placeholder={`${strings.name}...`}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onAddPatientNameChange(event.target.value)}
                            />
                        </FormGroup>

                        <FormGroup label={strings.last_name}>
                            <InputGroup
                                type={"text"}
                                value={addPatientLastName}
                                placeholder={`${strings.last_name}...`}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.onAddPatientLastNameChange(event.target.value)}
                            />
                        </FormGroup>
                    </div>

                    <div className={Classes.DIALOG_FOOTER}>
                        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                            <Button onClick={this.onAddPatientClose}>
                                {strings.cancel}
                            </Button>
                            <Button loading={addPatientLoading}
                                    disabled={addPatientID === "" || addPatientName === "" || addPatientLastName === ""}
                                    intent={Intent.SUCCESS} onClick={this.onAddPatientConfirm}>
                                {strings.add}
                            </Button>
                        </div>
                    </div>
                </Dialog>

                <Row>
                    <Col sm={12}>
                        <H2>{strings.navigation_patients}</H2>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <Loading loading={loading}>
                            <HTMLTable interactive style={{ width: "100%" }}>
                                <tbody>
                                <tr>
                                    <th>{strings.last_name}</th>
                                    <th>{strings.name}</th>
                                    <th>{strings.patient_id}</th>
                                    {
                                        authUserData && (authUserData.roles.includes("therapist") || authUserData.roles.includes("admin")) &&
                                        <th>
                                            <Button onClick={this.onAddPatient}>
                                                {strings.add}
                                            </Button>
                                        </th>
                                    }
                                </tr>
                                {patients.map(patient => (
                                    <tr key={patient.id} onClick={() => this.onPatientClicked(patient.id)}>
                                        <td>{patient.lastName}</td>
                                        <td>{patient.name}</td>
                                        <td>{patient.id}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </HTMLTable>
                        </Loading>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const PatientsPage = withAuthUser(withNotifications(withRouter(withFirebase(PatientsPageBase))));

export default PatientsPage;

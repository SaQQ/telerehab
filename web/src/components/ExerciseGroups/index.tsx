import React from "react"

import { IFirebaseContextProps, withFirebase } from "../Firebase";
import { INotificationProps, withNotifications } from "../Notifications";
import { IConfirmationProps, withConfirmations } from "../Confirmations";
import update from "immutability-helper";
import { UL,  Divider, Intent, H2, H5 } from "@blueprintjs/core";
import ExerciseGroupEdit from "./exercise-group-edit";
import IExerciseGroup, { EMPTY_EXERCISE_GROUP } from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group";
import { IconNames } from "@blueprintjs/icons";
import IExerciseGroupData from "./exercise-group-data";
import Collections from "@saqq/telerehab-infrastructure/lib/constants/collections";
import strings from "../../constants/strings";
import IExercise from "@saqq/telerehab-infrastructure/lib/interfaces/exercise";
import IWithID from "@saqq/telerehab-infrastructure/lib/interfaces/with-id";
import Loading from "../Loading";
import TagUtils from "../../utils/tag-utils";



interface IGroupedTags {
    [group: string]: IWithID[]
}

interface IExerciseGroupsPageProps extends IFirebaseContextProps, INotificationProps, IConfirmationProps {

}

interface IExerciseGroupsPageState {
    loading: boolean;
    exerciseGroupsData: IExerciseGroupData[];
    newExerciseGroupData: IExerciseGroupData;

    ungroupedTags: IWithID[];
    groupedTags: IGroupedTags;
}

class ExerciseGroupsPageBase extends React.Component<IExerciseGroupsPageProps, IExerciseGroupsPageState> {
    constructor(props: IExerciseGroupsPageProps) {
        super(props);

        this.state = {
            loading: false,
            exerciseGroupsData: [],
            newExerciseGroupData: {
                changed: false,
                loading: false,
                exerciseGroup: {
                    id: "",
                    name: ""
                }
            },

            ungroupedTags: [],
            groupedTags: {}
        };
    }

    async componentDidMount() {
        const { firebase, notifications } = this.props;

        this.setState(state => update(state, {
            loading: { $set: true }
        }));

        try {
            const snapshot = await firebase.firestore.collection(Collections.EXERCISE_GROUPS).orderBy("name").get();

            const data: IExerciseGroupData[] = snapshot.docs.map(doc => ({
                    exerciseGroup: {
                        ...EMPTY_EXERCISE_GROUP,
                        ...doc.data() as IExerciseGroup,
                        id: doc.id,
                    },
                    changed: false,
                    loading: false
                }
            ));

            this.setState(state => update(state, {
                exerciseGroupsData: { $set: data }
            }));

            this.sortTags(data);

        } catch(exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, {
            loading: { $set: false }
        }));
    }

    getGroupIndex(exerciseGroupsData: IExerciseGroupData[], id: string) {
        return exerciseGroupsData.findIndex((item) => item.exerciseGroup.id === id);
    }

    onNameChangeAction(id: string, name: string) {
        this.setState(state => {
            const itemIndex = this.getGroupIndex(state.exerciseGroupsData, id);
            
            return update(state, {
                exerciseGroupsData: { [itemIndex]: { 
                    changed: { $set: true },
                    exerciseGroup: { name: { $set: name } }
                } }
            });
        });
    }

    async onSave(id: string) {
        const { firebase, notifications } = this.props;
        const { exerciseGroupsData } = this.state;
        
        const itemIndex = this.getGroupIndex(exerciseGroupsData, id);
        const exerciseGroup = exerciseGroupsData[itemIndex].exerciseGroup;
        const exerciseGroupWithoutID: IExerciseGroup = {
            name: exerciseGroup.name
        };

        const setLoading = (loading: boolean) => {
            this.setState(state => {
                const stateItemIndex = this.getGroupIndex(state.exerciseGroupsData, id);
    
                return update(state, { 
                    exerciseGroupsData: { [stateItemIndex]: { 
                        loading: { $set: loading }
                    } } 
                });
            });
        };

        const setChanged = (changed: boolean) => {
            this.setState(state => {
                const stateItemIndex = this.getGroupIndex(state.exerciseGroupsData, id);

                return update(state, { 
                    exerciseGroupsData: { [stateItemIndex]: { 
                        changed: { $set: changed },
                    } } 
                });
            });
        };


        setLoading(true);

        try {
            await firebase.firestore.collection(Collections.EXERCISE_GROUPS).doc(id).update(exerciseGroupWithoutID);
            setChanged(false);

            this.sortTags(exerciseGroupsData);

            notifications.showSuccess(strings.successfullt_updated_exercise_tag);
        } catch(exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        setLoading(false);
    }

    async onRemove(removeId: string) {
        const { confirmation, firebase, notifications } = this.props;
        const { exerciseGroupsData } = this.state;

        // Removes exercise group with specified id from state
        const doRemoveFromState = () => {
            const itemIndex = this.getGroupIndex(exerciseGroupsData, removeId);
            const newExerciseGroupsData = update(exerciseGroupsData, { $splice: [[itemIndex, 1]] });

            this.sortTags(newExerciseGroupsData);

            this.setState(state => {
                return update(state, {
                    exerciseGroupsData: { $set: newExerciseGroupsData }
                });
            });  

            notifications.showSuccess(strings.successfullt_removed_exercise_tag);
        };

        // Marks document as loading
        const doSetLoading = (loading: boolean) => {
            this.setState(state => {
                const itemIndex = this.getGroupIndex(state.exerciseGroupsData, removeId);
                return update(state, { 
                    exerciseGroupsData: { [itemIndex]: { 
                        loading: { $set: loading }
                    } } 
                });
            });
        };


        // Get document reference
        const docRef = firebase.firestore.collection(Collections.EXERCISE_GROUPS).doc(removeId);


        doSetLoading(true);

        try {
            const result1 = await confirmation.showConfirmation({ 
                confirmButtonText: strings.delete,
                cancelButtonText: strings.cancel,
                message: strings.delete_exercise_tag_confirmation_question,
                
                intent: Intent.DANGER,
                icon: IconNames.TRASH,
            });

            // Canceled
            if (!result1) {
                doSetLoading(false);
                return;
            }

            const exercisesSnapshot = await firebase.firestore.collection(Collections.EXERCISES).where("exerciseGroupRefs", "array-contains", docRef).get();
        
            // No references found
            if (exercisesSnapshot.docs.length === 0) {
                await docRef.delete();
                doRemoveFromState();
                return;
            }


            // Warn about existing references
            const result2 = await confirmation.showConfirmation({
                confirmButtonText: strings.delete,
                cancelButtonText: strings.cancel,
                
                message: (
                    <div>
                        <p>{`${strings.formatString(strings.exercise_tag_is_referenced_in_X_exercises, exercisesSnapshot.docs.length) as string}:`}</p>
                        <ol>
                            {exercisesSnapshot.docs.map(doc => <li>{(doc.data() as IExercise).name}</li>)}
                        </ol>
                        <p>{strings.remove_all_references_to_this_exercise_tag_in_order_to_delete_it}</p>
                    </div>
                ),

                icon: IconNames.TRASH,
                intent: Intent.DANGER,
            });

            // Cancelled
            if (!result2) {
                doSetLoading(false);
                return;
            }

            // Remove all the references
            const batch = firebase.firestore.batch();

            exercisesSnapshot.docs.forEach(doc => {
                const data = { ...doc.data() } as IExercise;
                const newExerciseGroupRefs = data.exerciseGroupRefs.filter(ref => ref.id !== docRef.id);

                batch.update(doc.ref, { exerciseGroupRefs: newExerciseGroupRefs });
            });
            batch.delete(docRef);

            await batch.commit();

            doRemoveFromState();
        } catch(exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
                doSetLoading(false);
            }
        }
    }
    
    async onAdd() {
        const { firebase, notifications } = this.props;
        const { exerciseGroupsData, newExerciseGroupData: { exerciseGroup } } = this.state;
        const exerciseGroupWithoutID: IExerciseGroup = { 
            name: exerciseGroup.name
         };

        this.setState(state => update(state, {
            newExerciseGroupData: { loading: { $set: true } }
        }));

        try {
            const doc = await firebase.firestore.collection(Collections.EXERCISE_GROUPS).add(exerciseGroupWithoutID)
            const newItem: IExerciseGroupData = {
                changed: false,
                loading: false,
                exerciseGroup:
                {
                    ...exerciseGroupWithoutID,
                    id: doc.id
                }
            };

            const newExerciseGroupsData = update(exerciseGroupsData, { $push: [newItem] });

            this.setState(state => update(state, {
                newExerciseGroupData: {
                    changed: { $set: false },
                    exerciseGroup: {
                        name: { $set: "" }
                    }
                },
                exerciseGroupsData: { $set: newExerciseGroupsData }
            }));

            notifications.showSuccess(strings.successfullt_added_exercise_tag);

            this.sortTags(newExerciseGroupsData);

        } catch(exc) {
            if (exc instanceof Error) {
                notifications.showError(exc.message);
            }
        }

        this.setState(state => update(state, {
            newExerciseGroupData: { loading: { $set: false } }
        }));
    }

    onAddNameChanged(name: string) {
        this.setState(state => update(state, {
            newExerciseGroupData: {
                exerciseGroup: {
                    name: { $set: name }
                },
                changed: { $set: true }
            }
        }));
    }

    private sortTags(exerciseGroupsData: IExerciseGroupData[]) {
        const ungroupedTags: IWithID[] = [];
        const groupedTags: IGroupedTags = {};

        exerciseGroupsData.forEach(data => {
            const groupName = TagUtils.getTagGroup(data.exerciseGroup.name);
            if (!groupName) {
                ungroupedTags.push(data.exerciseGroup);
                return;
            }

            if (!(groupName in groupedTags)) {
                groupedTags[groupName] = [data.exerciseGroup];
            } else {
                groupedTags[groupName].push(data.exerciseGroup);
            }
        });

        this.setState(state => update(state, {
            ungroupedTags: { $set: ungroupedTags },
            groupedTags: { $set: groupedTags }
        }));
    }

    render() {
        const { loading, exerciseGroupsData, newExerciseGroupData, ungroupedTags, groupedTags } = this.state;

        return (
            <div>
                <H2>{strings.navigation_exercise_tags}</H2>
                
                <Loading loading={loading}>
                    <div>
                        <UL>
                            <li key={"ungroupped_tags"}>
                                <H5>{strings.ungrouped_tags}</H5>
                                <UL>
                                    {ungroupedTags.map(withID => exerciseGroupsData[this.getGroupIndex(exerciseGroupsData, withID.id)]).map(data =>
                                        <li key={data.exerciseGroup.id}>
                                            <ExerciseGroupEdit
                                                create={false}
                                                exerciseGroupData={data}
                                                nameChangeAction={(name) => this.onNameChangeAction(data.exerciseGroup.id, name)}
                                                saveAction={() => this.onSave(data.exerciseGroup.id)}
                                                removeAction={() => this.onRemove(data.exerciseGroup.id)}
                                            />
                                        </li>)}
                                </UL>
                            </li>

                            {Object.keys(groupedTags).sort().map(group =>
                                <li key={`tag_group_${group}`}>
                                    <H5>{group}</H5>
                                    <UL>
                                        {groupedTags[group].map(withID => exerciseGroupsData[this.getGroupIndex(exerciseGroupsData, withID.id)]).map(data =>
                                            <li key={data.exerciseGroup.id}>
                                                <ExerciseGroupEdit
                                                    create={false}
                                                    exerciseGroupData={data}
                                                    nameChangeAction={(name) => this.onNameChangeAction(data.exerciseGroup.id, name)}
                                                    saveAction={() => this.onSave(data.exerciseGroup.id)}
                                                    removeAction={() => this.onRemove(data.exerciseGroup.id)}
                                                />
                                            </li>)}
                                    </UL>
                                </li>)}
                        </UL>

                        <Divider />
                        
                        <ExerciseGroupEdit
                            create={true}
                            exerciseGroupData={newExerciseGroupData}
                            nameChangeAction={(name) => this.onAddNameChanged(name)}
                            saveAction={() => this.onAdd()}
                            removeAction={() => {}}
                        />
                    </div>
                </Loading>
            </div>
        );
    }
}

const ExerciseGroupsPage = withConfirmations(withNotifications(withFirebase(ExerciseGroupsPageBase)));

export default ExerciseGroupsPage;
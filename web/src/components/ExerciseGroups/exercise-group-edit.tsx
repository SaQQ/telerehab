import React, { ChangeEvent } from "react"
import { Button, ButtonGroup, Spinner, ControlGroup, InputGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import IExerciseGroupData from "./exercise-group-data"
import strings from "../../constants/strings";

import "./exercise-group-edit.css"


interface IExerciseGroupEditProps {
    create: boolean;
    exerciseGroupData: IExerciseGroupData;

    nameChangeAction: (name: string) => void;
    saveAction: () => void;
    removeAction: () => void;
}

interface IExerciseGroupEditState {
}

class ExerciseGroupEditBase extends React.Component<IExerciseGroupEditProps, IExerciseGroupEditState> {
    constructor(props: IExerciseGroupEditProps) {
        super(props);

        this.state = {
        };
    }

    onNameChanged(name: string) {
        this.props.nameChangeAction(name);
    }

    onSave() {
        this.props.saveAction();
    }

    onRemove() {
        this.props.removeAction();
    }

    render() {
        const { create, exerciseGroupData: { changed, loading, exerciseGroup } } = this.props;

        return (
            <ControlGroup>
                <InputGroup
                    type="text"
                    onChange={(event: ChangeEvent<HTMLInputElement>) => this.onNameChanged(event.target.value)}
                    value={exerciseGroup.name}
                    placeholder={strings.exercise_tag_name}
                    fill
                    rightElement={loading ? <Spinner size={Spinner.SIZE_SMALL} /> : undefined}
                />
                {create && <ButtonGroup>
                    <Button disabled={!changed || loading} icon={IconNames.ADD} onClick={() => this.onSave()}>{strings.add}</Button>
                </ButtonGroup>}
                {!create && <ButtonGroup>
                    <Button disabled={!changed || loading} icon={IconNames.FLOPPY_DISK} onClick={() => this.onSave()}>{strings.save}</Button>
                    <Button disabled={loading} icon={IconNames.CROSS} onClick={() => this.onRemove()}>{strings.remove}</Button>
                </ButtonGroup>}
            </ControlGroup>
        );
    }
}

const ExerciseGroupEdit = ExerciseGroupEditBase;

export default ExerciseGroupEdit;

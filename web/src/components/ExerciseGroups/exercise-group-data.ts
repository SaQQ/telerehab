import IExerciseGroupWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group-with-id";

export default interface IExerciseGroupData {
    exerciseGroup: IExerciseGroupWithID;
    changed: boolean;
    loading: boolean;
}
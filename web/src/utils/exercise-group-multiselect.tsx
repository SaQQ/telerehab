import React from "react"
import { MultiSelect, ItemPredicate, ItemRenderer } from "@blueprintjs/select";
import { MenuItem, Button } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import IExerciseGroupWithID from "@saqq/telerehab-infrastructure/lib/interfaces/exercise-group-with-id";
import strings from "../constants/strings";
import TagUtils from "./tag-utils";


interface IExerciseGroupMultiSelectProps {
    fill: boolean;
    allGroups: IExerciseGroupWithID[];
    selectedGroups: IExerciseGroupWithID[];
    onSelectGroups: (groups: IExerciseGroupWithID[]) => void;
}

interface IExerciseGroupMultiSelectState {

}

export default class ExerciseGroupMultiSelect extends React.Component<IExerciseGroupMultiSelectProps, IExerciseGroupMultiSelectState> {

    static readonly MultiSelectClass = MultiSelect.ofType<IExerciseGroupWithID>();

    constructor(props: IExerciseGroupMultiSelectProps) {
        super(props);

        this.state = {

        };
    }


    private static readonly renderTag = (exerciseGroup: IExerciseGroupWithID) => exerciseGroup.name;

    private readonly isGroupSelected = (exerciseGroup: IExerciseGroupWithID) => {
        return this.getSelectedGroupIndex(exerciseGroup) !== -1;
    };

    private readonly getSelectedGroupIndex = (exerciseGroup: IExerciseGroupWithID) => {
        const { selectedGroups } = this.props;
        return selectedGroups.findIndex(group => group.id === exerciseGroup.id);        
    };

    private readonly selectGroups = (selectedGroupsToSelect: IExerciseGroupWithID[]) => {
        const { selectedGroups: selectedTags, onSelectGroups } = this.props;

        const newExerciseTags = selectedTags.slice();
        selectedGroupsToSelect.forEach(selectedTag => {
            
            // Check if already has that tag selected
            const hasTag = selectedTags.findIndex(tag => tag.id === selectedTag.id) >= 0;

            // For new tag check if has tags with the same group
            if (!hasTag) {
                const groupName = TagUtils.getTagGroup(selectedTag.name);
                if (groupName) {
                    const sameGroupIndex = newExerciseTags.findIndex(tag => {
                        const newGroupName = TagUtils.getTagGroup(tag.name);
                        if (!newGroupName)
                            return false;

                        return groupName === newGroupName;
                    });

                    if (sameGroupIndex >= 0) {
                        newExerciseTags.splice(sameGroupIndex, 1);
                    }
                }
                
                newExerciseTags.push(selectedTag);
            }
        });

        onSelectGroups(newExerciseTags);
    };

    private readonly deselectGroup = (index: number) => {
        const { selectedGroups, onSelectGroups } = this.props;

        const newExerciseGroups = selectedGroups.slice();
        newExerciseGroups.splice(index, 1);
        onSelectGroups(newExerciseGroups);
    };

    private readonly handleGroupSelect = (exerciseGroup: IExerciseGroupWithID) => {
        if (!this.isGroupSelected(exerciseGroup)) {
            this.selectGroups([exerciseGroup]);
        } else {
            this.deselectGroup(this.getSelectedGroupIndex(exerciseGroup));
        }
    };

    private readonly handleGroupsPaste = (selectedGroups: IExerciseGroupWithID[]) => {
        this.selectGroups(selectedGroups);
    };

    private readonly handleGroupsClear = () => {
        const { onSelectGroups } = this.props;
        onSelectGroups([]);
    };

    private handleTagRemove = (_tag: string, index: number) => {
        this.deselectGroup(index);
    };

    private readonly renderGroup: ItemRenderer<IExerciseGroupWithID> = (exerciseGroup, { modifiers, handleClick }) => {
        if (!modifiers.matchesPredicate) {
            return null;
        }

        return (
            <MenuItem
                active={modifiers.active}
                icon={this.isGroupSelected(exerciseGroup) ? IconNames.TICK : IconNames.BLANK}
                key={exerciseGroup.name}
                onClick={handleClick}
                text={exerciseGroup.name}
                shouldDismissPopover={false}
            />
        );
    };

    private static readonly areExerciseGroupsEqual = (exerciseGroup1: IExerciseGroupWithID, exerciseGroup2: IExerciseGroupWithID) => exerciseGroup1.id === exerciseGroup2.id;

    private static readonly filterGroups: ItemPredicate<IExerciseGroupWithID> = (query, exerciseGroup, _index, exactMatch) => {
        const normalizedName = exerciseGroup.name.toLowerCase();
        const normalizedQuery = query.toLowerCase();

        if (exactMatch) {
            return normalizedName === normalizedQuery;
        } else {
            return normalizedName.indexOf(normalizedQuery) >= 0;
        }
    };

    render() {
        const { fill, allGroups, selectedGroups } = this.props;

        const clearButton = selectedGroups.length > 0 ? <Button icon={IconNames.CROSS} minimal onClick={this.handleGroupsClear} /> : undefined;
        const noResultsElement = <MenuItem disabled text={strings.no_results} />;

        return (
            <ExerciseGroupMultiSelect.MultiSelectClass
                fill={fill}
                items={allGroups}
                selectedItems={selectedGroups}
                placeholder={`${strings.exercise_tags}...`}
                resetOnSelect={true}
                itemRenderer={this.renderGroup}
                itemsEqual={ExerciseGroupMultiSelect.areExerciseGroupsEqual}
                noResults={noResultsElement}
                onItemSelect={this.handleGroupSelect}
                onItemsPaste={this.handleGroupsPaste}
                popoverProps={{ minimal: true }}
                tagRenderer={ExerciseGroupMultiSelect.renderTag}
                tagInputProps={{ onRemove: this.handleTagRemove, rightElement: clearButton }}
                itemPredicate={ExerciseGroupMultiSelect.filterGroups}
            />
        );
    }
}

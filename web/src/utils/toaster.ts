import { Position, Toaster } from "@blueprintjs/core";
 
export const DefaultToaster = Toaster.create({
    className: "recipe-toaster",
    position: Position.TOP,
});

export default DefaultToaster;
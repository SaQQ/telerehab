import React, { useMemo } from "react";
import CSS from "csstype";
import { DropzoneOptions, useDropzone } from "react-dropzone";

const baseStyle: CSS.Properties = {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px",
    borderWidth: "2",
    borderRadius: "2",
    borderColor: "#eeeeee",
    borderStyle: "dashed",
    backgroundColor: "#fafafa",
    color: "#bdbdbd",
    outline: "none",
    transition: "border .24s ease-in-out"
};
  
const activeStyle: CSS.Properties = {
    borderColor: "#2196f3"
};

const acceptStyle: CSS.Properties = {
    borderColor: "#00e676"
};

const rejectStyle: CSS.Properties = {
    borderColor: "#ff1744"
};

export interface IStyledDropzoneProps extends DropzoneOptions {

}


const StyledDropzone: React.FunctionComponent<IStyledDropzoneProps> = (props) => {
    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone(props);

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isDragActive,
        isDragAccept,
        isDragReject,
    ]);

    return (
        <section>
            <div {...getRootProps({style})}>
                <input {...getInputProps()} />
                {props.children}
            </div>
        </section>
    );
}

export default StyledDropzone;

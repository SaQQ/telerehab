import React from 'react';

interface CancellablePromise<T> {
    promise: Promise<T>;
    cancel: () => void;
}

export class PromiseCancelled extends Error {
    constructor() {
        super("Promise cancelled");
    }
}

export function makeCancelable<T>(promise: Promise<T>): CancellablePromise<T> {
    let isCanceled = false;

    const wrappedPromise = new Promise<T>((resolve, reject) => {
        promise
            .then(val => (isCanceled ? reject(new PromiseCancelled()) : resolve(val)))
            .catch(error => (isCanceled ? reject(new PromiseCancelled()) : reject(error)));
    });

    return {
        promise: wrappedPromise,
        cancel() {
            isCanceled = true;
        },
    };
}

export class CancellablePromiseComponent<TProps, TState> extends React.Component<TProps, TState> {
    private promises: CancellablePromise<any>[] = [];

    componentWillUnmount(): void {
        this.promises.forEach(p => p.cancel());
        this.promises = [];
    }

    protected addCancelablePromise<T>(promise: Promise<T>) {
        const cancellablePromise = makeCancelable(promise);
        this.promises.push(cancellablePromise);
        return cancellablePromise.promise;
    }
}

export default class TagUtils {
    public static getTagGroup(tag: string) {
        const dividerIdx = tag.indexOf("-");
        if (dividerIdx < 0) {
            return null;
        }

        return tag.substring(0, dividerIdx).trim();
    }
}

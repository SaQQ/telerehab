export default class DateUtils {
    
    public static padWithZeros(n: number, width: number) {
        const nStr = n.toString();
        return nStr.length >= width ? nStr : new Array(width - nStr.length + 1).join("0") + nStr;
    }

    public static getDateString(date: Date) {
        return `${date.getFullYear()}-${DateUtils.padWithZeros(date.getMonth() + 1, 2)}-${DateUtils.padWithZeros(date.getDate(), 2)}`;
    }
}

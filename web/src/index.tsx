import * as React from "react";
import * as ReactDOM from "react-dom";

import App from "./components/App";
import * as serviceWorker from "./serviceWorker";

import { ScreenClassProvider } from 'react-grid-system';
import Firebase, { FirebaseContext } from "./components/Firebase";

import "@blueprintjs/core/lib/css/blueprint.css";

ReactDOM.render(
    <ScreenClassProvider>
        <FirebaseContext.Provider value={new Firebase()}>
            <App/>
        </FirebaseContext.Provider>
    </ScreenClassProvider>,
    document.getElementById("root"),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
